//
//  MainTabbarCtr.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MainTabbarCtr.h"
#import "MainViewCtr.h"
#import "ShoppingCarCtr.h"
#import "PersonCtr.h"
#import "BXNavicationController.h"
#import "ChatListVC.h"
#import "LoginVC.h"
@interface MainTabbarCtr ()<UITabBarControllerDelegate>

@end

@implementation MainTabbarCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    self.tabBar.tintColor = ThemeColor;//ios 13 pop bug
    MainViewCtr * vc = [[MainViewCtr alloc]init];
    vc.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"首页", nil) image:Image(@"shouye") selectedImage:[self backImage:Image(@"shouye-2")]];
    BXNavicationController * navVC = [[BXNavicationController alloc]initWithRootViewController:vc];
    //还款
    ShoppingCarCtr * repay = [[ShoppingCarCtr alloc]init];
    repay.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"购物车", nil) image:Image(@"gouwuche") selectedImage:[self backImage:Image(@"gouwuche-2")]];
    BXNavicationController * repayNav = [[BXNavicationController alloc]initWithRootViewController:repay];
    
    ChatListVC * chat = [ChatListVC new];
    chat.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"消息", nil) image:Image(@"xiaoxi-1") selectedImage:[self backImage:Image(@"xiaoxi")]];
    BXNavicationController * chatNav = [[BXNavicationController alloc]initWithRootViewController:chat];
    //个人
    PersonCtr * person  = [PersonCtr new];
    person.tabBarItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"个人", nil) image:Image(@"geren") selectedImage:[self backImage:Image(@"geren-2")]];
    BXNavicationController * personNav = [[BXNavicationController alloc]initWithRootViewController:person];
    [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName:ThemeColor} forState:UIControlStateSelected];
    
    self.viewControllers = @[navVC,repayNav,chatNav,personNav];
    self.selectedIndex = 0;
}

-(UIImage*)backImage:(UIImage*)image{
    return  [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    BXNavicationController *navCtrl = (BXNavicationController *)viewController;
    UIViewController *rootCtrl = navCtrl.topViewController;
    if ([rootCtrl isKindOfClass:[ShoppingCarCtr class]]|| [rootCtrl isKindOfClass:[ChatListVC class]]) {
        NSString * token = [DLUserDefaultModel userDefaultsModel].token;
        if (token.length == 0) {
            [self gotoLoginVC];
            return NO;
        }else{
            return YES;
        }
    }else{
        return YES;
    }
}
-(void)gotoLoginVC{
    LoginVC * vc = [LoginVC new];
    BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
