//
//  BXNavicationController.h
//  SABusiness
//
//  Created by Jinniu on 2019/12/12.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BXNavicationController : UINavigationController
@property (nonatomic, strong) id popDelegate;
@end

NS_ASSUME_NONNULL_END
