//
//  PersonBindCardVC.m
//  SABusiness
//
//  Created by Jinniu on 2019/12/27.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import "PersonBindCardVC.h"

@interface PersonBindCardVC (){
    UIScrollView * _bgScro;
    UITextField * _nameTF, * _bankNameTF,*_numTF;
}
@end

@implementation PersonBindCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.myTitle = NSLocalizedString(@"绑定银行卡", nil);
    [self createUI];
}
-(void)botBtnClick{
    
}
-(void)createUI{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:NSLocalizedString(@"保存", nil) forState:UIControlStateNormal];
    btn.backgroundColor = ThemeColor;
    btn.cornerRadius = 5;
    [btn addTarget:self action:@selector(botBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    btn.sd_layout.leftSpaceToView(self.view, 15).rightSpaceToView(self.view, 15).bottomSpaceToView(self.view, KTabbarSafeBottomMargin+30).heightIs(45);
    
    _bgScro = [UIScrollView new];
    [self.view addSubview:_bgScro];
    _bgScro.sd_layout.spaceToSuperView(UIEdgeInsetsMake(kTopHeight, 0, KTabbarSafeBottomMargin +100, 0));
    
    NSArray * nameArr = @[NSLocalizedString(@"真实姓名", nil),NSLocalizedString(@"银行名称", nil),NSLocalizedString(@"银行卡号", nil)];
    NSString * tmp = NSLocalizedString(@"请输入", nil);
    UIView * lastView= nil;
    for (int i =0; i<3; i++) {
        UIView * vpp = [UIView new];
        vpp.backgroundColor = [UIColor whiteColor];
        [_bgScro addSubview:vpp];
        vpp.sd_layout.leftEqualToView(_bgScro).rightEqualToView(_bgScro).topSpaceToView(lastView==nil?_bgScro:lastView, 1).heightIs(45);
        UILabel* lab = [UILabel labelWithTitle:nameArr[i] color:[UIColor blackColor] fontSize:14];
        [vpp addSubview:lab];
        lab.sd_layout.leftSpaceToView(vpp, 15).centerYEqualToView(vpp).heightIs(14);
        [lab setSingleLineAutoResizeWithMaxWidth:300];
        UITextField * tf = [UITextField new];
        tf.tag = 10 + i;
        NSString * ho = [NSString stringWithFormat:@"%@ %@",tmp,nameArr[i]];
        tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:ho attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
        [vpp addSubview:tf];
        tf.sd_layout.leftSpaceToView(lab, 10).centerYEqualToView(lab).heightIs(35).rightSpaceToView(vpp, 15);
        lastView = vpp;
    }
    
}
@end
