//
//  UserDataVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/21.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "UserDataVC.h"
#import "PGDatePickManager.h"
#import "DLPickerView.h"
#import <TZImagePickerController/TZImagePickerController.h>
@interface UserDataVC ()<TZImagePickerControllerDelegate,PGDatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nicknameTF;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *sexLab;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLab;
@property (weak, nonatomic) IBOutlet UIButton *choseBirthBtnClick;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
@property(nonatomic,copy)NSString * headPath,* sexFlag,*dateStr;
@end

@implementation UserDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"用户信息", nil);
    self.topHeight.constant = kTopHeight +10;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.viewWidth.constant = IPHONE_WIDTH;
    self.botHeight.constant= KTabbarSafeBottomMargin + 80;
    [self.iconView addTarget:self action:@selector(choseImageClick)];
    [self configData];
}
-(void)configData{
    VicSingleObject * single = [VicSingleObject getInstance];
    NSString * head = [NSString stringWithFormat:@"%@%@",MainDomain,single.userInfoModel.avatar];
    [self.iconView sd_setImageWithURL:URL(head) placeholderImage:PlaceHolderImg];
    self.nicknameTF.text = single.userInfoModel.nickname.length==0?@"":single.userInfoModel.nickname;
    if (![single.userInfoModel.sex isKindOfClass:[NSNull class]]) {
        if ([single.userInfoModel.sex isEqualToString:@"1"]) {
            self.sexFlag = @"1";
            self.sexLab.text = NSLocalizedString(@"男", nil);
        }else{
            self.sexFlag = @"2";
            self.sexLab.text = NSLocalizedString(@"女", nil);
        }
    }
    if ([Mytools timeStampTotime:[NSString stringWithFormat:@"%ld",single.userInfoModel.birthday]].length>=10) {
        NSString * date = [[Mytools timeStampTotime:[NSString stringWithFormat:@"%ld",single.userInfoModel.birthday]] substringWithRange:NSMakeRange(0, 10)];
        self.birthdayLab.text = date;
        self.dateStr = date;
    }    
}
-(void)choseImageClick{
    TZImagePickerController * pick = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:self];
    [pick setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        UIImage * img = photos[0];
        self.iconView.image = img;
        NSString * url = [WorkUrl returnURL:Interface_For_UploadFile];
        UIImage * img2 = [img compressImageToByte:1000 * 1024];
        NSDictionary * dic = @{@"token":[DLUserDefaultModel userDefaultsModel].token};
        [SVProgressHUD show];
        [PGNetworkHelper uploadSingleFileWithURL:url parameters:dic image:img2 name:@"avatar" mimeType:@"png" progress:nil success:^(id responseObject) {
            VLog(@"%@",responseObject);
            [SVProgressHUD dismiss];
            if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
                self.headPath = responseObject[@"data"][@"url"];
            }else{
                [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            }

        } failure:^(NSError *error) {
            VLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    }];
    [self presentViewController:pick animated:YES completion:nil];
}
-(AFHTTPSessionManager *)manager {
    static PGNetAPIClient *manager = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [PGNetAPIClient sharedClient];
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = 20.0f;
        manager.requestSerializer.cachePolicy = NSURLCacheStorageNotAllowed;
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"multipart/form-data", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    });
    return manager;
}
- (IBAction)choseSexBtnClick:(id)sender {
    DLPickerView * pick = [[DLPickerView alloc]initWithDataSource:@[NSLocalizedString(@"男", nil),NSLocalizedString(@"女", nil)] withSelectedItem:nil withSelectedBlock:^(id  _Nonnull item) {
        self.sexLab.text = item;
        self.sexLab.textColor = [UIColor blackColor];
        if ([item isEqualToString:@"男"]||[item isEqualToString:@"man"]) {
            self.sexFlag = @"1";
        }else{
            self.sexFlag = @"2";
        }
    }];
    [pick show];
}
- (IBAction)choseBirthdayClick:(id)sender {
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    datePicker.datePickerMode = PGDatePickerModeDate;
    [self presentViewController:datePickManager animated:false completion:nil];
    datePickManager.isShadeBackground = true;
    //设置线条的颜色
    datePicker.lineBackgroundColor = [UIColor groupTableViewBackgroundColor];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow = [UIColor blackColor];
    //设置未选中行的字体颜色
    datePicker.textColorOfOtherRow = [UIColor lightGrayColor];
    datePickManager.cancelButtonTextColor = [UIColor lightGrayColor];
    datePickManager.cancelButtonText = NSLocalizedString(@"取消", nil);
    datePickManager.cancelButtonFont = [UIFont boldSystemFontOfSize:16];
    
    datePickManager.confirmButtonTextColor = ThemeColor;
    datePickManager.confirmButtonText = NSLocalizedString(@"确定", nil);
    datePickManager.confirmButtonFont = [UIFont boldSystemFontOfSize:16];
}
#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
//    NSLog(@"dateComponents = %@", dateComponents);
    NSString * date = [NSString stringWithFormat:@"%ld-%02ld-%02ld",dateComponents.year,dateComponents.month,dateComponents.day];
    self.dateStr = date;
    self.birthdayLab.text = date;
    self.birthdayLab.textColor = [UIColor blackColor];
}
- (IBAction)submitBtnClick:(id)sender {
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    if (self.headPath.length) {
        [dic setObject:self.headPath forKey:@"avatar"];
    }
    if (self.nicknameTF.text.length) {
        [dic setObject:self.nicknameTF.text forKey:@"nickname"];
    }
    if(self.sexFlag.length){
        [dic setObject:self.sexFlag forKey:@"sex"];
    }
    if (self.dateStr.length) {
        [dic setObject:self.dateStr forKey:@"date"];
    }
    NSString * url = [WorkUrl returnURL:Interface_For_PostSetUserInfo];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"msg"] status:Success];
            [ReuseFile getUserInfo:^(NSDictionary * _Nonnull dic) {
                NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
                NSNotification * message = [[NSNotification alloc]initWithName:@"modifyuserinfo" object:self userInfo:nil];
                [center postNotification:message];
                [self.navigationController popViewControllerAnimated:YES];
            }];            
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

@end
