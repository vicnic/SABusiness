//
//  SystemMsgCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SystemMsgCell : UITableViewCell
@property(nonatomic,retain)NSDictionary * dic;
@end

NS_ASSUME_NONNULL_END
