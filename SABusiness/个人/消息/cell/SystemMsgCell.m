//
//  SystemMsgCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SystemMsgCell.h"
@interface SystemMsgCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end
@implementation SystemMsgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}
- (void)setDic:(NSDictionary *)dic{
    if (![dic[@"title"] isKindOfClass:[NSNull class]]) {
        self.titleLab.text = dic[@"title"];
    }
    if (![dic[@"content"] isKindOfClass:[NSNull class]]) {
        self.contentLab.text = dic[@"content"];
    }
    if (![dic[@"updated_at"] isKindOfClass:[NSNull class]]) {
        self.timeLab.text = dic[@"updated_at"];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
