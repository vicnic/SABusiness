//
//  ProblemReportVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/21.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "ProblemReportVC.h"
#import <QMUIKit/QMUIKit.h>
#import <TZImagePickerController/TZImagePickerController.h>
@interface ProblemReportVC ()<TZImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet QMUITextView *inputTV;
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;

@end

@implementation ProblemReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"问题反馈", nil);
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.topHeight.constant = kTopHeight+10;
    self.botHeight.constant = KTabbarSafeBottomMargin + 80;
    [self.picView addTarget:self action:@selector(picViewClcik)];
}
-(void)picViewClcik{
    TZImagePickerController * pick = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:self];
    [pick setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        UIImage * pic = photos[0];
        self.picView.image = pic;
    }];
    [self presentViewController:pick animated:YES completion:nil];
}
- (IBAction)saveBtnClick:(id)sender {
    if (self.inputTV.text.length ==0 ) {
        [Mytools warnText:NSLocalizedString(@"问题描述不能为空", nil) status:Error];
        return;
    }
}


@end
