//
//  SecuritySettingVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/21.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SecuritySettingVC.h"
#import "ModifyPasswordVC.h"
#import "ProblemReportVC.h"
#import <SDImageCache.h>
@interface SecuritySettingVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSArray * titleArr;
@end

@implementation SecuritySettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"安全设置", nil);
    self.titleArr = @[NSLocalizedString(@"修改密码", Nil),NSLocalizedString(@"清除缓存", nil),NSLocalizedString(@"问题反馈", nil)];
    [self.view addSubview:self.myTab];
}

#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titleArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = self.titleArr[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        ModifyPasswordVC * vc = [ModifyPasswordVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row==1){
        [SVProgressHUD show];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
            [SVProgressHUD dismiss];
            [Mytools warnText:NSLocalizedString(@"清理成功", nil) status:Success];
        }];
    }else if(indexPath.row==2){
        ProblemReportVC * vc = [ProblemReportVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
    }
    return _myTab;
}

@end
