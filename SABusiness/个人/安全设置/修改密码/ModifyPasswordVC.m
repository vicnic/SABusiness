//
//  ModifyPasswordVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/21.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "ModifyPasswordVC.h"
#import "LoginVC.h"
#import "BXNavicationController.h"
@interface ModifyPasswordVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet UITextField *oldPswTF;
@property (weak, nonatomic) IBOutlet UITextField *pswNewTF;
@property (weak, nonatomic) IBOutlet UITextField *pswNew2TF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;

@end

@implementation ModifyPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"修改密码",nil);
    self.topHeight.constant = kTopHeight;
    self.botHeight.constant = KTabbarSafeBottomMargin + 80;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}
- (IBAction)saveBtnClick:(id)sender {
    if (self.oldPswTF.text.length == 0) {
        [Mytools warnText:NSLocalizedString(@"旧密码不能为空", nil) status:Error];
        return;
    }
    if (self.oldPswTF.text.length == 0) {
        [Mytools warnText:NSLocalizedString(@"新密码不能为空", nil) status:Error];
        return;
    }
    if (self.oldPswTF.text.length == 0) {
        [Mytools warnText:NSLocalizedString(@"再次输入不能为空", nil) status:Error];
        return;
    }
    if (![self.pswNewTF.text isEqualToString:self.pswNew2TF.text]) {
        [Mytools warnText:NSLocalizedString(@"两次输入不一致", nil) status:Error];
        return;
    }
    NSString * url = [WorkUrl returnURL:Interface_For_ModifyUserPassword];
    NSMutableDictionary * p = [ReuseFile getParamDic];
    [p setObject:self.oldPswTF.text forKey:@"old_password"];
    [p setObject:self.pswNewTF.text forKey:@"new_password"];
    [p setObject:self.pswNewTF.text forKey:@"new_password_confirm"];
    [p setObject:[DLUserDefaultModel userDefaultsModel].token forKey:@"token"];
    [PGNetworkHelper GET:url parameters:p  cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"desc"] status:Success];
            LoginVC * vc = [LoginVC new];
            BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
            [UIApplication sharedApplication].delegate.window.rootViewController = nav;
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
@end
