//
//Created by ESJsonFormatForMac on 20/03/11.
//

#import <Foundation/Foundation.h>


@interface MyFocusListModel : NSObject

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_thumb;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, copy) NSString *shop_description;

@property (nonatomic, copy) NSString *account;

@property (nonatomic, assign) NSInteger is_forbidden;

@property (nonatomic, assign) NSInteger focus_count;

@property (nonatomic, assign) NSInteger shop_id;

@end
