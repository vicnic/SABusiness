//
//  MyAttentionVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/26.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "MyAttentionVC.h"
#import "MyAttentionsCell.h"
#import "StoreMainViewVC.h"
@interface MyAttentionVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation MyAttentionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"我的关注", nil);
    self.dataArr = [NSMutableArray new];
    [self.view addSubview:self.myTab];
    [self getData];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetMyFocusStore];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [MyFocusListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myTab reloadData];
            [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyAttentionsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[MyAttentionsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.model= self.dataArr[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyFocusListModel * mo = self.dataArr[indexPath.row];
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = mo.shop_id;
    [self.navigationController pushViewController:vc animated:YES];
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.rowHeight = 100;
    }
    return _myTab;
}

@end
