//
//  MyAttentionsCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/26.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "MyAttentionsCell.h"
@interface MyAttentionsCell()
@property(nonatomic,retain)UIImageView * headView;
@property(nonatomic,retain)UILabel * nameLab;
@end
@implementation MyAttentionsCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
- (void)setModel:(MyFocusListModel *)model{
    [self.headView sd_setImageWithURL:URL(model.shop_thumb) placeholderImage:PlaceHolderImg];
    self.nameLab.text = model.shop_name;
}
-(void)createCusUI{
    UIView * bgView = [[UIView alloc]initWithFrame:Frame(15, 10, IPHONE_WIDTH-30, 80)];
    bgView.backgroundColor = [ UIColor whiteColor];
    bgView.cornerRadius = 5;
    [self.contentView addSubview:bgView];
    self.headView = [UIImageView new];
    _headView.image = PlaceHolderImg;
    [bgView addSubview:self.headView];
    _headView.sd_layout.leftSpaceToView(bgView, 15).centerYEqualToView(bgView).heightIs(50).widthEqualToHeight();
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:16];
    [bgView addSubview:self.nameLab];
    _nameLab.sd_layout.leftSpaceToView(_headView, 10).heightIs(24).centerYEqualToView(_headView);
    [_nameLab setSingleLineAutoResizeWithMaxWidth:300];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
