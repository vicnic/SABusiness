//
//  MyAttentionsCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/26.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyFocusListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyAttentionsCell : UITableViewCell
@property(nonatomic,retain)MyFocusListModel * model;
@end

NS_ASSUME_NONNULL_END
