//
//  PersonCtr.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "PersonCtr.h"
#import "PersonCell.h"
#import "MyPointsItemView.h"
#import "PersonHeaderView.h"
#import "MyPointDetailVC.h"
#import "SecuritySettingVC.h"
#import <JSHAREService.h>
#import "PersonBindCardVC.h"
#import "AddressListVC.h"
#import "UserDataVC.h"
#import "SystemMsgVC.h"
#import "MyAttentionVC.h"
#import "LoginVC.h"
#import "BXNavicationController.h"
@interface PersonCtr ()<UITableViewDelegate,UITableViewDataSource>{
    MyPointsItemView * _orderTypeView;
}
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSArray * titleArr,*imgArr;
@end

@implementation PersonCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar = YES;
    self.isHideBottomLine = YES;
    self.titleArr = @[@[],@[NSLocalizedString(@"我的关注", nil),NSLocalizedString(@"我的积分", nil),NSLocalizedString(@"收货地址", nil),NSLocalizedString(@"用户信息", nil),NSLocalizedString(@"安全设置", nil),NSLocalizedString(@"系统公告", nil)]];
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(modifyUserAction) name:@"modifyuserinfo" object:nil];
    self.imgArr = @[@"star",@"jifen",@"dizhi",@"yonghuxinxi",@"yinhangqia",@"shezhi",@"xinbaniconshangchuan-"];
    [self.view addSubview:self.myTab];
    [self getOrderNumber];
}
-(void)modifyUserAction{
    [self getOrderNumber];
}
-(void)getOrderNumber{
    NSString * url = [WorkUrl returnURL:Interface_For_GetCornerNumber];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSInteger waitPay = [responseObject[@"data"][@"wait_pay"] integerValue];
            NSInteger waitSend = [responseObject[@"data"][@"wait_send"] integerValue];
            NSInteger waitReceive = [responseObject[@"data"][@"wait_receive"] integerValue];
            NSInteger waitComment = [responseObject[@"data"][@"wait_comment"] integerValue];
            self->_orderTypeView.wpayStr = [NSString stringWithFormat:@"%ld",waitPay];
            self->_orderTypeView.wsendStr = [NSString stringWithFormat:@"%ld",waitSend];
            self->_orderTypeView.wreceiveStr = [NSString stringWithFormat:@"%ld",waitReceive];
            self->_orderTypeView.wcommonStr = [NSString stringWithFormat:@"%ld",waitComment];
        }else{
//            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * arr = self.titleArr[section];
    return arr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PersonCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.titleLab.text = self.titleArr[indexPath.section][indexPath.row];
    cell.iconView.image = Image(self.imgArr[indexPath.row]);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?(114+kStatusBarHeight):10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?137:0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section==0){
        NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"PersonHeaderView" owner:nil options:nil];
        PersonHeaderView * v = obj.firstObject;
        v.frame = Frame(0, 0, IPHONE_WIDTH, 124);
        return v;
    }
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0) {
        NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"MyPointsItemView" owner:nil options:nil];
        _orderTypeView = obj.firstObject;
        _orderTypeView.frame = Frame(0, 0, IPHONE_WIDTH, 137);
        return _orderTypeView;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * token = [DLUserDefaultModel userDefaultsModel].token;
    if (token.length==0) {
        [self gotoLoginVC];
        return;
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            MyAttentionVC * vc = [MyAttentionVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row==1){
            MyPointDetailVC * vc = [MyPointDetailVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row==2){
            AddressListVC * vc = [AddressListVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row==3){
            UserDataVC * vc = [UserDataVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row==4){
            SecuritySettingVC * vc = [SecuritySettingVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 5){
            NSData * imgData = UIImagePNGRepresentation(Image(@"applogo"));
            JSHAREMessage *message = [JSHAREMessage message];
            message.url = @"http://www.baidu.com";
            message.image = imgData;
            message.imageURL = @"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2479987762,3939983567&fm=26&gp=0.jpg";
            message.title = @"Weshop makes life happy";
            message.platform = JSHAREPlatformFacebook;
            message.mediaType = JSHARELink;
            [JSHAREService share:message handler:^(JSHAREState state, NSError *error) {
                NSLog(@"分享回调");
            }];
            //            SystemMsgVC * vc = [SystemMsgVC new];
            //            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
-(void)gotoLoginVC{
    LoginVC * vc = [LoginVC new];
    BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT-kTabbarHeight) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        [_myTab registerNib:[UINib nibWithNibName:@"PersonCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _myTab;
}

@end
