//
//  MyPointDetailCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MyPointDetailCell.h"
@interface MyPointDetailCell()
@property (retain, nonatomic) UILabel *titleLab;
@property (retain, nonatomic) UILabel *timeLab;
@property (retain, nonatomic) UILabel *valueLab;

@end
@implementation MyPointDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.valueLab = [UILabel labelWithTitle:@"0" color:[UIColor blackColor] fontSize:14];
    [self.contentView addSubview:self.valueLab];
    _valueLab.sd_layout.rightSpaceToView(self.contentView, 15).centerYEqualToView(self.contentView).heightIs(16);
    [_valueLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.titleLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.contentView, 15).topSpaceToView(self.contentView, 15).rightSpaceToView(self.valueLab, 10).autoHeightRatio(0);
    
    self.timeLab = [UILabel labelWithTitle:@"" color:[UIColor darkGrayColor] fontSize:12];
    [self.contentView addSubview:self.timeLab];
    _timeLab.sd_layout.leftEqualToView(_titleLab).topSpaceToView(_titleLab, 10).heightIs(16);
    [_timeLab setSingleLineAutoResizeWithMaxWidth:300];
    
    [self setupAutoHeightWithBottomView:_timeLab bottomMargin:10];
}
- (void)setModel:(CellListModel *)model{
    self.valueLab.text = [NSString stringWithFormat:@"%ld",model.count];
    self.titleLab.text = model.title;
    self.timeLab.text = model.created_at;
}
@end
