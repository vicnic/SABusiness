//
//  MyPointDetailCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPointDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyPointDetailCell : UITableViewCell
@property(nonatomic,retain)CellListModel *model;
@end

NS_ASSUME_NONNULL_END
