//
//  MyPointDetailVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MyPointDetailVC.h"
#import "MyPointHeadView.h"
#import "GivingIntegralVC.h"
#import "MyPointDetailCell.h"
@interface MyPointDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)MyPointDetailModel * mainModel;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation MyPointDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"我的积分", nil);
    self.rightBtnName = @"赠送积分";
    self.rightBtnNameColor = [UIColor blackColor];
    self.rightBtnFontSize = 14;
    self.dataArr = [NSMutableArray new];
    [self.view addSubview:self.myTab];
    [self getData];
}
- (void)rightBtnClick{
    GivingIntegralVC * vc = [GivingIntegralVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetUserPointList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            self.mainModel = [MyPointDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
            self.dataArr = [CellListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"list"]];
            [self.myTab reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyPointDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[MyPointDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellListModel * mo = self.dataArr[indexPath.row];
    CGFloat height = [tableView cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[MyPointDetailCell class] contentViewWidth:IPHONE_WIDTH];
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 213;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"MyPointHeadView" owner:nil options:nil];
    MyPointHeadView * v = obj.firstObject;
    v.frame = Frame(0, 0, IPHONE_WIDTH, 213);
    if (self.mainModel) {
        v.unusedLab.text = [NSString stringWithFormat:@"%ld",self.mainModel.integration.unused];
        v.freezeLab.text = [NSString stringWithFormat:@"%ld",self.mainModel.integration.freeze];
        v.usedLab.text = [NSString stringWithFormat:@"%ld",self.mainModel.integration.used];
        v.expiredLab.text = [NSString stringWithFormat:@"%ld",self.mainModel.integration.expired];
    }
    return  v;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
    }
    return _myTab;
}

@end
