//
//  MyPointHeadView.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPointHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *unusedLab;
@property (weak, nonatomic) IBOutlet UILabel *freezeLab;
@property (weak, nonatomic) IBOutlet UILabel *usedLab;
@property (weak, nonatomic) IBOutlet UILabel *expiredLab;

@end

NS_ASSUME_NONNULL_END
