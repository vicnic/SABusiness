//
//  GivingIntegralShareView.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^CloseBtnBlock)(BOOL isNotify);
@interface GivingIntegralShareView : UIView
@property(nonatomic,retain)UIImage * img;
@property(nonatomic,copy)NSString * shareUrl,*score;
@end

NS_ASSUME_NONNULL_END
