//
//  GivingIntegralShareView.m
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "GivingIntegralShareView.h"
#import "IntegerSeePaperView.h"
#import <NudeIn/NudeIn.h>

@interface GivingIntegralShareView()
@property (weak, nonatomic) IBOutlet UIImageView *qrCodeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin3;
@property (weak, nonatomic) IBOutlet UILabel *outDateLab;
@property (weak, nonatomic) IBOutlet UIView *nudeBgView;



@end
@implementation GivingIntegralShareView
- (void)awakeFromNib{
    [super awakeFromNib];
    CGFloat margin = (IPHONE_WIDTH-40 - 45* 4)/3.f;
    self.margin1.constant = self.margin2.constant = self.margin3.constant = margin;
}

- (void)setImg:(UIImage *)img{
    _img = img;
    self.qrCodeView.image = img;
}
- (void)setShareUrl:(NSString *)shareUrl{
    _shareUrl = shareUrl;
}
- (void)setScore:(NSString *)score{
    _score = score;
    NSString * showString = [NSString stringWithFormat:@"%@积分",score];
    NudeIn *str = [NudeIn make:^(NUDTextMaker *make) {
        make.text(@"让客户打开微信扫一扫获取").font(12).attach();
        make.text(showString).color([UIColor redColor]).font(12).attach();
    }];
    NSString * str1 = [NSString stringWithFormat:@"%@%@",@"让客户打开微信扫一扫获取",showString];
    CGFloat w1 = [str1 sizeWithFont:[UIFont systemFontOfSize:12] maxSize:CGSizeMake(300, 16)].width;
    [self.nudeBgView addSubview:str];
    str.sd_layout.centerXEqualToView(self.nudeBgView).centerYEqualToView(self.nudeBgView).widthIs(w1).heightIs(14);
}
- (IBAction)closeBtnClick:(id)sender {
    [self closeAction:NO];
}
-(void)closeAction:(BOOL)isNotify{
    [UIView animateWithDuration:0.4 animations:^{
        self.y = IPHONE_HEIGHT;
    }completion:^(BOOL finished) {
        [self.superview removeFromSuperview];
    }];
}
- (IBAction)showSeePaperClick:(id)sender {
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"IntegerSeePaperView" owner:nil options:nil];
    IntegerSeePaperView * v = obj.firstObject;
    v.frame = Frame(0, IPHONE_HEIGHT, IPHONE_WIDTH, 500);
//    v.shopName = single.shopInfoModel.ShopName;
    v.score = _score;
    v.codeImg = _img;
    [self.superview addSubview:v];
    [UIView animateWithDuration:0.4 animations:^{
        v.y = IPHONE_HEIGHT-500;
    }];
}
- (IBAction)wxBtnClick:(id)sender {
}
- (IBAction)friendCircleClick:(id)sender {
}
- (IBAction)copyLinkClick:(id)sender {
    [Mytools warnText:@"已复制" status:Success];
    UIPasteboard * board = [UIPasteboard generalPasteboard];
    board.string = _shareUrl;
}

- (IBAction)backIntegralClick:(id)sender {
    [[self getCurrentViewController].navigationController popViewControllerAnimated:YES];
}

@end
