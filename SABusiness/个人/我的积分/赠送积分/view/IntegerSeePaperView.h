//
//  IntegerSeePaperView.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/24.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IntegerSeePaperView : UIView
@property(nonatomic,copy)NSString * score, * shopName;
@property(nonatomic,retain)UIImage * codeImg;
@end

NS_ASSUME_NONNULL_END
