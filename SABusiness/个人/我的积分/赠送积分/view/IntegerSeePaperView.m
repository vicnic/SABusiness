//
//  IntegerSeePaperView.m
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/24.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "IntegerSeePaperView.h"
@interface IntegerSeePaperView()
@property (weak, nonatomic) IBOutlet UIView *seaView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seaWidth;
@property (weak, nonatomic) IBOutlet UILabel *scoreLab;
@property (weak, nonatomic) IBOutlet UILabel *shopLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *codeView;
@property(nonatomic,retain)UIImage * shareView;
@end
@implementation IntegerSeePaperView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.seaWidth.constant = IPHONE_WIDTH-30;
    [self drawLine];
}
- (void)setScore:(NSString *)score{
    self.scoreLab.text = [NSString stringWithFormat:@"%@积分",score];
}
- (void)setShopName:(NSString *)shopName{
    self.shopLab.text = [NSString stringWithFormat:@"    %@    ",shopName];
}
- (void)setCodeImg:(UIImage *)codeImg{
    self.codeView.image = codeImg;
}
- (IBAction)closeBtnClick:(id)sender {
    [UIView animateWithDuration:0.4 animations:^{
        self.y = IPHONE_HEIGHT;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)drawLine{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(0, 145)];
    [path addQuadCurveToPoint:CGPointMake(IPHONE_WIDTH-30, 145) controlPoint:CGPointMake((IPHONE_WIDTH-30)/2.f, 200)];
    [path addLineToPoint:CGPointMake(IPHONE_WIDTH-30, 0)];
    [path addLineToPoint:CGPointMake(0 , 0)];
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.path = path.CGPath;
    layer.strokeColor = [UIColor colorWithHue:0.99 saturation:0.77 brightness:0.81 alpha:1].CGColor;
    layer.lineWidth = 1;
    layer.fillColor = [UIColor colorWithHue:0.99 saturation:0.77 brightness:0.81 alpha:1].CGColor;
    [self.seaView.layer addSublayer:layer];
    
    [self.seaView bringSubviewToFront:self.scoreLab];
    [self.seaView bringSubviewToFront:self.shopLab];
    [self.seaView bringSubviewToFront:self.bgView];
    [self.seaView bringSubviewToFront:self.codeView];
}
- (IBAction)saveClick:(id)sender {
    self.shareView = [Mytools viewBecomeImage:self.seaView];
    [ReuseFile saveImageToLocal:self.shareView];
}
- (IBAction)wxShareClick:(id)sender {
    self.shareView = [Mytools viewBecomeImage:self.seaView];
}
- (IBAction)friendShareClick:(id)sender {
    self.shareView = [Mytools viewBecomeImage:self.seaView];
}
-(void)shareMethod:(NSInteger)tag{
    
}
@end
