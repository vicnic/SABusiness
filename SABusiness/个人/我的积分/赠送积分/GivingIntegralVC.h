//
//  GivingIntegralVC.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^RefreshBlock)(void);
@interface GivingIntegralVC : SuperViewController
@property(nonatomic,assign)CGFloat currentScore;
@property(nonatomic,copy)RefreshBlock refreshBlock;
@end

NS_ASSUME_NONNULL_END
