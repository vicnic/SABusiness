//
//  GivingIntegralVC.m
//  TwoOneEight
//
//  Created by Jinniu on 2019/8/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "GivingIntegralVC.h"
#import "GivingIntegralShareView.h"
#import "SGQRCodeGenerateManager.h"
#import "SGQRCodeScanningVC.h"
#import "SGQRCodeTool.h"
@interface GivingIntegralVC ()<UITextFieldDelegate>{
    GivingIntegralShareView * _shareV;
}
@property (weak, nonatomic) IBOutlet UITextField *scoreTF;
@property (weak, nonatomic) IBOutlet UILabel *descLab;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property(nonatomic,retain) UIView * shelterView;
@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property(nonatomic,copy)NSString * scanResult;
@end

@implementation GivingIntegralVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = @"赠送积分";
    self.topHeight.constant = kTopHeight + 10 ;
    self.scoreTF.delegate = self;
    [self.scoreTF addTarget:self action:@selector(textFieldDidChangeValue:) forControlEvents:UIControlEventEditingChanged];
    self.view.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.95 alpha:1];
    self.descLab.text = [NSString stringWithFormat:@"当前剩余积分%.2f",self.currentScore];
    NSNotificationCenter *center2 = [NSNotificationCenter defaultCenter];
    [center2 addObserver:self selector:@selector(receiveSendNotify) name:@"JNSendIntegralActiveNotification" object:nil];
    self.currentScore = 10;//测试的硬编码
}

-(void)receiveSendNotify{
    NSString * titleStr = [NSString stringWithFormat:@"%@积分赠送成功",self.scoreTF.text];
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:titleStr andMessage:@""];
    alertView.titleFont = [UIFont boldSystemFontOfSize:18];
    alertView.defaultButtonTitleColor = [UIColor colorWithHue:0.64 saturation:0.78 brightness:0.95 alpha:1];
    [alertView addButtonWithTitle:@"我知道了" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
        if(self.refreshBlock){
            self.refreshBlock();
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertView show];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (string.length == 0) {
        return YES;
    }
    NSString *checkStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *regex = @"^\\-?([1-9]\\d*|0)(\\.\\d{0,2})?$";
    return [self isValid:checkStr withRegex:regex];
}
- (void)textFieldDidChangeValue:(UITextField*)textField{
    if (textField.text.length>0&&[textField.text floatValue]<=self.currentScore) {
        self.sendBtn.backgroundColor = ThemeColor;
        self.sendBtn.userInteractionEnabled = YES;
        
        self.scanBtn.userInteractionEnabled = YES;
        self.scanBtn.backgroundColor = [UIColor whiteColor];
        self.scanBtn.layer.borderColor = ThemeColor.CGColor;
        [self.scanBtn setTitleColor:ThemeColor forState:UIControlStateNormal];
        self.scanBtn.layer.borderWidth = 1;
    }else{
        self.sendBtn.backgroundColor = [UIColor colorWithHue:0.65 saturation:0.17 brightness:0.95 alpha:1];
        self.sendBtn.userInteractionEnabled = NO;
        self.scanBtn.backgroundColor = [UIColor colorWithHue:0.65 saturation:0.17 brightness:0.95 alpha:1];
        self.scanBtn.userInteractionEnabled = NO;
        [self.scanBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.scanBtn.layer.borderColor = [UIColor clearColor].CGColor;
    }
    if ([textField.text floatValue]>self.currentScore) {
        self.descLab.text = @"积分不足，请重新输入";
        self.descLab.textColor = [UIColor colorWithHue:1 saturation:0.72 brightness:0.91 alpha:1];
    }else{
        self.descLab.text = [NSString stringWithFormat:@"当前剩余积分%.2f",self.currentScore];
        self.descLab.textColor = [UIColor lightGrayColor];
    }
}
- (BOOL) isValid:(NSString*)checkStr withRegex:(NSString*)regex{
    NSPredicate *predicte = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicte evaluateWithObject:checkStr];
}
- (IBAction)sendBtnClick:(id)sender {
    if ([self.scoreTF.text integerValue]>self.currentScore) {
        return;
    }
    [self sendRequest:NO];
}
-(void)sendRequest:(BOOL)isScan{
    [self.view endEditing:YES];
    [self showCode:@"http://www.baidu.com/ds/djfsldfseif"];
//    NSString * url = [WorkUrl returnURL:Interface_For_PostScanSendPoint];
//    VicSingleObject * single = [VicSingleObject getInstance];
//    NSDictionary * dic = nil;
//    if (isScan) {
//        dic = @{@"Point":self.scoreTF.text,@"token":single.WDToken,@"UserId":self.scanResult};
//    }else{
//        dic = @{@"Point":self.scoreTF.text,@"token":single.WDToken};
//    }
//    [SVProgressHUD show];
//    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
//        if ([responseObject[@"status"] integerValue]==1) {
//            if(!isScan){
//                NSString * data = responseObject[@"data"];
//                [self showCode:data];
//            }else{
//                [Mytools warnText:@"赠送成功" status:Success];
//            }
//        }else{
//            [Mytools warnText:responseObject[@"msg"] status:Error];
//        }
//        [SVProgressHUD dismiss];
//    } failure:^(NSError *error) {
//        [SVProgressHUD dismiss];
//    }];
}
- (IBAction)scanBtnClick:(id)sender {
    if ([self.scoreTF.text integerValue]>self.currentScore) {
        return;
    }
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    SGQRCodeScanningVC *vc = [[SGQRCodeScanningVC alloc] init];
                    vc.returnCodeStrBlock = ^(NSString *codeStr) {
                        self.scanResult = codeStr;
                        [self sendRequest:YES];
                    };
                    [self presentViewController:vc animated:YES completion:nil];
                });
                NSLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
            } else {
                NSLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
            }
        }];
    } else if (status == AVAuthorizationStatusAuthorized) { // 用户允许当前应用访问相机
        SGQRCodeScanningVC *vc = [[SGQRCodeScanningVC alloc] init];
        vc.returnCodeStrBlock = ^(NSString *codeStr) {
            self.scanResult = codeStr;
            [self sendRequest:YES];
        };
        [self presentViewController:vc animated:YES completion:nil];
    } else if (status == AVAuthorizationStatusDenied) { // 用户拒绝当前应用访问相机
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请去-> [设置 - 隐私 - 相机 - SGQRCodeExample] 打开访问开关" preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertC addAction:alertA];
        [self presentViewController:alertC animated:YES completion:nil];
    } else if (status == AVAuthorizationStatusRestricted) {
        NSLog(@"因为系统原因, 无法访问相册");
    }
}

-(void)showCode:(NSString*)data{    
    UIImage * dataImg = [SGQRCodeGenerateManager generateWithDefaultQRCodeData:data imageViewWidth:200];
    [self.view addSubview:self.shelterView];
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"GivingIntegralShareView" owner:nil options:nil];
    _shareV = obj.firstObject;
    _shareV.img = dataImg;
    _shareV.shareUrl = data;
    _shareV.score = self.scoreTF.text;
    _shareV.frame = Frame(0, IPHONE_HEIGHT, IPHONE_WIDTH, 500);
    [self.shelterView addSubview:_shareV];
    [UIView animateWithDuration:0.4 animations:^{
        self->_shareV.y = IPHONE_HEIGHT-500;
    }];
}

-(UIView*)shelterView{
    if (_shelterView == nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    }
    return _shelterView;
}
@end
