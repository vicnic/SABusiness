//
//  MyPointDetailModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/20.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TotalIntegrationModel,CellListModel;
@interface MyPointDetailModel : NSObject

@property (nonatomic, strong) TotalIntegrationModel *integration;

@property (nonatomic, strong) NSArray <CellListModel*>*list;

@end
@interface TotalIntegrationModel : NSObject

@property (nonatomic, assign) NSInteger unused;

@property (nonatomic, assign) NSInteger expired;

@property (nonatomic, assign) NSInteger freeze;

@property (nonatomic, assign) NSInteger used;

@end

@interface CellListModel : NSObject

@property (nonatomic, assign) NSInteger status;

@property (nonatomic, copy) NSString *account;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, copy) NSString *created_at;

@property (nonatomic, assign) NSInteger is_expired;

@end



NS_ASSUME_NONNULL_END
