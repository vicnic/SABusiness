//
//  OrderCommentVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/24.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"
#import "PersonOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^ReloadBlock)(void);
@interface OrderCommentVC : SuperViewController
@property(nonatomic,retain)PersonOrderListModel * model;
@property(nonatomic,copy)ReloadBlock reloadBlock;
@end

NS_ASSUME_NONNULL_END
