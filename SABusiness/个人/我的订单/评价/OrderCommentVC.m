//
//  OrderCommentVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/24.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "OrderCommentVC.h"
#import <QMUIKit/QMUIKit.h>
#import "PersonOrderCell.h"
#import "StoreMainViewVC.h"
#import "FMLStarView.h"

@interface OrderCommentVC ()<UITableViewDelegate,UITableViewDataSource,FMLStarViewDelegate,QMUITextViewDelegate>{
    UIButton * _selBtn;
}
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)QMUITextView * remarkTV;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation OrderCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"评价", nil);
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight+10, IPHONE_WIDTH, IPHONE_HEIGHT - kTopHeight - KTabbarSafeBottomMargin - 80-10) style:UITableViewStyleGrouped];
    _myTab.delegate = self;
    _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    _myTab.dataSource = self;
    [self.view addSubview:self.myTab];
    self.dataArr = [NSMutableArray new];
    [self createUI];
}
-(void)createUI{
    UIView * bgView = [[UIView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-80-KTabbarSafeBottomMargin, IPHONE_WIDTH, 80)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:NSLocalizedString(@"提交", nil) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    btn.backgroundColor = ThemeColor;
    btn.cornerRadius = 5;
    [btn addTarget:self action:@selector(submitBtnClilck) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:btn];
    btn.sd_layout.topSpaceToView(bgView, 10).leftSpaceToView(bgView, 15).rightSpaceToView(bgView, 15).heightIs(45);
}
-(void)submitBtnClilck{
    NSString * url = [WorkUrl returnURL:Interface_For_PostCommitComment];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:self.model.order_id forKey:@"order_id"];
    [dic setObject:[Mytools dictionaryToJson:self.dataArr] forKey:@"comment"];
    
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:NSLocalizedString(@"操作成功", nil) status:Success];
            if(self.reloadBlock){
                self.reloadBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.model.order_list.count ;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PersonOrderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[PersonOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    PersonOrderList * mo = self.model.order_list[indexPath.section];
    cell.model = mo;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PersonOrderList * mo = self.model.order_list[indexPath.section];
    CGFloat height = [tableView cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[PersonOrderCell class] contentViewWidth:IPHONE_WIDTH];
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 170;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section<self.model.order_list.count) {
        UIView* headView = [UIView new];
        UIView * bgView  = [UIView new];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.frame = Frame(10, 0, IPHONE_WIDTH-20, section<self.model.order_list.count?45:10);
        [headView addSubview:bgView];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = bgView.bounds;
        maskLayer.path = maskPath.CGPath;
        bgView.layer.mask = maskLayer;
        
        UILabel * nameLab = [UILabel labelWithTitle:self.model.shop_name color:[UIColor blackColor] fontSize:16];
        [nameLab addTarget:self action:@selector(gotoShopMainView)];
        [bgView addSubview:nameLab];
        nameLab.sd_layout.leftSpaceToView(bgView, 10).centerYEqualToView(bgView).heightIs(20);
        [nameLab setSingleLineAutoResizeWithMaxWidth:300];
        return headView;
    }
    return nil;
    
}
-(void)gotoShopMainView{
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = self.model.shop_id;;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)faceBtnClick:(UIButton*)sender{
    sender.selected = !sender.selected;
    if (!_selBtn) {
        _selBtn = sender;
    }else{
        _selBtn.selected = !_selBtn.selected;
        _selBtn = sender;
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView* headView = [UIView new];
    UIView * bgView  = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = Frame(10, 0, IPHONE_WIDTH-20, 170);
    [headView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 160) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    UILabel * lab = [UILabel labelWithTitle:NSLocalizedString(@"服务评价", nil) color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    lab.frame = Frame(15, 10 , [NSLocalizedString(@"服务评价", nil) sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 20)].width, 20);
    [bgView addSubview:lab];
    FMLStarView * starView = [[FMLStarView alloc] initWithFrame:Frame(CGRectGetMaxX(lab.frame), lab.center.y - 12.5, 150, 25) numberOfStars:5 isTouchable:YES index:section+1];
    starView.currentScore = 0;
    starView.totalScore = 5;
    starView.isFullStarLimited = YES;
    starView.delegate = self;
    [bgView addSubview:starView];
    
    UIView * barView = [UIView new];
    barView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:barView];
    barView.sd_layout.leftEqualToView(bgView).rightEqualToView(bgView).bottomEqualToView(bgView).heightIs(10);
    QMUITextView * tv = [[QMUITextView alloc]initWithFrame:Frame(10, CGRectGetMaxY(starView.frame)+10, IPHONE_WIDTH-40, 110)];
    tv.layer.borderColor = [UIColor colorWithHue:0 saturation:0 brightness:0.9 alpha:1].CGColor;
    tv.layer.borderWidth =1;
    tv.cornerRadius = 5;
    tv.delegate = self;
    tv.placeholder = NSLocalizedString(@"请输入评价内容", nil);
    tv.tag = section + 100;
    [bgView addSubview:tv];
    return headView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSInteger section  = textView.tag - 100;
    PersonOrderList * mo = self.model.order_list[section];
    BOOL isFind = NO;
    for (NSMutableDictionary * dic  in self.dataArr) {
        NSInteger goodid = [dic[@"goods_id"] integerValue];
        if (goodid == mo.goods_id) {
            dic[@"comment"] = textView.text;
            isFind = YES;
            break;
        }
    }
    if (isFind == NO) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setObject:@(mo.goods_id) forKey:@"goods_id"];
        [dic setObject:textView.text forKey:@"comment"];
        [self.dataArr addObject:dic];
    }
}
#pragma mark 星代理
- (void)fml_didClickStarViewByScore:(CGFloat)score atIndex:(NSInteger)index{
    PersonOrderList * mo = self.model.order_list[index - 1];
    BOOL isFind = NO;
    for (NSMutableDictionary * dic  in self.dataArr) {
        NSInteger goodid = [dic[@"goods_id"] integerValue];
        if (goodid == mo.goods_id) {
            dic[@"star"] = @(score);
            isFind = YES;
            break;
        }
    }
    if (isFind == NO) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setObject:@(mo.goods_id) forKey:@"goods_id"];
        [dic setObject:@(score) forKey:@"star"];
        [self.dataArr addObject:dic];
    }
}
@end
