//
//  PersonOrderCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/23.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonOrderCell : UITableViewCell
@property(nonatomic,retain)PersonOrderList* model;
@end

NS_ASSUME_NONNULL_END
