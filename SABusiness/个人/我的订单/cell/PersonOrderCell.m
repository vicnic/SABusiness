//
//  PersonOrderCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/23.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "PersonOrderCell.h"
#import "WuLiuInfoView.h"
#import "OrderCommentVC.h"
@interface PersonOrderCell()
@property (retain, nonatomic) UIImageView *iconView;
@property (retain, nonatomic) UILabel *priceLab;
@property (retain, nonatomic) UILabel *titleLab;
@property (retain, nonatomic) UILabel *descLab;
@property (retain, nonatomic) UILabel *pointLab;
@property (retain, nonatomic) UILabel *totalLab,*countLab;
@property(nonatomic,retain)UIView * shelterView,*bgView;
@end
@implementation PersonOrderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.bgView = [UIView new];
    _bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.bgView];
    _bgView.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 0).rightSpaceToView(self.contentView, 10);
    
    self.iconView = [UIImageView new];
    [_bgView addSubview:self.iconView];
    _iconView.sd_layout.leftSpaceToView(_bgView, 10).topSpaceToView(_bgView, 10).widthIs(60).heightIs(60);
    
    self.priceLab = [UILabel labelWithTitle:@"" color:ThemeColor fontSize:14];
    [_bgView addSubview:self.priceLab];
    _priceLab.sd_layout.rightSpaceToView(_bgView, 10).topEqualToView(self.iconView).heightIs(18);
    [_priceLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.titleLab = [UILabel labelWithTitle:@"多肉植物蛋白质" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [_bgView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.iconView, 10).topEqualToView(self.iconView).rightSpaceToView(_priceLab, 10).autoHeightRatio(0);
    
    self.countLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:12];
    [self.bgView addSubview:self.countLab];
    _countLab.sd_layout.rightEqualToView(_priceLab).topSpaceToView(_priceLab, 10).heightIs(14);
    [_countLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.descLab = [UILabel labelWithTitle:@"家用美观，防辐射" color:[UIColor darkGrayColor] fontSize:12 alignment:NSTextAlignmentLeft];
    [_bgView addSubview:self.descLab];
    _descLab.sd_layout.leftEqualToView(self.titleLab).topSpaceToView(self.titleLab, 8).rightSpaceToView(_countLab, 10).autoHeightRatio(0);
    
    self.pointLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    [_bgView addSubview:self.pointLab];
    _pointLab.sd_layout.leftEqualToView(_descLab).topSpaceToView(_descLab, 10).heightIs(15);
    [_pointLab setSingleLineAutoResizeWithMaxWidth:300];
    
    [_bgView setupAutoHeightWithBottomView:_pointLab bottomMargin:10];
    [self setupAutoHeightWithBottomView:_bgView bottomMargin:0];
}

- (void)setModel:(PersonOrderList *)model{
    [self.iconView sd_setImageWithURL:URL(model.goods_thumb) placeholderImage:PlaceHolderImg];
    NSString * moneyStr =model.goods_price;
    self.priceLab.text = moneyStr;
    self.titleLab.text = model.goods_name;
    self.descLab.text = model.goods_introduction;
    NSString * pont = NSLocalizedString(@"数量", nil);
    self.countLab.text = [NSString stringWithFormat:@"%@:%ld",pont,model.goods_count];
    NSString * total = NSLocalizedString(@"共计", nil);
    self.pointLab.text = [NSString stringWithFormat:@"%@:%@",total,@"555"];
}
@end
