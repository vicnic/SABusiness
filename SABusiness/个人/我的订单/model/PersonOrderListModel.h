//
//Created by ESJsonFormatForMac on 20/03/04.
//

#import <Foundation/Foundation.h>

@class PersonOrderList;
@interface PersonOrderListModel : NSObject

@property (nonatomic, copy) NSString *deduct_integration;

@property (nonatomic, assign) NSInteger total_integration;

@property (nonatomic, copy) NSString *total_price;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, strong) NSArray *order_list;

@property (nonatomic, copy) NSString *order_id;

@property (nonatomic, copy) NSString *shop_thumb;

@property (nonatomic, assign) NSInteger address_id;

@property (nonatomic, assign) NSInteger order_status;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, copy) NSString *shop_description;

@property (nonatomic, copy) NSString *account;

@property (nonatomic, assign) NSInteger is_forbidden;

@property (nonatomic, assign) NSInteger focus_count;

@property (nonatomic, assign) NSInteger shop_id;

@end
@interface PersonOrderList : NSObject

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *goods_thumb;

@property (nonatomic, assign) NSInteger shop_id;

@property (nonatomic, copy) NSString *goods_currency;

@property (nonatomic, copy) NSString *goods_introduction;

@property (nonatomic, assign) NSInteger goods_total_integration;

@property (nonatomic, copy) NSString *goods_tags;

@property (nonatomic, assign) NSInteger goods_count;

@property (nonatomic, assign) NSInteger goods_integration;

@property (nonatomic, assign) NSInteger goods_id;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, assign) NSInteger goods_sales_count;

@property (nonatomic, copy) NSString *goods_total_price;

@property (nonatomic, copy) NSString *goods_cate;

@property (nonatomic, assign) NSInteger is_recommend;

@end

