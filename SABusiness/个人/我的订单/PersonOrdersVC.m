//
//  PersonOrdersVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/23.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "PersonOrdersVC.h"
#import "PersonOrderCell.h"
#import "WuLiuInfoView.h"
#import "StoreMainViewVC.h"
#import "SendPayedPicView.h"
#import "OrderCommentVC.h"
#import "YBPopupMenu.h"

@interface PersonOrdersVC ()<UITableViewDelegate,UITableViewDataSource,YBPopupMenuDelegate>{
    NSInteger _page,_popType;
}
@property(nonatomic,retain)UIView * shelterView;
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation PersonOrdersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"个人", nil);
    self.dataArr = [NSMutableArray new];
    _popType = 6;//默认已完成
    if (self.type == 5) {
        self.rightBtnImage = Image(@"筛选");
    }
    
    _page = 1;
    [self.view addSubview:self.myTab];
    [self getData];
}
- (void)rightBtnClick{
    NSArray * arr = @[NSLocalizedString(@"已完成", nil),NSLocalizedString(@"已取消", nil),NSLocalizedString(@"交易中", nil)];
    CGFloat maxw = 0;
    for (NSString * s  in arr) {
        CGFloat w = [s sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(IPHONE_WIDTH, 15)].width;
        if (w>maxw) {
            maxw = w;
        }
    }
    [YBPopupMenu showRelyOnView:self.rightBtn titles:arr icons:nil menuWidth:maxw + 32 otherSettings:^(YBPopupMenu *popupMenu) {
        popupMenu.delegate = self;
    }];
}
-(void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index{
    if (index ==0) {
        _popType = 6;
    }else if (index==1){
        _popType = 5;
    }else if (index == 2){
        _popType = 7;
    }
    [self.dataArr removeAllObjects];
    _page = 1;
    [self getData];
}
-(void)getData{
    NSString * url = @"";
    if (self.type == 1) {
        url = [WorkUrl returnURL:Interface_For_GetWaitPayList];
    }else if (self.type==2){
        url = [WorkUrl returnURL:Interface_For_GetWaitSendList];
    }else if (self.type==3){
        url = [WorkUrl returnURL:Interface_For_GetWaitReceiveList];
    }else if (self.type==4){
        url = [WorkUrl returnURL:Interface_For_GetWaitCommentList];
    }else if (self.type==5){
        if (_popType == 5) {
            url = [WorkUrl returnURL:Interface_For_GetCanceledOrderList];
        }else if (_popType == 6){
            url = [WorkUrl returnURL:Interface_For_GetHasCompleteList];
        }else{
            url = [WorkUrl returnURL:Interface_For_GetTradingOrderList];
        }
    }
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(_page) forKey:@"page"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myTab.mj_header endRefreshing];
        [self.myTab.mj_footer endRefreshing];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [PersonOrderListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myTab reloadData];
            [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    PersonOrderListModel * mo = self.dataArr[section];
    return mo.order_list.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PersonOrderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[PersonOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    PersonOrderListModel * model = self.dataArr[indexPath.section];
    PersonOrderList * mo = model.order_list[indexPath.row];
    cell.model = mo;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PersonOrderListModel * model = self.dataArr[indexPath.section];
    PersonOrderList * mo = model.order_list[indexPath.row];
    CGFloat height = [tableView cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[PersonOrderCell class] contentViewWidth:IPHONE_WIDTH];
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return _type>4?20:55;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    PersonOrderListModel * mo = self.dataArr[section];
    UIView* headView = [UIView new];
    UIView * bgView  = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = Frame(10, 0, IPHONE_WIDTH-20, 45);
    [headView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    UILabel * nameLab = [UILabel labelWithTitle:mo.shop_name color:[UIColor blackColor] fontSize:16];
    [nameLab addTarget:self action:@selector(gotoShopMainView:)];
    nameLab.tag = section + 1000;
    [bgView addSubview:nameLab];
    nameLab.sd_layout.leftSpaceToView(bgView, 10).centerYEqualToView(bgView).heightIs(20);
    [nameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UILabel* statueLab = [UILabel labelWithTitle:[self getStatue:mo.order_status] color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:statueLab];
    statueLab.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(nameLab).heightIs(16);
    [statueLab setSingleLineAutoResizeWithMaxWidth:300];
    
    return  headView;
}
-(NSString *)getStatue:(NSInteger)type{
    return type==1?NSLocalizedString(@"待付款", nil):type==2?NSLocalizedString(@"待发货", nil):type==3?NSLocalizedString(@"待收货", nil):type==4?NSLocalizedString(@"待评价", nil):type==5?NSLocalizedString(@"已取消", nil):type==6?NSLocalizedString(@"已完成", nil):NSLocalizedString(@"交易中", nil);
}
-(void)gotoShopMainView:(UITapGestureRecognizer*)tap{
    PersonOrderListModel * mo = self.dataArr[tap.view.tag - 1000];
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = mo.shop_id;
    [self.navigationController pushViewController:vc animated:YES];
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * footerView = [UIView new];
    UIView * bgView = [[UIView alloc]initWithFrame:Frame(10, 0, IPHONE_WIDTH-20, 45)];
    bgView.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, _type>4?10:45) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    if (_type == 1) {
        NSArray * arr = @[NSLocalizedString(@"付款", nil),NSLocalizedString(@"取消订单", nil)];
        UIButton * lastBtn = nil;
        for (int i =0;i<arr.count;i++) {
            NSString * name = arr[i];
            CGFloat w = [name sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 30)].width;
            CGFloat btnW = w + 15*2;
            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:name forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            btn.tag = section + 1;
            [btn addTarget:self action:@selector(waitPayClick:) forControlEvents:UIControlEventTouchUpInside];
            [btn setTitleColor:i==0?ThemeColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            btn.layer.borderColor = i==0?ThemeColor.CGColor:[UIColor lightGrayColor].CGColor;
            btn.layer.borderWidth = 1;
            btn.cornerRadius = 15;
            [bgView addSubview:btn];
            btn.sd_layout.rightSpaceToView(lastBtn==nil?bgView:lastBtn, 10).centerYEqualToView(bgView).heightIs(30).widthIs(btnW);
            lastBtn = btn;
        }
    }else if (_type == 2){
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * str = NSLocalizedString(@"提醒卖家", nil);
        [btn setTitle:str forState:UIControlStateNormal];
        [btn  setTitleColor:ThemeColor forState:UIControlStateNormal];
        btn.layer.borderColor = ThemeColor.CGColor;
        btn.layer.borderWidth = 1;
        btn.cornerRadius = 15;
        [btn addTarget:self action:@selector(alertSeller) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [bgView addSubview:btn];
        btn.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(bgView);
        [btn setupAutoSizeWithHorizontalPadding:15 buttonHeight:30];
    }else if(_type==3){
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * str = NSLocalizedString(@"物流信息", nil);
        [btn setTitle:str forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(showWuLiuInfo) forControlEvents:UIControlEventTouchUpInside];
        [btn  setTitleColor:ThemeColor forState:UIControlStateNormal];
        btn.layer.borderColor = ThemeColor.CGColor;
        btn.layer.borderWidth = 1;
        btn.cornerRadius = 15;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [bgView addSubview:btn];
        btn.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(bgView);
        [btn setupAutoSizeWithHorizontalPadding:15 buttonHeight:30];
        
        UIButton * confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * receive = NSLocalizedString(@"确认收货", nil);
        [confirmBtn setTitle:receive forState:UIControlStateNormal];
        [confirmBtn addTarget:self action:@selector(confirmReceiveClick:) forControlEvents:UIControlEventTouchUpInside];
        [confirmBtn  setTitleColor:ThemeColor forState:UIControlStateNormal];
        confirmBtn.layer.borderColor = ThemeColor.CGColor;
        confirmBtn.layer.borderWidth = 1;
        confirmBtn.tag = section + 1;
        confirmBtn.cornerRadius = 15;
        confirmBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [bgView addSubview:confirmBtn];
        confirmBtn.sd_layout.rightSpaceToView(btn, 10).centerYEqualToView(bgView);
        [confirmBtn setupAutoSizeWithHorizontalPadding:15 buttonHeight:30];
    }else if (_type==4){
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * str = NSLocalizedString(@"评价", nil);
        [btn setTitle:str forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(commentAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn  setTitleColor:ThemeColor forState:UIControlStateNormal];
        btn.layer.borderColor = ThemeColor.CGColor;
        btn.layer.borderWidth = 1;
        btn.tag = section + 1;
        btn.cornerRadius = 15;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [bgView addSubview:btn];
        btn.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(bgView);
        [btn setupAutoSizeWithHorizontalPadding:15 buttonHeight:30];
    }
    UIView * barView = [[UIView alloc]initWithFrame:Frame(0, _type>4?10:45, IPHONE_WIDTH, 10)];
    barView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:barView];
    return footerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)confirmReceiveClick:(UIButton*)sender{
    PersonOrderListModel * mo = self.dataArr[sender.tag -1];
    NSString * url = [WorkUrl returnURL:Interface_For_ConfirmReceivieGoods];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:mo.order_id forKey:@"order_id"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:NSLocalizedString(@"操作成功", nil) status:Success];
            [self.myTab.mj_header beginRefreshing];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)waitPayClick:(UIButton*)sender{
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"付款", nil)]) {
        [self haspayedBtnClick:@""];//等后台orderid
    }else{
        //取消订单
    }
}
- (void)haspayedBtnClick:(NSString*)orderid{
    UIWindow * w = [UIApplication sharedApplication].delegate.window;
    [w addSubview:self.shelterView];
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"SendPayedPicView" owner:nil options:nil];
    SendPayedPicView * v = obj.firstObject;
    v.btnBlock = ^(NSInteger type) {
        if (type==0) {
            [self.shelterView removeFromSuperview];
            self.shelterView = nil;
        }else{
            [self.shelterView removeFromSuperview];
            self.shelterView = nil;
        }
    };
    [self.shelterView addSubview:v];
    v.sd_layout.leftSpaceToView(_shelterView, 15).rightSpaceToView(_shelterView, 15).centerYEqualToView(_shelterView).heightIs(254);
}
-(void)alertSeller{
    [Mytools warnText:NSLocalizedString(@"已提醒", nil) status:Success];
}
-(void)commentAction:(UIButton*)sender{
    PersonOrderListModel * model = self.dataArr[sender.tag - 1];
    OrderCommentVC * vc = [OrderCommentVC new];
    vc.model = model;
    vc.reloadBlock = ^{
        [self.myTab.mj_header beginRefreshing];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)showWuLiuInfo{
    UIWindow* w = [UIApplication sharedApplication].delegate.window;
    [w addSubview:self.shelterView];
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"WuLiuInfoView" owner:nil options:nil];
    WuLiuInfoView * v = obj.firstObject;
    [_shelterView addSubview:v];
    v.sd_layout.spaceToSuperView(UIEdgeInsetsMake(80, 50, 80, 50));
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self.dataArr removeAllObjects];
            [self getData];
        }];
        _myTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self->_page ++ ;
            [self getData];
        }];
    }
    return _myTab;
}
- (UIView *)shelterView{
    if (_shelterView == nil) {
        _shelterView = [[UIView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, IPHONE_HEIGHT)];
        _shelterView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
        [_shelterView addTarget:self action:@selector(shelterClick)];
    }
    return _shelterView;
}
-(void)shelterClick{
    [_shelterView removeFromSuperview];
    _shelterView = nil;
}
@end
