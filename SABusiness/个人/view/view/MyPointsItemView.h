//
//  MyPointsItemView.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPointsItemView : UIView
@property(nonatomic,copy)NSString * wpayStr, * wsendStr, *wreceiveStr,*wcommonStr, * cancelStr;
@end

NS_ASSUME_NONNULL_END
