//
//  PersonHeaderView.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/19.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "PersonHeaderView.h"
#import "UserDataVC.h"
#import "LoginVC.h"
#import "BXNavicationController.h"
@interface PersonHeaderView()
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *pointLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;

@end
@implementation PersonHeaderView
- (IBAction)editBtnClick:(id)sender {
    UserDataVC * vc = [UserDataVC new];
    [[Mytools currentViewController].navigationController pushViewController:vc animated:YES];
}
-(void)modifyUserAction{
    VicSingleObject * single = [VicSingleObject getInstance];
    NSString * head = [NSString stringWithFormat:@"%@%@",MainDomain,single.userInfoModel.avatar];
    [self.iconView sd_setImageWithURL:URL(head) placeholderImage:PlaceHolderImg];
    self.nameLab.text = single.userInfoModel.nickname.length==0?@"":single.userInfoModel.nickname;
}
- (void)awakeFromNib{
    [super awakeFromNib];
    [self getData];
    self.topHeight.constant = kStatusBarHeight + 10;
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(modifyUserAction) name:@"modifyuserinfo" object:nil];
    NSString * token = [DLUserDefaultModel userDefaultsModel].token;
    if (token.length==0) {
        self.iconView.image = PlaceHolderImg;
        self.nameLab.text = NSLocalizedString(@"尚未登录", nil);
        self.phoneLab.text =@"";
        self.phoneLab.text = @"0";
        UIButton * loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:loginBtn];
        loginBtn.sd_layout.spaceToSuperView(UIEdgeInsetsMake(0, 0, 0, 0));
    }else{
        VicSingleObject * single = [VicSingleObject getInstance];
        self.nameLab.text = single.userInfoModel.nickname.length==0?@"":single.userInfoModel.nickname;
        self.phoneLab.text = single.userInfoModel.mobile;
        self.pointLab.text = @"";
        NSString * head = [NSString stringWithFormat:@"%@%@",MainDomain,single.userInfoModel.avatar];
        [self.iconView sd_setImageWithURL:URL(head) placeholderImage:PlaceHolderImg];
    }
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetUserPointList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSInteger pint = [responseObject[@"data"][@"integration"][@"unused"] integerValue];
            self.pointLab.text =[NSString stringWithFormat:@"%ld",pint];
        }else{
//            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
    }];
}
-(void)loginBtnClick{
    LoginVC * vc = [LoginVC new];
    BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
    [[self getCurrentViewController]presentViewController:nav animated:YES completion:nil];
}
@end
