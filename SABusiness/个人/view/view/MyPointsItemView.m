//
//  MyPointsItemView.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MyPointsItemView.h"
#import "PersonOrdersVC.h"
#import "LoginVC.h"
#import "BXNavicationController.h"
@interface MyPointsItemView()
@property (weak, nonatomic) IBOutlet UIButton *waitPayBtn;
@property (weak, nonatomic) IBOutlet UIButton *waitSendBtn;
@property (weak, nonatomic) IBOutlet UIButton *waitReceiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *waitCommonBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property(nonatomic,retain)UILabel * wpayLab,*wsendLab,*wreceiveLab,*wCommonLab,*cancelLab;
@end
@implementation MyPointsItemView
- (void)awakeFromNib{
    [super awakeFromNib];
    self.wpayLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    self.wpayLab.cornerRadius = 7;
    self.wpayLab.hidden = YES;
    self.wpayLab.layer.borderWidth = 1;
    self.wpayLab.layer.borderColor = [UIColor redColor].CGColor;
    [self addSubview:self.wpayLab];
    _wpayLab.sd_layout.centerXEqualToView(self.waitPayBtn).offset(18).topEqualToView(self.waitPayBtn).offset(5).widthIs(14).heightEqualToWidth();
    
    self.wsendLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    self.wsendLab.cornerRadius = 7;
    self.wsendLab.hidden = YES;
    self.wsendLab.layer.borderWidth = 1;
    self.wsendLab.layer.borderColor = [UIColor redColor].CGColor;
    [self addSubview:self.wsendLab];
    _wsendLab.sd_layout.centerXEqualToView(self.waitSendBtn).offset(18).topEqualToView(self.waitSendBtn).offset(5).widthIs(14).heightEqualToWidth();
    
    self.wreceiveLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    self.wreceiveLab.cornerRadius = 7;
    self.wreceiveLab.hidden = YES;
    self.wreceiveLab.layer.borderWidth = 1;
    self.wreceiveLab.layer.borderColor = [UIColor redColor].CGColor;
    [self addSubview:self.wreceiveLab];
    _wreceiveLab.sd_layout.centerXEqualToView(self.waitReceiveBtn).offset(18).topEqualToView(self.waitReceiveBtn).offset(5).widthIs(14).heightEqualToWidth();
    
    self.wCommonLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    self.wCommonLab.cornerRadius = 7;
    self.wCommonLab.hidden = YES;
    self.wCommonLab.layer.borderWidth = 1;
    self.wCommonLab.layer.borderColor = [UIColor redColor].CGColor;
    [self addSubview:self.wCommonLab];
    _wCommonLab.sd_layout.centerXEqualToView(self.waitCommonBtn).offset(18).topEqualToView(self.waitCommonBtn).offset(5).widthIs(14).heightEqualToWidth();
    
    self.cancelLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    self.cancelLab.cornerRadius = 7;
    self.cancelLab.hidden = YES;
    self.cancelLab.layer.borderWidth = 1;
    self.cancelLab.layer.borderColor = [UIColor redColor].CGColor;
    [self addSubview:self.cancelLab];
    _cancelLab.sd_layout.centerXEqualToView(self.cancelBtn).offset(18).topEqualToView(self.cancelBtn).offset(5).widthIs(14).heightEqualToWidth();
    
}
- (IBAction)seeAllOrderClick:(id)sender {
}
- (IBAction)waitPayClick:(id)sender {
    [self getInOrderVC:1];
}
- (IBAction)waitSend:(id)sender {
    [self getInOrderVC:2];
}
- (IBAction)waitReceiveClick:(id)sender {
    [self getInOrderVC:3];
}
- (IBAction)waitCommonClick:(id)sender {
    [self getInOrderVC:4];
}
- (IBAction)cancelBtnClick:(id)sender {
    [self getInOrderVC:5];
}
-(void)getInOrderVC:(NSInteger)type{
    NSString * token = [DLUserDefaultModel userDefaultsModel].token;
    if (token.length==0) {
        [self gotoLoginVC];
        return;
    }
    PersonOrdersVC * vc = [PersonOrdersVC new];
    vc.type = type;
    [[self getCurrentViewController].navigationController pushViewController:vc animated:YES];
}
-(void)gotoLoginVC{
    LoginVC * vc = [LoginVC new];
    BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
    [[self getCurrentViewController] presentViewController:nav animated:YES completion:nil];
}

- (void)setWpayStr:(NSString *)wpayStr{
    if ([wpayStr integerValue]==0) {
        self.wpayLab.hidden = YES;
        return;
    }else{
        self.wpayLab.hidden = NO;
    }
    CGFloat w = [self getStringWidth:wpayStr];
    if (w<14) {
        w = 14;
    }
    _wpayLab.sd_layout.widthIs(w);
    self.wpayLab.text = wpayStr;
}
- (void)setWsendStr:(NSString *)wsendStr{
    if ([wsendStr integerValue]==0) {
        self.wsendLab.hidden = YES;
        return;
    }else{
        self.wsendLab.hidden = NO;
    }
    CGFloat w = [self getStringWidth:wsendStr];
    if (w<14) {
        w = 14;
    }
    _wsendLab.sd_layout.widthIs(w);
    self.wsendLab.text = wsendStr;
}
-(void)setWreceiveStr:(NSString *)wreceiveStr{
    if ([wreceiveStr integerValue]==0) {
        self.wreceiveLab.hidden = YES;
        return;
    }else{
        self.wreceiveLab.hidden = NO;
    }
    CGFloat w = [self getStringWidth:wreceiveStr];
    if (w<14) {
        w = 14;
    }
    _wreceiveLab.sd_layout.widthIs(w);
    self.wreceiveLab.text = wreceiveStr;
}
- (void)setWcommonStr:(NSString *)wcommonStr{
    if ([wcommonStr integerValue]==0) {
        self.wCommonLab.hidden = YES;
        return;
    }else{
        self.wCommonLab.hidden = NO;
    }
    CGFloat w = [self getStringWidth:wcommonStr];
    if (w<14) {
        w = 14;
    }
    _wCommonLab.sd_layout.widthIs(w);
    self.wCommonLab.text = wcommonStr;
}
- (void)setCancelStr:(NSString *)cancelStr{
    if ([cancelStr integerValue]==0) {
        self.cancelLab.hidden = YES;
        return;
    }else{
        self.cancelLab.hidden = NO;
    }
    CGFloat w = [self getStringWidth:cancelStr];
    if (w<14) {
        w = 14;
    }
    _cancelLab.sd_layout.widthIs(w);
    self.cancelLab.text = cancelStr;
}
-(CGFloat)getStringWidth:(NSString*)str{
    return [str sizeWithFont:[UIFont systemFontOfSize:12] maxSize:CGSizeMake(300, 20)].width;
}
@end
