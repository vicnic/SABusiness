//
//  AddressListCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/20.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "AddressListCell.h"
#import "AddressInfoVC.h"
@interface AddressListCell()
@property(nonatomic,retain)UILabel * nameLab, * phoneLab, * morenLab, * detailLab;
@property(nonatomic,retain)UIButton * editBtn;
@end
@implementation AddressListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    UIView * bgView = [UIView new];
    bgView.cornerRadius = 5;
    bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 10).rightSpaceToView(self.contentView, 10);
    
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:self.nameLab];
    self.nameLab.sd_layout.leftSpaceToView(bgView, 10).topSpaceToView(bgView, 10).heightIs(16);
    [self.nameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.phoneLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:13];
    [bgView addSubview:self.phoneLab];
    _phoneLab.sd_layout.leftSpaceToView(_nameLab, 10).centerYEqualToView(_nameLab).heightIs(16);
    [_phoneLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.morenLab = [UILabel labelWithTitle:[NSString stringWithFormat:@" %@ ",NSLocalizedString(@"默认", nil)] color:[UIColor redColor] fontSize:12];
    _morenLab.hidden = YES;
    _morenLab.backgroundColor = [UIColor colorWithHue:0.06 saturation:0.07 brightness:0.99 alpha:1];
    [bgView addSubview:self.morenLab];
    _morenLab.sd_layout.centerYEqualToView(_nameLab).rightSpaceToView(bgView, 10).heightIs(15);
    [_morenLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_editBtn setTitle:NSLocalizedString(@"编辑", nil) forState:UIControlStateNormal];
    _editBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_editBtn addTarget:self action:@selector(editBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_editBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [bgView addSubview:self.editBtn];
    _editBtn.sd_layout.rightSpaceToView(bgView, 5).centerYEqualToView(bgView).offset(10);
    [_editBtn setupAutoSizeWithHorizontalPadding:5 buttonHeight:35];
    
    self.detailLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [bgView addSubview:self.detailLab];
    _detailLab.sd_layout.leftEqualToView(_nameLab).rightSpaceToView(_editBtn, 5).topSpaceToView(_nameLab, 10).autoHeightRatio(0);
    
    [bgView setupAutoHeightWithBottomView:self.detailLab bottomMargin:15];
    [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
}
- (void)setModel:(AddressListModel *)model{
    _model = model;
    self.nameLab.text = model.real_name;
    self.phoneLab.text = model.mobile;
    self.detailLab.text = model.detail_address;
    if(model.is_default==1){
        self.morenLab.hidden = NO;
    }else{
        self.morenLab.hidden = YES;
    }
}
-(void)editBtnClick{
    if (self.editBlock) {
        self.editBlock();
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
