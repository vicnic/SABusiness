//
//  AddressListCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/20.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^EditBlock)(void);
@interface AddressListCell : UITableViewCell
@property(nonatomic,retain)AddressListModel * model;
@property(nonatomic,copy)EditBlock editBlock;
@end

NS_ASSUME_NONNULL_END
