//
//  AddressListModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/20.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 {
 "id": 2,
 "real_name": "测试1",
 "account": "iosvicnic",
 "mobile": "238293823",
 "detail_address": "xxxxxxxxxxxxxxxxxxxxxx",
 "postcode": "333",
 "created_at": "1579087646",
 "updated_at": "1579087646"
 }
 */
NS_ASSUME_NONNULL_BEGIN

@interface AddressListModel : NSObject
@property(nonatomic,copy)NSString * real_name,* account,* mobile,*detail_address,*postcode;
@property(nonatomic,assign)NSInteger tid,is_default;
@end

NS_ASSUME_NONNULL_END
