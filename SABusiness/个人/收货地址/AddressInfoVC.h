//
//  AddressInfoVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"
#import "AddressListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^RloadListBlock)(void);
typedef void(^AddressSaveBlock)(void);
@interface AddressInfoVC : SuperViewController
@property(nonatomic,copy)RloadListBlock reloadBlock;
@property(nonatomic,copy)AddressSaveBlock saveBlock;
@property(nonatomic,retain)AddressListModel * model;
@end

NS_ASSUME_NONNULL_END
