//
//  AddressInfoVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/20.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "AddressInfoVC.h"

@interface AddressInfoVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet UILabel *morenLab;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botmargin1;
@property (weak, nonatomic) IBOutlet UITextField *addressTF;
@property (weak, nonatomic) IBOutlet UITextField *postCodeTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
@property (weak, nonatomic) IBOutlet UISwitch *morensw;

@end

@implementation AddressInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"收货地址", nil);
    self.topHeight.constant = kTopHeight+10;
    self.viewWidth.constant = IPHONE_WIDTH;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UIView * botView = [[UIView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-80, IPHONE_WIDTH, 80)];
    botView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:botView];
    UIButton *delBtn;
    CGRect saveFrame = CGRectZero;
    if (self.model) {
        [self configEditModel];
        self.morensw.on = self.model.is_default?YES:NO;
        delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [delBtn setTitle:NSLocalizedString(@"删除", nil) forState:UIControlStateNormal];
        delBtn.cornerRadius = 4;
        [delBtn addTarget:self action:@selector(delBtnClick) forControlEvents:UIControlEventTouchUpInside];
        delBtn.frame = Frame(15, 10, (IPHONE_WIDTH-40)/2.f, 45);
        delBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        delBtn.backgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.9 alpha:1];
        [botView addSubview:delBtn];
        saveFrame = Frame(CGRectGetMaxX(delBtn.frame) + 10, 10, (IPHONE_WIDTH-40)/2.f, 45);
    }else{
        self.morensw.hidden = YES;
        self.morenLab.hidden = YES;
        self.botmargin1.constant = -30;
        saveFrame = Frame(15, 10, IPHONE_WIDTH-30, 45);
    }
    UIButton * saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setTitle:NSLocalizedString(@"保存", nil) forState:UIControlStateNormal];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    saveBtn.backgroundColor = ThemeColor;
    saveBtn.cornerRadius = 4;
    saveBtn.frame = saveFrame;
    [saveBtn addTarget:self action:@selector(saveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [botView addSubview:saveBtn];
    
}
-(void)configEditModel{
    self.nameTF.text = self.model.real_name;
    self.phoneTF.text = self.model.mobile;
    self.addressTF.text = self.model.detail_address;
    self.postCodeTF.text = self.model.postcode;
    
}
- (IBAction)morenSWClick:(id)sender {
    NSString * url = [WorkUrl returnURL:Interface_For_SetAddressDefault];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.model.tid) forKey:@"address_id"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            if(self.reloadBlock){
                self.reloadBlock();
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)delBtnClick{
    NSString * url = [WorkUrl returnURL:Interface_For_GETDelAdress];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.model.tid) forKey:@"address_id"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            if(self.reloadBlock){
                self.reloadBlock();
            }
            [ReuseFile getUserInfo:^(NSDictionary * _Nonnull dic) {
                //刷新地址
                [SVProgressHUD dismiss];
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)saveBtnClick{
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    if (self.nameTF.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"真实姓名不能为空", nil) status:Error];
        return;
    }
    if (self.phoneTF.text.length ==0) {
        [Mytools warnText:NSLocalizedString(@"手机号不能为空", nil) status:Error];
        return;
    }
    if (self.addressTF.text.length ==0) {
        [Mytools warnText:NSLocalizedString(@"详细地址不能为空", nil) status:Error];
        return;
    }
    if (self.postCodeTF.text.length ==0) {
        [Mytools warnText:NSLocalizedString(@"邮编不能为空", nil) status:Error];
        return;
    }
    NSString * url = [WorkUrl returnURL:Interface_For_PostSetAddress];
    [dic setObject:self.nameTF.text forKey:@"real_name"];
    [dic setObject:self.phoneTF.text forKey:@"mobile"];
    [dic setObject:self.addressTF.text forKey:@"detail_address"];
    [dic setObject:self.postCodeTF.text forKey:@"postcode"];
    if (self.model) {
        [dic setObject:@(self.model.tid) forKey:@"id"];
    }
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            if(self.saveBlock){
                self.saveBlock();
            }
            [ReuseFile getUserInfo:^(NSDictionary * _Nonnull dic) {
                //刷新地址
                [SVProgressHUD dismiss];
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

@end
