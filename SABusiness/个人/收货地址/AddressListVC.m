//
//  AddressListVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/20.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "AddressListVC.h"
#import "AddressListCell.h"
#import "AddressInfoVC.h"
@interface AddressListVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)NSMutableArray * dataArr;
@end

@implementation AddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"收货地址", nil);
    self.rightBtnName = NSLocalizedString(@"新增", nil);
    self.rightBtnFontSize = 14;
    self.rightBtnNameColor = [UIColor blackColor];
    self.dataArr = [NSMutableArray new];
    [self.view addSubview:self.myTab];
    [self getData];
}
- (void)rightBtnClick{
    [self pushEditVC:nil];
}
-(void)pushEditVC:(AddressListModel*)mo{
    AddressInfoVC * vc = [AddressInfoVC new];
    if (mo) {
        vc.model = mo;
    }
    vc.saveBlock = ^{
        [self.myTab.mj_header beginRefreshing];
    };
    vc.reloadBlock = ^{
        [self.myTab.mj_header beginRefreshing];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetUserAddressList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myTab.mj_header endRefreshing];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [AddressListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myTab reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressListCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[AddressListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.model = self.dataArr[indexPath.row];
    cell.editBlock = ^{
        [self pushEditVC:self.dataArr[indexPath.row]];
    };
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressListModel * mo = self.dataArr[indexPath.row];
    CGFloat height = [self.myTab cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[AddressListCell class] contentViewWidth:IPHONE_WIDTH];
    return height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self.dataArr removeAllObjects];
            [self getData];
        }];
    }
    return _myTab;
}
@end
