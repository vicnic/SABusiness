//
//  ReuseFile.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/10.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^RequestBackBlock)(NSDictionary * dic);
@interface ReuseFile : NSObject
+(NSMutableDictionary *)getParamDic;
+(void)getUserInfo:(RequestBackBlock)block;
+(NSString*)getErrorString:(NSDictionary*)dic;
+(void)getRongYunToken:(RequestBackBlock)block;
+(void)NeedResetNoViewWithTable:(UITableView*)tab andArr:(NSMutableArray*)arr;
+(void)saveImageToLocal:(UIImage *)img;
@end

NS_ASSUME_NONNULL_END
