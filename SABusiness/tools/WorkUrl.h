//
//  WorkUrl.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/10.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef  enum{
    Interface_For_regist,//注册
    Interface_For_Login,//登录
    Interface_For_RegisterGetSMS,//注册获取验证码
    Interface_For_ForgetPasswordGetVerify,//忘记密码获取验证码
    Interface_For_SubmitResetPassword,//重置密码
    Interface_For_GetMainViewBannerList,//首页banner列表
    Interface_For_GetMainViewRecommentBannerList,//好物推荐
    Interface_For_GetMainViewCateList,//首页分类列表
    Interface_For_GetUserInfo,//用户信息
    Interface_For_GetSearchHotWords,//获取搜索热词
    Interface_For_ModifyUserPassword,//修改密码
    Interface_For_UploadFile,//上传文件
    Interface_For_PostSetUserInfo,//修改用户信息
    Interface_For_GetSystemNotice,//系统公告
    Interface_For_GetUserAddressList,//获取用户地址列表
    Interface_For_PostSetAddress,//设置地址
    Interface_For_GetUserPointList,//获取积分列表
    Interface_For_GetShopMainviewRecommend,//店铺首页的好物推荐
    Interface_For_GetShopMainviewAllGoods,//店铺首页的全部商品
    Interface_For_GetShopInfo,//获取店铺信息
    Interface_For_PostSetShopFocus,//关注、取消关注商铺
    Interface_For_GetMainViewGoodsList,//首页商品列表
    Interface_For_GetCateListData,//获取首页分类点击进入的数据
    Interface_For_PostSearchAllGoods,//搜索所有商品
    Interface_For_PostShopSearchGoods,//搜索商铺商品
    Interface_For_GetRongYunToken,//获取融云token
    Interface_For_GetShopCarList,//获取购物车列表
    Interface_For_SetShoppingCartNum,//购物车内的商品增减
    Interface_For_DeleteGoods,//删除商品
    Interface_For_SearchByClickTag,//搜索页面的tag点击
    Interface_For_SetAddressDefault,//设置默认地址
    Interface_For_PostOrderList,//提交订单
    Interface_For_GetWaitPayList,//待付款列表
    Interface_For_GetWaitSendList,//待发货
    Interface_For_GetWaitReceiveList,//待收货
    Interface_For_GetWaitCommentList,//待评价
    Interface_For_GetHasCompleteList,//已完成
    Interface_For_ConfirmReceivieGoods,//确认收货
    Interface_For_GetCanceledOrderList,//已取消
    Interface_For_GetTradingOrderList,//交易中
    Interface_For_PostCommitComment,//提交评论
    Interface_For_GetGoodsDetailInfo,//获取商品详情
    Interface_For_GetMyFocusStore,//我的关注列表
    Interface_For_GetCornerNumber,//个人中心角标数量
    Interface_For_GETDelAdress,//删除地址
    Interface_For_GETGoodsDetailComment,//商品详情评论
}Interface_Type;
@interface WorkUrl : NSObject
+ (NSString *)returnURL:(Interface_Type)type;
@end

NS_ASSUME_NONNULL_END
