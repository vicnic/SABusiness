//
//  DLDeviceNameFile.h
//  TwoOneEight
//
//  Created by Jinniu on 2019/7/31.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DLDeviceNameFile : NSObject
+ (NSString *)getCurrentDeviceName;
@end

NS_ASSUME_NONNULL_END
