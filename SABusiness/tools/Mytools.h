//
//  Mytools.h
//  7UGame
//
//  Created by 111 on 2017/2/14.
//  Copyright © 2017年 111. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef  enum{
    Common,//普通
    Success,//成功
    Toast,//吐司模式
    Error,//错误
}NoticeType;
typedef void (^SuccessArrBlock)(NSString* unionid);
typedef void (^SuccessBlock)(NSDictionary*backDic);
typedef void (^FailBlock)(NSError * error);
typedef void(^netStateBlock)(NSInteger netState);
@interface Mytools : NSObject
+(BOOL) verifyURL:(NSString *)url;
//用于MD5加密的封装方法
+(NSString *)getMD5code:(NSString *)pswStr;

//获取当前时间戳
+(NSString*)getCurrentTimestamp;

//字符串转日期
+(NSDate*)dateStrToDate:(NSString *)dateStr;

//日期转字符串
+(NSString *)dateToDateStr:(NSDate*)date;

//返回当天的日期
+(NSDate *)getCurrentDate;

//日期大小比较yyyy-MM-dd
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;

//url编码
+(NSString*)encodeString:(NSString*)unencodedString;

//时间戳转时间
+ (NSString *)timeStampTotime:(NSString *)timeString;

//转化为金钱格式
+(NSString *)numberToMoneyFormatter:(long)num;

//网络监测
+(void)AFNetMonitorNet:(netStateBlock)netBlock;

//sv消息弹窗
+(void)warnText:(NSString *)str status:(NoticeType)typeP;

+(void)deleteAllUserDefaules;

//json格式字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//对象转json格式字符串
+ (NSString*)dictionaryToJson:(id)dic;

//数组转字符串
+ (NSString*)ArrtionaryToJson:(NSMutableArray *)arr;

//判断是不是整数
+(BOOL)isPureInt:(NSString*)string;
//判断是不是浮点数
+(BOOL)isPureFloat:(NSString*)string;

+(BOOL)IsChinese:(NSString *)str ;

//返回一个月多少天
+(NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month;

//取主色调
+(UIColor*)mostColor:(UIImage*)image;
+(NSString*)returnDecimalNumStr:(NSString*)numStr;//解决json解析精度丢失的问题

//UIView转UIImage
+(UIImage*)viewBecomeImage:(UIView*)v;
//限制输入两位小数
+(NSString *)getMoneyFormat:(NSString*)money;
//判断推送通知是否开启
+ (BOOL)isUserNotificationEnable;
//非法字符过滤判断
+(BOOL)hasIllegalCharacter:(NSString *)content;
//获取当前视图控制器
+(UIViewController*)currentViewController;
//小数点格式化：如果有两位小数不为0则保留两位小数，如果有一位小数不为0则保留一位小数，否则显示整数
+(NSString *)formatFloat:(float)f;
@end
