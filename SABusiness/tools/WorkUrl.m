//
//  WorkUrl.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/10.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "WorkUrl.h"

@implementation WorkUrl
+ (NSString *)returnURL:(Interface_Type)type{
    NSString *url;
    switch (type) {
        case Interface_For_regist:{
            url = [NSString stringWithFormat:@"%@/shop/common/register",MainDomain];
        }break;
        case Interface_For_Login:{
            url = [NSString stringWithFormat:@"%@/shop/common/login",MainDomain];
        }break;
        case Interface_For_RegisterGetSMS:{
            url = [NSString stringWithFormat:@"%@/shop/common/get-reg-code",MainDomain];
        }break;
        case Interface_For_ForgetPasswordGetVerify:{
            url = [NSString stringWithFormat:@"%@/shop/common/get-forget-code",MainDomain];
        }break;
        case Interface_For_SubmitResetPassword:{
            url = [NSString stringWithFormat:@"%@/shop/common/forget-password",MainDomain];
        }break;
        case Interface_For_GetMainViewBannerList:{
            url = [NSString stringWithFormat:@"%@/shop/common/banner-list",MainDomain];
        }break;
        case Interface_For_GetMainViewRecommentBannerList:{
            url = [NSString stringWithFormat:@"%@/shop/common/banner-recommend",MainDomain];
        }break;
        case Interface_For_GetMainViewCateList:{
            url = [NSString stringWithFormat:@"%@/shop/common/cate-list",MainDomain];
        }break;
        case Interface_For_GetUserInfo:{
            url = [NSString stringWithFormat:@"%@/shop/user/info",MainDomain];
        }break;
        case Interface_For_GetSearchHotWords:{
            url = [NSString stringWithFormat:@"%@/shop/common/hot-search-tag",MainDomain];
        }break;
        case Interface_For_ModifyUserPassword:{
            url = [NSString stringWithFormat:@"%@/shop/user/change-password",MainDomain];
        }break;
        case Interface_For_UploadFile:{
            url = [NSString stringWithFormat:@"%@/shop/user/avatar-upload",MainDomain];
        }break;
        case Interface_For_PostSetUserInfo:{
            url = [NSString stringWithFormat:@"%@/shop/user/set-user-profile",MainDomain];
        }break;
        case Interface_For_GetSystemNotice:{
            url = [NSString stringWithFormat:@"%@/shop/common/system-notice",MainDomain];
        }break;
        case Interface_For_GetUserAddressList:{
            url = [NSString stringWithFormat:@"%@/shop/user/get-user-address",MainDomain];
        }break;
        case Interface_For_PostSetAddress:{
            url = [NSString stringWithFormat:@"%@/shop/user/set-user-address",MainDomain];
        }break;
        case Interface_For_GetUserPointList:{
            url = [NSString stringWithFormat:@"%@/shop/user/get-user-integration",MainDomain];
        }break;
        case Interface_For_GetShopMainviewRecommend:{
            url = [NSString stringWithFormat:@"%@/shop/common/get-store-recommend",MainDomain];
        }break;
        case Interface_For_GetShopMainviewAllGoods:{
            url = [NSString stringWithFormat:@"%@/shop/common/get-store-goods-list",MainDomain];
        }break;
        case Interface_For_GetShopInfo:{
            url = [NSString stringWithFormat:@"%@/shop/user/get-store-info",MainDomain];
        }break;
        case Interface_For_PostSetShopFocus:{
            url = [NSString stringWithFormat:@"%@/shop/user/set-store-focus",MainDomain];
        }break;
        case Interface_For_GetMainViewGoodsList:{
            url = [NSString stringWithFormat:@"%@/shop/common/goods-recommend",MainDomain];
        }break;
        case Interface_For_GetCateListData:{
            url = [NSString stringWithFormat:@"%@/shop/common/cate-goods-list",MainDomain];
        }break;
        case Interface_For_PostSearchAllGoods:{
            url = [NSString stringWithFormat:@"%@/shop/user/search-goods",MainDomain];
        }break;
        case Interface_For_PostShopSearchGoods:{
            url = [NSString stringWithFormat:@"%@/shop/user/search-store-goods",MainDomain];
        }break;
        case Interface_For_GetRongYunToken:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/get-rong-token",MainDomain];
        }break;
        case Interface_For_GetShopCarList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/get-cart-list",MainDomain];
        }break;
        case Interface_For_SetShoppingCartNum:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/set-goods-cart",MainDomain];
        }break;
        case Interface_For_DeleteGoods:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/del-goods-cart",MainDomain];
        }break;
        case Interface_For_SearchByClickTag:{
            url = [NSString stringWithFormat:@"%@/shop/user/hot-search",MainDomain];
        }break;
        case Interface_For_SetAddressDefault:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/set-default-address",MainDomain];
        }break;
        case Interface_For_PostOrderList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order-submit",MainDomain];
        }break;
        case Interface_For_GetWaitPayList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/wait-pay",MainDomain];
        }break;
        case Interface_For_GetWaitSendList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/wait-send",MainDomain];
        }break;
        case Interface_For_GetWaitReceiveList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/already-send",MainDomain];
        }break;
        case Interface_For_GetWaitCommentList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/wait-comment",MainDomain];
        }break;
        case Interface_For_GetHasCompleteList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/complete",MainDomain];
        }break;
        case Interface_For_ConfirmReceivieGoods:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/confirm-receipt",MainDomain];
        }break;
        case Interface_For_GetCanceledOrderList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/cancel",MainDomain];
        }break;
        case Interface_For_GetTradingOrderList:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/refund",MainDomain];
        }break;
        case Interface_For_PostCommitComment:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/order/comment",MainDomain];
        }break;
        case Interface_For_GetGoodsDetailInfo:{
            url = [NSString stringWithFormat:@"%@/shop/common/get-goods-detail",MainDomain];
        }break;
        case Interface_For_GetMyFocusStore:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/my-focus-list",MainDomain];
        }break;
        case Interface_For_GetCornerNumber:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/my-order-count",MainDomain];
        }break;
        case Interface_For_GETDelAdress:{
            url = [NSString stringWithFormat:@"%@/shop/user/auth/del-user-address",MainDomain];
        }break;
        case Interface_For_GETGoodsDetailComment:{
            url = [NSString stringWithFormat:@"%@//shop/common/get-goods-commends-list",MainDomain];
        }break;


        default: break;
    }
    return url;
}
@end
