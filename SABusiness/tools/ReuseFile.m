//
//  ReuseFile.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/10.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "ReuseFile.h"
#import "DLDeviceNameFile.h"
#import "UITableView+HD_NoList.h"
@implementation ReuseFile
+(NSMutableDictionary *)getParamDic{
    NSMutableDictionary * p = [NSMutableDictionary new];
    [p setObject:[DLDeviceNameFile getCurrentDeviceName] forKey:@"device"];
    NSString * toke = [DLUserDefaultModel userDefaultsModel].token;
    if (toke.length) {
        [p setObject:toke forKey:@"token"];
    }
    return p;
}
+(void)saveImageToLocal:(UIImage *)img{
    ReuseFile * file = [ReuseFile new];
    UIImageWriteToSavedPhotosAlbum(img, file, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error == nil) {
        [Mytools warnText:@"图片保存成功" status:Success];
    }else{
        [Mytools warnText:@"图片保存失败" status:Error];
    }
}
+(NSString*)getErrorString:(NSDictionary*)dic{
    NSInteger code = [dic[@"code"] integerValue];
    if(code == 422){
        NSDictionary * dataDic = dic[@"data"];
        __block NSString * warnStr = @"";
        [dataDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            NSArray * arr = (NSArray*)obj;
            for (NSString * str in arr) {
                if (str.length) {
                    if (warnStr.length) {
                        warnStr = [NSString stringWithFormat:@"%@\n%@",warnStr,str];
                    }else{
                        warnStr = str;
                    }
                }
            }
            if (warnStr.length) {
                *stop = YES;
            }
        }];
        return warnStr;
    }else{
        return dic[@"desc"];
    }
}
+(void)getUserInfo:(RequestBackBlock)block{
    NSString * url =[WorkUrl returnURL:Interface_For_GetUserInfo];
    NSDictionary * dic = @{@"token":[DLUserDefaultModel userDefaultsModel].token};
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            VicSingleObject * single = [VicSingleObject getInstance];
            single.userInfoModel = [SBUserInfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            single.userInfoModel.token = [DLUserDefaultModel userDefaultsModel].token;
            if (block) {
                block(responseObject);
            }
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[self getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
+(void)NeedResetNoViewWithTable:(UITableView*)tab andArr:(NSMutableArray*)arr{
    if (arr.count>0) {
        [tab dismissNoView];
    }else{
        [tab dismissNoView];
        [tab showNoView:@"暂无数据" image:Image(@"暂无消息") certer:CGPointZero];
    }
}
+(void)getRongYunToken:(RequestBackBlock)block;{
    NSString * url = [WorkUrl returnURL:Interface_For_GetRongYunToken];
    NSMutableDictionary * dic = [self getParamDic];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [DLUserDefaultModel userDefaultsModel].token = responseObject[@"data"][@"rong_cloud_token"];
            if (block) {
                block(responseObject);
            }
        }else{
            [Mytools warnText:[self getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
@end
