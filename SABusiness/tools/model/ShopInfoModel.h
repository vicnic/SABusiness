//
//Created by ESJsonFormatForMac on 20/02/28.
//

#import <Foundation/Foundation.h>

@class Shop_Info;
@interface ShopInfoModel : NSObject

@property (nonatomic, assign) NSInteger is_focus;

@property (nonatomic, strong) Shop_Info *shop_info;

@end
@interface Shop_Info : NSObject

@property (nonatomic, copy) NSString *shop_thumb;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger focus_count;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, copy) NSString *shop_description;

@property (nonatomic, assign) NSInteger updated_at;

@end

