//
//  SBUserInfoModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/15.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class AddressObj;
@interface SBUserInfoModel : NSObject

@property (nonatomic, copy) NSString *mobile;

@property (nonatomic, copy) NSString *account;

@property (nonatomic, copy) NSString *avatar;

@property (nonatomic, assign) NSInteger tid;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *rong_cloud_token;

@property (nonatomic, assign) NSInteger birthday;

@property (nonatomic, copy) NSString *email,*token,*platform_bank_num;

@property (nonatomic, strong) AddressObj *address;

@property (nonatomic, copy) NSString *sex;

@end
@interface AddressObj : NSObject

@property (nonatomic, copy) NSString *account;

@property (nonatomic, copy) NSString *mobile;

@property (nonatomic, copy) NSString *detail_address;

@property (nonatomic, assign) NSInteger aid;

@property (nonatomic, assign) NSInteger is_default;

@property (nonatomic, copy) NSString *postcode;

@property (nonatomic, copy) NSString *real_name;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, assign) NSInteger updated_at;

@end
NS_ASSUME_NONNULL_END
