//
//  Mytools.m
//  7UGame
//
//  Created by 111 on 2017/2/14.
//  Copyright © 2017年 111. All rights reserved.
//

#import "Mytools.h"
#import "AFNetworking.h"
#import <CommonCrypto/CommonDigest.h>
#import "PGNetworkHelper+Synchronously.h"
#import <FTIndicator/FTIndicator.h>
static int t_count;
@implementation Mytools

+(void)sendNotifyWithLoginOut:(NSString *)warnStr{
    if(t_count>0){
        return;
    }
    t_count ++ ;
    HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"" andMessage:warnStr];
    [alertView addButtonWithTitle:@"确定" type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
        t_count = 0;
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        NSNotification * message = [[NSNotification alloc]initWithName:LoginOutByOtherLogin object:self userInfo:nil];
        [center postNotification:message];
    }];
    alertView.buttonFont = [UIFont systemFontOfSize:[UIFont buttonFontSize]];
    [alertView show];
}

+(NSString *)getMD5code:(NSString *)pswStr{
    NSString *resultStr = nil;//加密后的结果
    const char *cStr = [pswStr UTF8String];//指针不能变，cStr指针变量本身可以变化
    unsigned char result[16];
    CC_MD5(cStr, (unsigned int)strlen(cStr), result);
    resultStr = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                 result[0], result[1], result[2], result[3],
                 result[4], result[5], result[6], result[7],
                 result[8], result[9], result[10], result[11],
                 result[12], result[13], result[14], result[15]
                 ];
    return resultStr;
}
+(NSString*)getCurrentTimestamp{
    //获取系统当前的时间戳
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
//    NSDate * dat = [self getBeiJingTime];
    NSTimeInterval a=[dat timeIntervalSince1970];
    NSInteger newA = a;
    NSString * timeString = [NSString stringWithFormat:@"%ld", (long)newA];
    return timeString;
}
+ (NSString *)timeStampTotime:(NSString *)timeString
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"shanghai"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* date =[NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSString* dateString = [formatter stringFromDate:date];
    return dateString;
}
+(NSDate *)getCurrentDate{
    //以下为准确时间
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
    return localeDate;
}

+(NSString*)encodeString:(NSString*)unencodedString{
    NSString * encodedStr = (NSString*) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)unencodedString,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
    return encodedStr;
}

//字符串转日期
+(NSDate*)dateStrToDate:(NSString *)dateStr{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date =[dateFormat dateFromString:dateStr];
    NSDate *date2 = [date dateByAddingTimeInterval:8 * 60 * 60];
    return date2;
}
+(NSDate*)dateStrToDate:(NSString *)dateStr withFormat:(NSString*)format{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:[NSString stringWithFormat:@"yyyy-%@ 00:00:00",format]];
    NSDate *date =[dateFormat dateFromString:dateStr];
    NSDate *date2 = [date dateByAddingTimeInterval:8 * 60 * 60];
    return date2;
}
+(NSString *)dateToDateStr:(NSDate*)date{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date2 = [date dateByAddingTimeInterval:-8 * 60 * 60];
    NSString *currentDateStr = [dateFormat stringFromDate:date2];
    return currentDateStr;
}

+(NSString *)numberToMoneyFormatter:(long)num{
    NSNumberFormatter * formatter = [[NSNumberFormatter alloc]init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSString * newNumber = [formatter stringFromNumber:[NSNumber numberWithLong:num]];
    return newNumber;
}
+(void)AFNetMonitorNet:(netStateBlock)netBlock{
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    [mgr startMonitoring];
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        netBlock(status);//0,-1表示无网络
    }];
}


//消息弹窗
+(void)warnText:(NSString *)str status:(NoticeType)typeP{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (typeP == Common) {
            //感叹号
            [FTIndicator showInfoWithMessage:str userInteractionEnable:NO];
        }else if (typeP == Success){
            //success
            [FTIndicator showSuccessWithMessage:str];
        }else if(typeP == Toast){
            [FTIndicator showInfoWithMessage:str];
        }else{
            //error
            [FTIndicator showErrorWithMessage:str];
        }
    });
    
}

+(void)deleteAllUserDefaules{
    NSUserDefaults *defatluts = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [defatluts dictionaryRepresentation];
    for(NSString *key in [dictionary allKeys]){
        if ([key isEqualToString:@"gesturelock"]==NO&&
            [key isEqualToString:@"username"]==NO&&
            [key isEqualToString:@"inputUsername"]==NO&&
            [key isEqualToString:@"gestureFinalSaveKey"]==NO&&
            [key isEqualToString:@"firstLaunch"]==NO&&
            [key isEqualToString:@"gesturestate"]==NO&&
            [key isEqualToString:@"fingerstate"]==NO&&
            [key isEqualToString:@"currentVersion"]==NO&&
            [key isEqualToString:@"rememberPassword"]==NO&&
            [key isEqualToString:@"employeesrememberPassword"]==NO&&
            [key isEqualToString:@"agentUserName"]==NO&&
            [key isEqualToString:@"employeesusername"]==NO&&
            [key isEqualToString:@"employeespassword"]==NO&&
            [key isEqualToString:@"password"]==NO&&
            [key isEqualToString:@"MainDomain"]==NO&&
            [key isEqualToString:@"O2ODomain"]==NO&&
            [key isEqualToString:@"MergeCodeDomain"]==NO&&
            [key isEqualToString:@"O2OWebDomain"]==NO&&
            [key isEqualToString:@"HYKDomain"]==NO&&
            [key isEqualToString:@"VcardDomain"]==NO&&
            [key isEqualToString:@"orderid"]==NO&&
            [key isEqualToString:@"getIpStatue"]==NO&&
            [key isEqualToString:@"lastContactDate"]==NO&&[key isEqualToString:@"isEmployeesLogin"]==NO&&![key containsString:@"showCount-"]&&![key containsString:@"lastShowDates-"]&&![key containsString:@"isHidenJHMRemind"]) {
            [defatluts removeObjectForKey:key];
            [defatluts synchronize];
        }
    }
    [defatluts removeObjectForKey:@"PeripheralDeviceName"];
    [defatluts removeObjectForKey:@"token"];
}
#pragma mark json格式字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

#pragma mark 对象转json格式字符串
+ (NSString*)dictionaryToJson:(id )dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}

#pragma mark 数组转json格式字符串
+ (NSString*)ArrtionaryToJson:(NSMutableArray *)arr
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}
+(BOOL) verifyURL:(NSString *)url{
    NSString *pattern = @"^(((http[s]{0,1}|ftp|Http[s]{0,1})://)?[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:url];
    return isMatch;
}
//日期大小比较
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //stringFromDate这个方法加了八小时
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    //dateFromString这个方法减了八小时，这狗日的
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult result = [dateA compare:dateB];
//    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //NSLog(@"Date1  is in the future");
        return 1;
    }
    else if (result ==NSOrderedAscending){
        //NSLog(@"Date1 is in the past");
        return -1;
    }
    //NSLog(@"Both dates are the same");
    return 0;
    
}

+(BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

+ (BOOL)isPureFloat:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

+(NSInteger)howManyDaysInThisYear:(NSInteger)year withMonth:(NSInteger)month{
    if((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12))
        return 31 ;
    if((month == 4) || (month == 6) || (month == 9) || (month == 11))
        return 30;
    if((year % 4 == 1) || (year % 4 == 2) || (year % 4 == 3)){
        return 28;
    }
    if(year % 400 == 0)
        return 29;
    if(year % 100 == 0)
        return 28;
    return 29;
}

+(UIColor*)mostColor:(UIImage*)image{
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_6_1
    int bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast;
#else
    int bitmapInfo = kCGImageAlphaPremultipliedLast;
#endif
    //第一步 先把图片缩小 加快计算速度. 但越小结果误差可能越大
    CGSize thumbSize=CGSizeMake(image.size.width, image.size.height);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 thumbSize.width,
                                                 thumbSize.height,
                                                 8,//bits per component
                                                 thumbSize.width*4,
                                                 colorSpace,
                                                 bitmapInfo);
    
    CGRect drawRect = CGRectMake(0, 0, thumbSize.width, thumbSize.height);
    CGContextDrawImage(context, drawRect, image.CGImage);
    CGColorSpaceRelease(colorSpace);
    
    //第二步 取每个点的像素值
    unsigned char* data = CGBitmapContextGetData (context);
    if (data == NULL) return nil;
    NSCountedSet *cls=[NSCountedSet setWithCapacity:thumbSize.width*thumbSize.height];
    
    for (int x=0; x<thumbSize.width; x++) {
        for (int y=0; y<thumbSize.height; y++) {
            int offset = 4*(x*y);
            int red = data[offset];
            int green = data[offset+1];
            int blue = data[offset+2];
            int alpha =  data[offset+3];
            if (alpha>0) {//去除透明
                if (red==255&&green==255&&blue==255) {//去除白色
                }else{
                    NSArray *clr=@[@(red),@(green),@(blue),@(alpha)];
                    [cls addObject:clr];
                }
                
            }
        }
    }
    CGContextRelease(context);
    //第三步 找到出现次数最多的那个颜色
    NSEnumerator *enumerator = [cls objectEnumerator];
    NSArray *curColor = nil;
    NSArray *MaxColor=nil;
    NSUInteger MaxCount=0;
    while ( (curColor = [enumerator nextObject]) != nil )
    {
        NSUInteger tmpCount = [cls countForObject:curColor];
        if ( tmpCount < MaxCount ) continue;
        MaxCount=tmpCount;
        MaxColor=curColor;
        
    }
    return [UIColor colorWithRed:([MaxColor[0] intValue]/255.0f) green:([MaxColor[1] intValue]/255.0f) blue:([MaxColor[2] intValue]/255.0f) alpha:0.75];//([MaxColor[3] intValue]/255.0f)
}
+(NSString*)returnDecimalNumStr:(NSString*)numStr{
    NSString *doubleString  = [NSString stringWithFormat:@"%lf", [numStr doubleValue]];
    NSDecimalNumber *decNumber = [NSDecimalNumber decimalNumberWithString:doubleString];
    return [decNumber stringValue];
}
+(BOOL)IsChinese:(NSString *)str {
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){
            return YES;
        }        
    }
    return NO;
}
+(UIImage*)viewBecomeImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
+(NSString *)getMoneyFormat:(NSString*)money{
    if ([money containsString:@"￥"]) {
        money = [money stringByReplacingOccurrencesOfString:@"￥" withString:@""];
    }
    NSRange range = [money rangeOfString:@"."];    
    if (range.location == NSNotFound) {//没找到小数点就是整型或者非法字符
        BOOL isInt = [self isPureInt:money];
        if (money.length==0) {
            return @"";
        }
        if (!isInt) {
            [Mytools warnText:@"数字格式错误，请重输" status:Error];
            return @"";
        }
    }else{
        BOOL isFloat = [self isPureFloat:money];
        if (!isFloat) {
            [Mytools warnText:@"数字格式错误，请重输" status:Error];
            return @"";
        }else{//如果是浮点数，就判断是否是两位小数
            NSInteger floatLength = money.length - range.location - 1;
            if (floatLength>2) {
                return [money substringWithRange:NSMakeRange(0, range.location+3)];
            }
        }
    }
    return money;
}
//大于等于8.0   ps：currentUserNotificationSettings在iOS10 废弃
+ (BOOL)isUserNotificationEnable { // 判断用户是否允许接收通知
    BOOL isEnable = NO;
    UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    isEnable = (UIUserNotificationTypeNone == setting.types) ? NO : YES;
    return isEnable;
}


+(BOOL)hasIllegalCharacter:(NSString *)content {
    NSString *str =@"^[A-Za-z0-9\\u4e00-\u9fa5]+$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", str];
    if (![emailTest evaluateWithObject:content]) {
        return YES;
    }
    return NO;
}

+(UIViewController*)currentViewController{
    //获得当前活动窗口的根视图
    UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (1)
    {
        //根据不同的页面切换方式，逐步取得最上层的viewController
        if ([vc isKindOfClass:[UITabBarController class]]) {
            vc = ((UITabBarController*)vc).selectedViewController;
        }
        if ([vc isKindOfClass:[UINavigationController class]]) {
            vc = ((UINavigationController*)vc).visibleViewController;
        }
        if (vc.presentedViewController) {
            vc = vc.presentedViewController;
        }else{
            break;
        }
    }
    return vc;
}
+(NSString *)formatFloat:(float)f
{
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}
@end
