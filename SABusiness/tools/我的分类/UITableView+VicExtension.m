//
//  UITableView+VicExtension.m
//  ElectricSilverDelegate
//
//  Created by Jinniu on 2018/7/3.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import "UITableView+VicExtension.h"

@implementation UITableView (VicExtension)
+(void)load{
    Method originalMethod = class_getInstanceMethod([UITableView class], @selector(initWithFrame:style:));
    Method swizzledMethod = class_getInstanceMethod([UITableView class], @selector(my_initWithFrame:style:));
    method_exchangeImplementations(originalMethod, swizzledMethod);
}
-(instancetype)my_initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    UITableView * v = [self my_initWithFrame:frame style:style];
    if([UIDevice currentDevice].systemVersion.floatValue >= 11.f){
        v.estimatedRowHeight = 0;
        v.estimatedSectionHeaderHeight = 0;
        v.estimatedSectionFooterHeight = 0;
    }    
    return v;
}
@end
