//
//  UILabel+Extension.m
//  Weibo11
//
//  Created by JYJ on 15/12/5.
//  Copyright © 2015年 itheima. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize {
    return [self labelWithTitle:title color:color fontSize:fontSize alignment:NSTextAlignmentCenter];
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color fontSize:(CGFloat)fontSize alignment:(NSTextAlignment)alignment {
    
    UILabel *lab = [[UILabel alloc] init];
    
    lab.text = title;
    lab.textColor = color;
    lab.font = [UIFont systemFontOfSize:fontSize];
    lab.numberOfLines = 0;
    lab.textAlignment = alignment;
    [lab sizeToFit];
    
    return lab;
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font {
    return [self labelWithTitle:title color:color font:font alignment:NSTextAlignmentCenter];
}

+ (instancetype)labelWithTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font alignment:(NSTextAlignment)alignment {
    
    UILabel *lab = [[UILabel alloc] init];
    lab.text = title;
    lab.textColor = color;
    lab.font = font;
    lab.numberOfLines = 0;
    lab.textAlignment = alignment;
    [lab sizeToFit];
    return lab;
}


@end
