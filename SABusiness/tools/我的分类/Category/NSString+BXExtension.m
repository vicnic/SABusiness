//
//  NSString+BXExtension.m
//  BXInsurenceBroker
//
//  Created by JYJ on 16/2/23.
//  Copyright © 2016年 baobeikeji. All rights reserved.
//

#import "NSString+BXExtension.h"

@implementation NSString (BXExtension)


- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize {
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (BOOL)isEmptyString {
    return self.length == 0 || [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0;
}
//仅显示前4后4
-(NSString*)getShelterIDCard{
    if (self.length<8) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(4, self.length-8) withString:@" **** **** "];
    return finalStr;
}
//仅显示前4后3
-(NSString*)getShelterBankCard{
    if (self.length<7) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(4, self.length-7) withString:@" **** **** "];
    return finalStr;
}
//仅显示前3后4
-(NSString*)getShelterPhoneNum{
    if (self.length<7) {
        return self;
    }
    NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(3, self.length-7) withString:@" **** "];
    return finalStr;
}
//姓名 多个字只显示前1后1,2个字仅显示后1
-(NSString*)getShelterName{
    if (self.length<=1) {
        return self;
    }
    if (self.length==2) {
        NSString * finalStr = [self stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
        return finalStr;
    }else{
        NSString * rangeStr = [self substringWithRange:NSMakeRange(1, self.length-2)];
        NSString * starStr = @"";
        for (int i =0; i<self.length-2; i++) {
            starStr = [NSString stringWithFormat:@"%@*",starStr];
        }
        NSString * finalStr = [self stringByReplacingOccurrencesOfString:rangeStr withString:starStr];
        return finalStr;
    }
}
@end
