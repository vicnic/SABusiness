//
//  NSString+BXExtension.h
//  BXInsurenceBroker
//
//  Created by JYJ on 16/2/23.
//  Copyright © 2016年 baobeikeji. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BXExtension)

/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

- (BOOL)isEmptyString;
/**身份证：仅显示前4后4*/
-(NSString*)getShelterIDCard;
/**银行卡：仅显示前4后3*/
-(NSString*)getShelterBankCard;
/**手机号：仅显示前3后4*/
-(NSString*)getShelterPhoneNum;
/**姓名 多个字只显示前1后1,2个字仅显示后1*/
-(NSString*)getShelterName;
@end
