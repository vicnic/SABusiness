//
//  NSObject+HZCoding.h
//  ElectricSilverDelegate
//
//  Created by Jinniu on 2018/10/18.
//  Copyright © 2018年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
@interface NSObject (HZCoding)
-(void)HZ_encode:(NSCoder *)aCoder;
-(void)HZ_decode:(NSCoder *)aDecoder;
#define HZCodingImplementation \
-(void)encodeWithCoder:(NSCoder *)aCoder\
{\
[self HZ_encode:aCoder];\
}\
-(instancetype)initWithCoder:(NSCoder *)aDecoder\
{\
if (self = [super init]) {\
[self HZ_decode:aDecoder];\
}return self; \
}

@end
