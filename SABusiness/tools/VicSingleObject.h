//
//  VicSingleObject.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/4.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBUserInfoModel.h"
#import "ShopInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VicSingleObject : NSObject
@property(nonatomic,retain)SBUserInfoModel * userInfoModel;
@property(nonatomic,retain)ShopInfoModel * shopInfoModel;
+(id)getInstance;
+(void)attempDealloc;
@end

NS_ASSUME_NONNULL_END
