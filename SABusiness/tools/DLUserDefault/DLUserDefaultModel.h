//
//  DLUserDefaultModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "NSUserDefaultsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DLUserDefaultModel : NSUserDefaultsModel
@property(nonatomic, copy)NSString *token, * username, *password,* isRememberPsw,*rongyunToken;

@end

NS_ASSUME_NONNULL_END
