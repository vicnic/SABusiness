//
//  DLUserDefaultModel.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "DLUserDefaultModel.h"

@implementation DLUserDefaultModel
@dynamic token;
@dynamic username;
@dynamic password;
@dynamic isRememberPsw;
@dynamic rongyunToken;
- (NSDictionary *)setupDefaultValues{
    return @{
             @"token":@"",
             @"username":@"",
             @"password":@"",
             @"isRememberPsw":@"",
             @"rongyunToken":@""
             };
}
@end
