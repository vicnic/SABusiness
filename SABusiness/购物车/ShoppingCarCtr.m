//
//  ShoppingCarCtr.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "ShoppingCarCtr.h"
#import "ShoppingCarCell.h"
#import "CalculatePayCtr.h"
#import <QMUIKit/QMUIKit.h>
#import "StoreMainViewVC.h"
#import "AddressListVC.h"
@interface ShoppingCarCtr ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView*myTab;
@property(nonatomic,assign)BOOL isSelAll;
@property(nonatomic,retain)NSMutableArray * dataArr;
@property(nonatomic,retain)NSMutableDictionary * labDic;
@end

@implementation ShoppingCarCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"购物车", nil);
    self.dataArr = [NSMutableArray new];
    self.labDic = [NSMutableDictionary new];
    [self.view addSubview:self.myTab];
    [self getData];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetShopCarList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myTab.mj_header endRefreshing];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [ShoppingCarModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myTab reloadData];
            [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)calculateBtnClick:(UIButton*)sender{
    VicSingleObject * single = [VicSingleObject getInstance];
    if (single.userInfoModel.address.detail_address.length==0) {
        HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"" andMessage:NSLocalizedString(@"您的收货地址为空请前往新增", nil)];
        alertView.defaultButtonTitleColor = [UIColor redColor];
        alertView.cancelButtonTitleColor = [UIColor lightGrayColor];
        [alertView addButtonWithTitle:NSLocalizedString(@"取消", nil) type:HDAlertViewButtonTypeCancel handler:^(HDAlertView *alertView) {
            
        }];
        [alertView addButtonWithTitle:NSLocalizedString(@"确定", nil) type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
            AddressListVC * vc = [AddressListVC new];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        alertView.buttonFont = [UIFont systemFontOfSize:16];
        [alertView show];
    }else{
        ShoppingCarModel * mo = self.dataArr[sender.tag - 1];
        CalculatePayCtr * vc = [CalculatePayCtr new];
        vc.model = mo;
        vc.postBlock = ^{
            [self.myTab.mj_header beginRefreshing];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
-(void)allSelBtnClick{
//    全选
    self.isSelAll = !self.isSelAll;
    [self.myTab reloadData];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    ShoppingCarModel * mo = self.dataArr[section];
    return mo.goods_list.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCarCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[ShoppingCarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    ShoppingCarModel * mo = self.dataArr[indexPath.section];
    cell.model = mo.goods_list[indexPath.row];
    cell.isSelAll = self.isSelAll;
    cell.infoModel = mo.shop_info;
    cell.circleBlock = ^(BOOL isPlus, CGFloat price) {
        NSString * key = [NSString stringWithFormat:@"key%ld",indexPath.section];
        UILabel * lab = (UILabel*)self.labDic[key];
        if(isPlus){
            CGFloat allPrice = [lab.text doubleValue] + price;
            lab.text = [NSString stringWithFormat:@"%.2lf",allPrice];
        }else{
            CGFloat allPrice = [lab.text doubleValue] - price;
            lab.text = [NSString stringWithFormat:@"%.2lf",allPrice];
        }
    };
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCarModel * mo = self.dataArr[indexPath.section];
    ShoppingCardList * model = mo.goods_list[indexPath.row];
    CGFloat height = [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[ShoppingCarCell class] contentViewWidth:IPHONE_WIDTH];
    VLog(@"%lf",height);
    return  height;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 55;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ShoppingCarModel * mo = self.dataArr[section];
    UIView* headView = [UIView new];
    UIView * bgView  = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = Frame(10, 0, IPHONE_WIDTH-20, 45);
    [headView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    UILabel * nameLab = [UILabel labelWithTitle:mo.shop_info.shop_name color:[UIColor blackColor] fontSize:16];
    nameLab.tag = section+1000;
    [nameLab addTarget:self action:@selector(gotoShopMainView:)];
    [bgView addSubview:nameLab];
    nameLab.sd_layout.leftSpaceToView(bgView, 10).centerYEqualToView(bgView).heightIs(20);
    [nameLab setSingleLineAutoResizeWithMaxWidth:300];
    return  headView;
}
-(void)gotoShopMainView:(UITapGestureRecognizer*)tap{
    ShoppingCarModel * mo = self.dataArr[tap.view.tag -1000];
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = mo.shop_info.tid;
    [self.navigationController pushViewController:vc animated:YES];
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * footerView = [UIView new];
    UIView * bgView = [[UIView alloc]initWithFrame:Frame(10, 0, IPHONE_WIDTH-20, 45)];
    bgView.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    UIButton * calBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [calBtn setTitle:NSLocalizedString(@"结算", nil) forState:UIControlStateNormal];
    calBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    calBtn.backgroundColor = [UIColor orangeColor];
    calBtn.tag = section+1;
    calBtn.cornerRadius = 17.5;
    [calBtn addTarget:self action:@selector(calculateBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:calBtn];
    calBtn.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(bgView).widthIs(80).heightIs(35);
    CGFloat total = 0;
    ShoppingCarModel * model = self.dataArr[section];
    for (ShoppingCardList * mo  in model.goods_list) {
        total += [mo.goods_price doubleValue]* mo.goods_count;
    }
    NSString * totalStr = [NSString stringWithFormat:@"%.2lf",total];
    UILabel * valLab = [UILabel labelWithTitle:totalStr color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:valLab];
    valLab.sd_layout.centerYEqualToView(bgView).rightSpaceToView(calBtn, 8).heightIs(20);
    [valLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UILabel * calNumLab = [UILabel labelWithTitle:NSLocalizedString(@"共计", nil) color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:calNumLab];
    calNumLab.sd_layout.centerYEqualToView(bgView).rightSpaceToView(valLab, 0).heightIs(20);
    [calNumLab setSingleLineAutoResizeWithMaxWidth:300];
    
    NSString * key = [NSString stringWithFormat:@"key%ld",section];
    [self.labDic setObject:valLab forKey:key];
    UIView * barView = [[UIView alloc]initWithFrame:Frame(0, 45, IPHONE_WIDTH, 10)];
    barView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:barView];
    return footerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return NSLocalizedString(@"删除", nil);
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCarModel * mo = self.dataArr[indexPath.section];
    ShoppingCardList *model = mo.goods_list[indexPath.row];
    NSString * url = [WorkUrl returnURL:Interface_For_DeleteGoods];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(model.goods_id) forKey:@"goods_id"];
    [dic setObject:@(mo.shop_info.tid) forKey:@"shop_id"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [self.dataArr removeAllObjects];
            [self getData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-kTabbarHeight) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self.dataArr removeAllObjects];
            [self getData];
        }];
    }
    return _myTab;
}


@end
