//
//Created by ESJsonFormatForMac on 20/03/01.
//

#import <Foundation/Foundation.h>

@class ShoppingCarShopInfo,ShoppingCardList;
@interface ShoppingCarModel : NSObject

@property (nonatomic, strong) NSArray *goods_list;

@property (nonatomic, strong) ShoppingCarShopInfo *shop_info;

@end
@interface ShoppingCarShopInfo : NSObject

@property (nonatomic, copy) NSString *shop_thumb;

@property (nonatomic, assign) NSInteger tid;

@property (nonatomic, assign) NSInteger focus_count;

@property (nonatomic, assign) NSInteger is_forbidden;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, copy) NSString *shop_description;

@property (nonatomic, assign) NSInteger updated_at;

@end

@interface ShoppingCardList : NSObject

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *goods_thumb;

@property (nonatomic, assign) NSInteger shop_id;

@property (nonatomic, copy) NSString *goods_currency;

@property (nonatomic, copy) NSString *goods_introduction;

@property (nonatomic, copy) NSString *goods_tags;

@property (nonatomic, assign) NSInteger goods_count;

@property (nonatomic, assign) NSInteger goods_integration;

@property (nonatomic, assign) NSInteger goods_id;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, assign) NSInteger goods_sales_count;

@property (nonatomic, copy) NSString *goods_cate;

@property (nonatomic, assign) NSInteger is_recommend;

@end

