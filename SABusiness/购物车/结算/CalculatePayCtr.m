//
//  CalculatePayCtr.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "CalculatePayCtr.h"
#import "CalculatePayHeadView.h"
#import "CalculateOrderCell.h"
#import "SendPayedPicView.h"
#import "StoreMainViewVC.h"
@interface CalculatePayCtr ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,retain)CalculatePayHeadView * infoView;

@end

@implementation CalculatePayCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"确认订单", nil);
    
    [self.view addSubview:self.myTab];
}

#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section==0?0:self.model.goods_list.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CalculateOrderCell * cell = [[CalculateOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    if (indexPath.section>0) {
        cell.model = self.model.goods_list[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section==0?120:45;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section==0?0.01:55;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCardList * mo = self.model.goods_list[indexPath.section-1];
    CGFloat height = [tableView cellHeightForIndexPath:indexPath model:mo keyPath:@"model" cellClass:[CalculateOrderCell class] contentViewWidth:IPHONE_WIDTH];
    return height;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"CalculatePayHeadView" owner:nil options:nil];
        CalculatePayHeadView * v = obj.firstObject;
        v.frame = Frame(0, 0, IPHONE_WIDTH, 120);
        return v;
    }else{
        UIView* headView = [UIView new];
        UIView * bgView  = [UIView new];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.frame = Frame(10, 0, IPHONE_WIDTH-20, 45);
        [headView addSubview:bgView];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = bgView.bounds;
        maskLayer.path = maskPath.CGPath;
        bgView.layer.mask = maskLayer;
        
        UILabel * nameLab = [UILabel labelWithTitle:self.model.shop_info.shop_name color:[UIColor blackColor] fontSize:16];
        [nameLab addTarget:self action:@selector(gotoShopMainView)];
        [bgView addSubview:nameLab];
        nameLab.sd_layout.leftSpaceToView(bgView, 10).centerYEqualToView(bgView).heightIs(20);
        [nameLab setSingleLineAutoResizeWithMaxWidth:300];
        
        return  headView;
    }
}
-(void)gotoShopMainView{
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = self.model.shop_info.tid;
    [self.navigationController pushViewController:vc animated:YES];
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0) {
        return nil;
    }
    UIView * footerView = [UIView new];
    UIView * bgView = [[UIView alloc]initWithFrame:Frame(10, 0, IPHONE_WIDTH-20, 45)];
    bgView.backgroundColor = [UIColor whiteColor];
    [footerView addSubview:bgView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, IPHONE_WIDTH - 20, 45) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    UIButton * calBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [calBtn setTitle:NSLocalizedString(@"结算", nil) forState:UIControlStateNormal];
    calBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    calBtn.backgroundColor = [UIColor orangeColor];
    calBtn.tag = section+1;
    calBtn.cornerRadius = 17.5;
    [calBtn addTarget:self action:@selector(postOrder) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:calBtn];
    calBtn.sd_layout.rightSpaceToView(bgView, 10).centerYEqualToView(bgView).widthIs(80).heightIs(35);
    
    CGFloat total = 0;
    for (ShoppingCardList * mo  in self.model.goods_list) {
        total += [mo.goods_price doubleValue]* mo.goods_count;
    }
    NSString * totalStr = [NSString stringWithFormat:@"%.2lf",total];
    UILabel * valLab = [UILabel labelWithTitle:totalStr color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:valLab];
    valLab.sd_layout.centerYEqualToView(bgView).rightSpaceToView(calBtn, 8).heightIs(20);
    [valLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UILabel * calNumLab = [UILabel labelWithTitle:NSLocalizedString(@"共计", nil) color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:calNumLab];
    calNumLab.sd_layout.centerYEqualToView(bgView).rightSpaceToView(valLab, 0).heightIs(20);
    [calNumLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIView * barView = [[UIView alloc]initWithFrame:Frame(0, 45, IPHONE_WIDTH, 10)];
    barView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [bgView addSubview:barView];
    return footerView;
}
-(void)postOrder{
    NSString * url = [WorkUrl returnURL:Interface_For_PostOrderList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    VicSingleObject * single = [VicSingleObject getInstance];
    [dic setObject:@(self.model.shop_info.tid) forKey:@"shop_id"];
    [dic setObject:@(single.userInfoModel.address.aid) forKey:@"address_id"];
    NSMutableArray * arr = [NSMutableArray new];
    for (ShoppingCardList * mo in self.model.goods_list) {
        NSMutableDictionary * cpp = [NSMutableDictionary new];
        [cpp setObject:@(mo.goods_id) forKey:@"goods_id"];
        [cpp setObject:@(mo.goods_count) forKey:@"goods_count"];
        [arr addObject:cpp];
    }
    NSString * arrstring = [Mytools dictionaryToJson:arr];
    [dic setObject:arrstring forKey:@"order_goods_list"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            HDAlertView *alertView = [HDAlertView alertViewWithTitle:@"" andMessage:NSLocalizedString(@"您已成功提交订单，是否拷贝平台银行卡号进行转账", nil)];
            alertView.defaultButtonTitleColor = [UIColor redColor];
            alertView.cancelButtonTitleColor = [UIColor lightGrayColor];
            [alertView addButtonWithTitle:NSLocalizedString(@"取消", nil) type:HDAlertViewButtonTypeCancel handler:^(HDAlertView *alertView) {
                
            }];
            [alertView addButtonWithTitle:NSLocalizedString(@"确定", nil) type:HDAlertViewButtonTypeDefault handler:^(HDAlertView *alertView) {
                if (self.postBlock) {
                    self.postBlock();
                }
                UIPasteboard * bo = [UIPasteboard generalPasteboard];
                bo.string = single.userInfoModel.platform_bank_num;
                [Mytools warnText:NSLocalizedString(@"已复制", nil) status:Success];
            }];
            alertView.buttonFont = [UIFont systemFontOfSize:16];
            [alertView show];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        _myTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_myTab registerNib:[UINib nibWithNibName:@"CalculateOrderCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    }
    return _myTab;
}

@end
