//
//  CalculateOrderCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "CalculateOrderCell.h"

@interface CalculateOrderCell()
@property (retain, nonatomic)  UIImageView *picView;
@property (retain, nonatomic)  UILabel *titlelAB;
@property (retain, nonatomic)  UILabel *moneyLab;
@property (retain, nonatomic)  UILabel *descLab;
@property (retain, nonatomic)  UILabel *pointLab;
@property (retain, nonatomic)  UILabel *countLab;
@end
@implementation CalculateOrderCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    UIView * bgView = [UIView new];
    bgView.backgroundColor = [ UIColor whiteColor];
    [self.contentView addSubview:bgView];
    bgView.sd_layout.leftSpaceToView(self.contentView, 10).rightSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 0);
    
    self.picView = [UIImageView new];
    [bgView addSubview:self.picView];
    _picView.sd_layout.leftSpaceToView(bgView, 10).topSpaceToView(bgView, 10).widthIs(60).heightEqualToWidth();
    
    self.moneyLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14];
    [bgView addSubview:self.moneyLab];
    _moneyLab.sd_layout.rightSpaceToView(bgView, 10).topEqualToView(_picView).heightIs(18);
    [_moneyLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.titlelAB = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [bgView addSubview:self.titlelAB];
    _titlelAB.sd_layout.leftSpaceToView(_picView, 10).topEqualToView(_picView).rightSpaceToView(_moneyLab, 10).autoHeightRatio(0);
    
    self.countLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:12];
    [bgView addSubview:self.countLab];
    _countLab.sd_layout.rightEqualToView(_moneyLab).topSpaceToView(_moneyLab, 10).heightIs(14);
    [_countLab setSingleLineAutoResizeWithMaxWidth:300];
    
    self.descLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:12 alignment:NSTextAlignmentLeft];
    [bgView addSubview:self.descLab];
    _descLab.sd_layout.leftEqualToView(_titlelAB).topSpaceToView(_titlelAB, 10).rightSpaceToView(_countLab, 10).autoHeightRatio(0);
    
    self.pointLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:12];
    [bgView addSubview:self.pointLab];
    _pointLab.sd_layout.leftEqualToView(_descLab).topSpaceToView(_descLab, 10).heightIs(14);
    [_pointLab setSingleLineAutoResizeWithMaxWidth:300];
    
    [bgView setupAutoHeightWithBottomView:_pointLab bottomMargin:10];
    [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
    
}

- (void)setModel:(ShoppingCardList *)model{    
    [self.picView sd_setImageWithURL:URL(model.goods_thumb) placeholderImage:PlaceHolderImg];
    self.countLab.text = [NSString stringWithFormat:@"*%ld",model.goods_count];
    self.moneyLab.text = model.goods_price;
    self.titlelAB.text = model.goods_name;
    self.descLab.text = model.goods_introduction;
    NSString * pont = NSLocalizedString(@"积分", nil);
    self.pointLab.text = [NSString stringWithFormat:@"%@:%@",pont,@"6666"];
}
- (void)selBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}


@end
