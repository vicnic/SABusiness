//
//  CalculateOrderCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCarModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CalculateOrderCell : UITableViewCell
@property(nonatomic,retain)ShoppingCardList * model;
@end

NS_ASSUME_NONNULL_END
