//
//  CalculateOrderModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalculateOrderModel : NSObject
@property(nonatomic,retain)NSString * storeName;
@property(nonatomic,retain)NSString * statueStr;
@property(nonatomic,retain)NSString * goodsName;
@property(nonatomic,retain)NSString * goodsDesc;
@property(nonatomic,assign)CGFloat money;
@property(nonatomic,retain)NSString * pointValue;
@property(nonatomic,assign)NSInteger count;
@end

NS_ASSUME_NONNULL_END
