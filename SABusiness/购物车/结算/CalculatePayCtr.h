//
//  CalculatePayCtr.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"
#import "ShoppingCarModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^PostSuccessBlock)(void);
@interface CalculatePayCtr : SuperViewController
@property(nonatomic,retain)ShoppingCarModel * model;
@property(nonatomic,copy)PostSuccessBlock postBlock;
@end

NS_ASSUME_NONNULL_END
