//
//  SendPayedPicView.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^BotBtnBlock)(NSInteger type);
@interface SendPayedPicView : UIView
@property(nonatomic,copy)BotBtnBlock btnBlock;
@end

NS_ASSUME_NONNULL_END
