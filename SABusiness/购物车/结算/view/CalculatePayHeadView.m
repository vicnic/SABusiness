//
//  CalculatePayHeadView.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "CalculatePayHeadView.h"
@interface CalculatePayHeadView()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;

@end
@implementation CalculatePayHeadView

- (void)awakeFromNib{
    [super awakeFromNib];
    VicSingleObject * single = [VicSingleObject getInstance];
    NSString * head = [NSString stringWithFormat:@"%@%@",MainDomain,single.userInfoModel.avatar];
    [self.headView sd_setImageWithURL:URL(head) placeholderImage:PlaceHolderImg];
    self.nameLab.text = single.userInfoModel.nickname.length==0?@"":single.userInfoModel.nickname;
    self.phoneLab.text = single.userInfoModel.mobile;
    self.addressLab.text = single.userInfoModel.address.detail_address;
}

@end
