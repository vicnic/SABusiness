//
//  SendPayedPicView.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/26.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SendPayedPicView.h"
#import <TZImagePickerController/TZImagePickerController.h>
@interface SendPayedPicView()<TZImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *picView;

@end
@implementation SendPayedPicView
- (void)awakeFromNib{
    [super awakeFromNib];
    [self.picView addTarget:self action:@selector(pickViewClick)];
}
-(void)pickViewClick{
    self.hidden = YES;
    self.superview.hidden = YES;
    TZImagePickerController * pick = [[TZImagePickerController alloc]initWithMaxImagesCount:1 delegate:self];
    [pick setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        self.hidden = NO;
        self.superview.hidden = NO;
        UIImage * pic = photos[0];
        self.picView.image = pic;
    }];
    [[Mytools currentViewController] presentViewController:pick animated:YES completion:nil];
}
- (IBAction)cancelBtnClick:(id)sender {
    if (self.btnBlock) {
        self.btnBlock(0);
    }
}
- (IBAction)sureBtnClick:(id)sender {
    if (self.btnBlock) {
        self.btnBlock(1);
    }
}

@end
