//
//  ShoppingCarCell.h
//  SABusiness
//
//  Created by Jinniu on 2019/12/18.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCarModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^SelCircleBtnBlock)(BOOL isPlus,CGFloat price);
@interface ShoppingCarCell : UITableViewCell
@property(nonatomic,retain)ShoppingCardList* model;
@property(nonatomic,retain)ShoppingCarShopInfo * infoModel;
@property(nonatomic,assign)BOOL isSelAll;
@property(nonatomic,retain)UIView *bgView;
@property(nonatomic,copy)SelCircleBtnBlock circleBlock;
@end

NS_ASSUME_NONNULL_END
