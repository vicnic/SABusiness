//
//  ShoppingCarCell.m
//  SABusiness
//
//  Created by Jinniu on 2019/12/18.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import "ShoppingCarCell.h"
@interface ShoppingCarCell()
@property(nonatomic,retain)UIImageView * iconView;
@property(nonatomic,retain)UILabel * titleLab,*descLab , * priceLab;
@property(nonatomic,retain)UITextField * countTF;
@property(nonatomic,retain)UIButton * circleBtn;
@end
@implementation ShoppingCarCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self createCusUI];
    }
    return self;
}
-(void)createCusUI{
    self.bgView = [UIView new];
    _bgView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.bgView];
    _bgView.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.contentView, 0).rightSpaceToView(self.contentView, 10);
    
//    self.circleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_circleBtn setImage:Image(@"圆环") forState:UIControlStateNormal];
//    [_circleBtn setImage:Image(@"选中") forState:UIControlStateSelected];
//    _circleBtn.selected = NO;
//    [_circleBtn addTarget:self action:@selector(choseCircleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_bgView addSubview:self.circleBtn];
//    _circleBtn.sd_layout.leftSpaceToView(_bgView, 10).centerYEqualToView(_bgView).widthIs(30).heightEqualToWidth();
    
    self.iconView = [UIImageView new];
    [_bgView addSubview:self.iconView];
    _iconView.sd_layout.leftSpaceToView(_bgView, 10).topSpaceToView(_bgView, 10).widthIs(60).heightIs(60);
    
    self.titleLab = [UILabel labelWithTitle:@"多肉植物蛋白质" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [_bgView addSubview:self.titleLab];
    _titleLab.sd_layout.leftSpaceToView(self.iconView, 10).topEqualToView(self.iconView).rightSpaceToView(_bgView, 10).heightIs(14);
    
    self.descLab = [UILabel labelWithTitle:@"家用美观，防辐射" color:[UIColor darkGrayColor] fontSize:12];
    [_bgView addSubview:self.descLab];
    _descLab.sd_layout.leftEqualToView(self.titleLab).topSpaceToView(self.titleLab, 8).heightIs(12);
    [_descLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIView * btnView = [UIView new];
    btnView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    btnView.layer.borderWidth=1;
    btnView.cornerRadius = 3;
    [_bgView addSubview:btnView];
    btnView.sd_layout.topSpaceToView(self.descLab, 10).rightSpaceToView(_bgView, 10).heightIs(25).widthIs(80);
    UIButton * minusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [minusBtn setTitle:@"-" forState:UIControlStateNormal];
    minusBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [minusBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [minusBtn addTarget:self action:@selector(minusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnView addSubview:minusBtn];
    minusBtn.sd_layout.leftEqualToView(btnView).topEqualToView(btnView).bottomEqualToView(btnView).widthEqualToHeight();
    
    UIButton * plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [plusBtn setTitle:@"+" forState:UIControlStateNormal];
    plusBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [plusBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [plusBtn addTarget:self action:@selector(plusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [btnView addSubview:plusBtn];
    plusBtn.sd_layout.rightEqualToView(btnView).topEqualToView(btnView).bottomEqualToView(btnView).widthEqualToHeight();
    
    self.countTF = [UITextField new];
    self.countTF.text = @"1";
    self.countTF.userInteractionEnabled = NO;
    self.countTF.textAlignment = NSTextAlignmentCenter;
    self.countTF.font = [UIFont systemFontOfSize:12];
    self.countTF.keyboardType = UIKeyboardTypeNumberPad;
    [btnView addSubview:self.countTF];
    self.countTF.sd_layout.leftSpaceToView(minusBtn, 0).rightSpaceToView(plusBtn, 0).topEqualToView(btnView).bottomEqualToView(btnView);
    UIView * line1 = [UIView new];
    line1.backgroundColor = [UIColor lightGrayColor];
    [btnView addSubview:line1];
    line1.sd_layout.leftEqualToView(self.countTF).widthIs(1).topEqualToView(btnView).bottomEqualToView(btnView);
    UIView * line2 = [UIView new];
    line2.backgroundColor = [UIColor lightGrayColor];
    [btnView addSubview:line2];
    line2.sd_layout.rightEqualToView(self.countTF).widthIs(1).topEqualToView(btnView).bottomEqualToView(btnView);
    
    self.priceLab = [UILabel labelWithTitle:@"" color:ThemeColor fontSize:14];
    [_bgView addSubview:self.priceLab];
    _priceLab.sd_layout.leftEqualToView(self.descLab).centerYEqualToView(btnView).heightIs(20);
    [_priceLab setSingleLineAutoResizeWithMaxWidth:300];
    
    [_bgView setupAutoHeightWithBottomView:btnView bottomMargin:15];
    [self setupAutoHeightWithBottomView:_bgView bottomMargin:0];
}
- (void)setModel:(ShoppingCardList *)model{
    _model = model;
    self.titleLab.text = model.goods_name;
    self.descLab.text = model.goods_introduction;
    self.priceLab.text = model.goods_price;
    self.countTF.text = [NSString stringWithFormat:@"%ld",model.goods_count];
    [self.iconView sd_setImageWithURL:URL(model.goods_thumb) placeholderImage:PlaceHolderImg];
}
-(void)minusBtnClick{
    if ([self.countTF.text integerValue]==1) {
        return;
    }
    NSInteger count = [self.countTF.text integerValue] - 1;
    self.countTF.text = [NSString stringWithFormat:@"%ld",count];
    [self goodsNumClick];
}
- (void)setIsSelAll:(BOOL)isSelAll{
    _isSelAll = isSelAll;
    _circleBtn.selected  = isSelAll;
}
-(void)plusBtnClick{
    NSInteger count = [self.countTF.text integerValue] + 1;
    if(count>_model.goods_sales_count){
        [Mytools warnText:NSLocalizedString(@"已选商品数量超过库存数", nil) status:Common];
        return;
    }
    self.countTF.text = [NSString stringWithFormat:@"%ld",count];
    [self goodsNumClick];
}
-(void)choseCircleBtnClick:(UIButton*)sender{
    sender.selected = !sender.selected;
    if(self.circleBlock){
        self.circleBlock(sender.isSelected, [_model.goods_price doubleValue]* _model.goods_count);
    }
}
- (void)setInfoModel:(ShoppingCarShopInfo *)infoModel{
    _infoModel = infoModel;
}
-(void)goodsNumClick{
    NSString * url = [WorkUrl returnURL:Interface_For_SetShoppingCartNum];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(_model.goods_id) forKey:@"goods_id"];
    [dic setObject:@(self.infoModel.tid) forKey:@"shop_id"];
    [dic setObject:self.countTF.text forKey:@"goods_count"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if (![responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            self.countTF.text = [NSString stringWithFormat:@"%ld",[self.countTF.text integerValue]+1];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
    
}
@end
