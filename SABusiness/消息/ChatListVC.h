//
//  ChatListVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/4.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatListVC : RCConversationListViewController

@end

NS_ASSUME_NONNULL_END
