//
//  ChatListVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/1/4.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "ChatListVC.h"

@interface ChatListVC ()

@end

@implementation ChatListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"消息", nil);
    self.conversationListTableView.frame = Frame(0, kTopHeight,IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin);
    //设置需要显示哪些类型的会话,由于楼主只需要单聊功能,所以只设置ConversationType_PRIVATE
    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE)]];
    
}

- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType
         conversationModel:(RCConversationModel *)model
               atIndexPath:(NSIndexPath *)indexPath{
    RCConversationViewController *chat = [[RCConversationViewController alloc]init];
    chat.conversationType =ConversationType_PRIVATE;
    chat.targetId =model.targetId;
    chat.title =@"想显示的会话标题";
    [self.navigationController pushViewController:chat animated:YES];
}

@end
