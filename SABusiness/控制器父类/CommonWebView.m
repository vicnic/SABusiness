//
//  CommonWebView.m
//  SABusiness
//
//  Created by Jinniu on 2020/3/3.
//  Copyright © 2020 Jinniu. All rights reserved.
//

#import "CommonWebView.h"
#import "JWShareView.h"
#import <WebKit/WebKit.h>
@interface CommonWebView ()<WKNavigationDelegate, WKUIDelegate,WKScriptMessageHandler>{
    NSTimer * _timer;
    NSInteger _timeCount,timerCount/*计算调用了几次timer，从而获取秒数*/;
    NSTimer *timer;
}
@property (nonatomic, strong) WKWebView *web;
@property (nonatomic, strong) UIProgressView *progressView;
@end

@implementation CommonWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, self.isHideNavBar==YES?0: kTopHeight-1, IPHONE_WIDTH, 1)];
    self.progressView.backgroundColor = [UIColor blueColor];
    //设置进度条的高度，下面这句代码表示进度条的宽度变为原来的1倍，高度变为原来的1.5倍.
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view addSubview:self.progressView];
    [self createUI];
}
-(void)createUI{
    WKWebViewConfiguration *config = [WKWebViewConfiguration new];
    config.userContentController = [WKUserContentController new];
    //注册js方法
    [config.userContentController addScriptMessageHandler:self name: @"getDevice"];

    self.web = [[WKWebView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin- 50) configuration:config];
    _web.navigationDelegate = self;
    [self.view addSubview:self.web];
    
    [_web addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [_web addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    NSString * finalStr = self.url;
    if ([Mytools IsChinese:self.url]==YES) {
        finalStr = [self.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    NSURLRequest * req = [NSURLRequest requestWithURL:[NSURL URLWithString:finalStr]];
    [_web loadRequest:req];
    if (@available(iOS 11.0, *)) {
        _web.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
-(void)shareMyStore{
    NSArray *contentArray = @[@{@"name":@"微信好友",@"icon":@"sns_icon_7"},@{@"name":@"朋友圈",@"icon":@"sns_icon_8"},@{@"name":@"商品二维码",@"icon":@"sns_icon_code"},@{@"name":@"复制链接",@"icon":@"sns_icon_copy"}];
    JWShareView *shareView = [[JWShareView alloc] init];
    shareView.tag = 12580;
    [shareView addShareItems:[UIApplication sharedApplication].keyWindow shareItems:contentArray selectShareItem:^(NSInteger tag, NSString *title) {
        
    }];
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    VLog(@"开始加载%@",[webView.URL absoluteString]);
    
    //开始加载网页时展示出progressView
    self.progressView.hidden = NO;
    //开始加载网页的时候将progressView的Height恢复为1.5倍
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    //防止progressView被网页挡住
    [self.view bringSubviewToFront:self.progressView];
}

//加载完成
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    VLog(@"加载完成");
    //加载完成后隐藏progressView
    self.progressView.hidden = YES;
}

//加载失败
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    VLog(@"加载失败");
    //加载失败同样需要隐藏progressView
    self.progressView.hidden = YES;
}
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    VLog(@"方法名:%@，传递的数据：%@",message.name,message.body);// 方法名
    if ([message.name isEqualToString:@"closePage"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark 进度条处理箱
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.progress = self.web.estimatedProgress;
        if (self.progressView.progress == 1) {
            /*
             *添加一个简单的动画，将progressView的Height变为1.4倍，在开始加载网页的合伙人中会恢复为1.5倍
             *动画时长0.25s，延时0.3s后开始动画
             *动画结束后将progressView隐藏
             */
            __weak typeof (self)weakSelf = self;
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
            } completion:^(BOOL finished) {
                weakSelf.progressView.hidden = YES;
            }];
        }
    }else if ([keyPath isEqualToString:@"title"]) {
        if (object == _web){
            self.myTitle = _web.title;
        }else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
- (void)dealloc {
    [self.web removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.web removeObserver:self forKeyPath:@"title" context:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
