//
//  SuperViewController.h
//
//
//  Created by 111 on 2017/4/26.
//   Copyright © 2019年 jinniu All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SuperViewController : UIViewController
@property(nonatomic,copy)NSString * myTitle;
@property(nonatomic,copy)NSString * leftBtnImage;
@property(nonatomic,copy)NSString * rightBtnName;
@property(nonatomic,retain)UIColor * rightBtnNameColor;
@property(nonatomic,retain)UIImage * rightBtnImage;
@property(nonatomic,assign)CGFloat rightBtnFontSize;
@property(nonatomic,assign)CGFloat navBarHeight;
@property(nonatomic,assign)BOOL isHideLeftBtn,isSettingMyTitleColor;
@property(nonatomic,assign)CGFloat navTitleFontSize;
@property(nonatomic,retain)UIColor * navBgColor;
@property(nonatomic,assign)BOOL isHideBottomLine;
@property(nonatomic,assign)BOOL isHideNavBar;
@property(nonatomic,retain)UIColor * myTitleColor;
@property(nonatomic,retain)UIView * navView ;
@property(nonatomic,retain)UIButton * rightBtn;
-(void)createNavBar;
-(void)backClick;
-(void)rightBtnClick;
@end
