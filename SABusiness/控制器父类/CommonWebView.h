//
//  CommonWebView.h
//  SABusiness
//
//  Created by Jinniu on 2020/3/3.
//  Copyright © 2020 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommonWebView : SuperViewController
@property(nonatomic,copy)NSString * url;
@end

NS_ASSUME_NONNULL_END
