//
//  SGQRCodeScanningVC.h
//  SGQRCodeExample
//
//  Created by kingsic on 17/3/20.
//  Copyright © 2017年 kingsic. All rights reserved.
//

#import "SuperViewController.h"
typedef void(^ReturnCodeStrBlock)(NSString *codeStr);
@interface SGQRCodeScanningVC : SuperViewController
@property (nonatomic,strong) ReturnCodeStrBlock returnCodeStrBlock;
@end
