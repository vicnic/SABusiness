//
//  LeadPageView.h
//  7UGame
//
//  Created by 111 on 2017/2/18.
//  Copyright © 2017年 111. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeadPageView : UIView
+ (void)leadPageViewWithImageNames:(NSArray *)imageNames andView:(UIView*)view ;
@end
