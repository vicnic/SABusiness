//
//  LeadPageView.m
//  7UGame
//
//  Created by 111 on 2017/2/18.
//  Copyright © 2017年 111. All rights reserved.
//

#import "LeadPageView.h"
#define ScreenWidth     [UIScreen mainScreen].bounds.size.width
#define ScreenHeight    [UIScreen mainScreen].bounds.size.height
@interface LeadPageView ()<UIScrollViewDelegate>
/** 滚动图片的`ScrollView` */
@property (nonatomic, strong) UIScrollView *mainScrollView;
/** 底部 `pageControl` */
@property (nonatomic, strong) UIPageControl *pageControl;
/** `跳过` 按钮 */
@property (nonatomic, strong) UIButton *skipBtn;
/** 存放图片名称是数组 */
@property (nonatomic, strong) NSMutableArray  *imageNames;
/** 存放images数组 */
@property (nonatomic, strong) NSMutableArray *images;
/** 立即体验 按钮 */
@property (nonatomic, strong) UIButton *entryBtn;
@end
@implementation LeadPageView

- (instancetype)initWithFrame:(CGRect)frame imageNames:(NSArray *)imageNames {
    NSAssert(imageNames, @"imageNames can not be nil.");
    if (self = [super initWithFrame:frame]) {
        self.images = [NSMutableArray new];
        self.imageNames = [NSMutableArray new];
        self.images = imageNames.mutableCopy;
        /** 处理传进来的imageNames */
        [self checkImageNames];
        
        [self addSubview:self.mainScrollView];
        /** 跳过按钮 */
        [self addSubview:self.skipBtn];
        
        [self addSubview:self.pageControl];
        [self configScrollViewImages];
    }
    return self;
}
/**
 检查传入imageNames, 如果通过名称找不到图片, 移除.
 */
- (void)checkImageNames {
    //    self.images = [NSMutableArray array];
    //    for (NSString *imageName in self.imageNames) {
    //        UIImage *image = [UIImage imageNamed:imageName];
    //        if (image) {
    //            [self.images addObject:image];
    //        }
    //    }
}
/** 传入图片名称数组 */
+ (void)leadPageViewWithImageNames:(NSArray *)imageNames andView:(UIView*)view  {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    /* 当前app版本 */
    NSString *currentVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    /* 本地app版本 */
    NSString *localVersion = [[NSUserDefaults standardUserDefaults] stringForKey:@"currentVersion"];
    if (![currentVersion isEqualToString:localVersion]) {
        LeadPageView *leadPage = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds imageNames:imageNames];
        //        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        [view addSubview:leadPage];
        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:@"currentVersion"];
    }
}
- (void)configScrollViewImages {
    
    //    NSAssert(self.images.count, @"self.images is empty, check imageNames.");
    
    _mainScrollView.contentSize = CGSizeMake(self.images.count * self.width, self.height);
    for (int i = 0; i < self.images.count; ++i) {
        NSDictionary *dic = self.images[i];
        NSString * imageStr = [NSString stringWithFormat:@"%@%@",ImageDomain,dic[@"SetImg"]];
        UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.width, 0, self.width, self.height)];
        [imageV sd_setImageWithURL:[NSURL URLWithString:imageStr]];
        [self.mainScrollView addSubview:imageV];
    }
    /** 立即体验 */
    [self.mainScrollView addSubview:self.entryBtn];
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (CGFloat)width {
    return self.frame.size.width;
}
#pragma mark - /**************** scrollView delegate ****************/
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger currentIndex = scrollView.contentOffset.x / ScreenWidth;
    BOOL isHidden = (self.images.count-1 == currentIndex);
    self.entryBtn.hidden = !isHidden;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self scrollViewDidEndDecelerating:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetX = scrollView.contentOffset.x;
    NSInteger currentIndex = (offsetX + ScreenWidth/2) / ScreenWidth;
    self.pageControl.currentPage = currentIndex;
    /** scrollView 向左拖动大于100, 进入首页 */
    if (scrollView.contentOffset.x > ScreenWidth * (self.images.count-1)+100) {
        [self disMissLeadPage];
    }
}


#pragma mark - /**************** lazy load ****************/
- (UIScrollView *)mainScrollView {
    if (_mainScrollView == nil) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _mainScrollView.contentInset = UIEdgeInsetsZero;
        _mainScrollView.delegate = self;
        _mainScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _mainScrollView.backgroundColor = [UIColor whiteColor];
        _mainScrollView.showsHorizontalScrollIndicator = NO;
        _mainScrollView.showsVerticalScrollIndicator = NO;
        _mainScrollView.pagingEnabled = YES;
        //_mainScrollView.bounces = NO;
    }
    return _mainScrollView;
}

- (UIPageControl *)pageControl {
    if (_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.frame = CGRectMake((ScreenWidth-100)/2, ScreenHeight-100, 100, 20);
        _pageControl.numberOfPages = self.images.count;
        _pageControl.currentPage = 0;
        _pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:253.0f/255 green:208.0f/255 blue:0 alpha:1.0f];
        _pageControl.pageIndicatorTintColor = [UIColor grayColor];
    }
    return _pageControl;
}

- (UIButton *)skipBtn {
    if (_skipBtn == nil) {
        _skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _skipBtn.frame = CGRectMake(ScreenWidth-80, 44, 60, 30);
        _skipBtn.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8f];
        _skipBtn.layer.cornerRadius = 15;
        _skipBtn.layer.masksToBounds = YES;
        [_skipBtn setTitle:@"跳过" forState:UIControlStateNormal];
        _skipBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_skipBtn addTarget:self action:@selector(disMissLeadPage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _skipBtn;
}

- (UIButton *)entryBtn {
    if (_entryBtn == nil) {
        _entryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _entryBtn.frame = CGRectMake(ScreenWidth/4 + (self.images.count-1) * self.width, ScreenHeight-150, ScreenWidth/2, 46);
        [_entryBtn setTitle:@"立即体验" forState:UIControlStateNormal];
        _entryBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _entryBtn.backgroundColor = [UIColor orangeColor];
        _entryBtn.layer.cornerRadius = 23;
        _entryBtn.layer.masksToBounds = YES;
        _entryBtn.layer.borderWidth = 1.0f;
        _entryBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        _entryBtn.hidden = !(self.images.count == 1);
        [_entryBtn addTarget:self action:@selector(disMissLeadPage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _entryBtn;
}
/** 点击跳过后执行方法 */
- (void)disMissLeadPage {
    [UIView animateWithDuration:1.3f animations:^{
        self.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
