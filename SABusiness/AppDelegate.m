//
//  AppDelegate.m
//  SABusiness
//
//  Created by Jinniu on 2019/12/12.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "BXNavicationController.h"
#import "MainTabbarCtr.h"
#import "IQKeyboardManager.h"
#import <RongIMKit/RongIMKit.h>
#import <FTIndicator/FTIndicator.h>
#import <JSHAREService.h>
@interface AppDelegate ()<RCIMUserInfoDataSource>

@end
/*
 userid是2
     {"code":200,"userId":"2","token":"VPLSvmvEJaT/M+p9mNPO6+wDYQN9R6jVhrZOEej+GULi3821ktQKY+mxDMhrJmt1bt1Mlxvj35+TyjT9wIHuog=="}
 用户信息
 {"code":200,"userName":"vicnic2","userPortrait":"http://img0.imgtn.bdimg.com/it/u=1912113785,1587702892&fm=214&gp=0.jpg","createTime":"2020-01-03 20:04:36"}
 
 
 uiserid是1
 token:oeKI94TiF/gixOXnB70HmuwDYQN9R6jVhrZOEej+GULi3821ktQKY0CTvxp+QMvbLfyuXVK1FJmTyjT9wIHuog==
用户信息
 {"code":200,"userName":"vicnic","userPortrait":"http://b-ssl.duitang.com/uploads/item/201410/09/20141009224754_AswrQ.jpeg","createTime":"2019-12-31 23:42:43"}
 
 
 {
 "code" : 422,
 "data" : {
 "account" : [
 "账号必须大于6位！"
 ],
 "device" : [
 "validation.max.string"
 ]
 },
 "msg" : "fail",
 "desc" : "参数错误！"
 }

 */

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (@available(iOS 11.0, *)) {
        [UIScrollView appearance].contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [[RCIM sharedRCIM] initWithAppKey:@"vnroth0kvldno"];
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
    
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;//控制整个功能是否启用。
    manager.shouldResignOnTouchOutside = YES;//控制点击背景是否收起键盘
    manager.shouldShowToolbarPlaceholder = YES;//中间位置是否显示占位文字
    manager.placeholderFont = [UIFont boldSystemFontOfSize:16];//设置占位文字的字体
    manager.enableAutoToolbar = YES;//控制是否显示键盘上的工具条。
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeFlat];
    [SVProgressHUD setMaximumDismissTimeInterval:2];
    
    [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];
    
    JSHARELaunchConfig *config = [[JSHARELaunchConfig alloc] init];
    config.appKey = JGPushAppKey;
    config.FacebookAppID = @"552898885434206";
    config.FacebookDisplayName = @"Weshop";
    config.TwitterConsumerKey = @"4hCeIip1cpTk9oPYeCbYKhVWi";
    config.TwitterConsumerSecret = @"DuIontT8KPSmO2Y1oAvby7tpbWHJimuakpbiAUHEKncbffekmC";
    config.JChatProAuth = @"a7e2ce002d1a071a6ca9f37d";
    [JSHAREService setupWithConfig:config];
    [JSHAREService setDebug:YES];
    
    NSString * token = [DLUserDefaultModel userDefaultsModel].token;
    if (token.length==0) {//游客状态
        MainTabbarCtr * tab = [MainTabbarCtr new];
        self.window.rootViewController = tab;
    }else{
        [self autoLogin];
    }
    return YES;
}

-(void)autoLogin{
    NSString * url =[WorkUrl returnURL:Interface_For_Login];
    NSMutableDictionary * p = [NSMutableDictionary new];
    [p setObject:[DLUserDefaultModel userDefaultsModel].username forKey:@"account"];
    [p setObject:[DLUserDefaultModel userDefaultsModel].password forKey:@"password"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [DLUserDefaultModel userDefaultsModel].token = responseObject[@"data"][@"token"];
            [ReuseFile getUserInfo:^(NSDictionary * _Nonnull dic) {
                VicSingleObject * single = [VicSingleObject getInstance];
                if (single.userInfoModel.rong_cloud_token.length==0) {
                    [ReuseFile getRongYunToken:^(NSDictionary * _Nonnull dic) {
                        //返回一定成功
                        NSString * token = [DLUserDefaultModel userDefaultsModel].rongyunToken;
                        [self writeRYTokenToSDK:token];
                        [SVProgressHUD dismiss];
                        MainTabbarCtr * tab = [MainTabbarCtr new];
                        self.window.rootViewController = tab;
                    }];
                }else{
                    [SVProgressHUD dismiss];
                    [self writeRYTokenToSDK:single.userInfoModel.rong_cloud_token];
                    MainTabbarCtr * tab = [MainTabbarCtr new];
                    self.window.rootViewController = tab;
                }
            }];
        }else{
            [DLUserDefaultModel userDefaultsModel].token = @"";
            MainTabbarCtr * tab = [MainTabbarCtr new];
            self.window.rootViewController = tab;
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)writeRYTokenToSDK:(NSString*)token{
    [[RCIM sharedRCIM]connectWithToken:token success:^(NSString *userId) {
        NSLog(@"登陆成功的userid是:%@",userId);
    } error:^(RCConnectErrorCode status) {
        NSLog(@"登陆的错误码为:%ld", (long)status);
    } tokenIncorrect:^{
        NSLog(@"token错误");
    }];
}
//填入服务器给的用户信息赋值给SDK
- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *))completion{
    // simple
    RCUserInfo *user = [[RCUserInfo alloc]init];
    user.userId = @"1";// id
    user.name = @"vicnic2";
    user.portraitUri = @"http://img0.imgtn.bdimg.com/it/u=1912113785,1587702892&fm=214&gp=0.jpg";// 头像的url
    return completion(user);
}
@end
