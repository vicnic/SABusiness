//
//  ShopMainViewCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/28.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShopMainViewRecommendModel.h"
#import "ShopMainViewAllGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShopMainViewCell : UICollectionViewCell
@property(nonatomic,retain)ShopMainViewRecommendModel * recModel;
@property(nonatomic,retain)ShopMainViewAllGoodsModel * goodsModel;

@end

NS_ASSUME_NONNULL_END
