//
//  ShopMainViewCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/28.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "ShopMainViewCell.h"
@interface ShopMainViewCell()
@property(nonatomic,retain)UIImageView * imgView;
@property(nonatomic,retain)UILabel * titleLab, * moneyLab,*payNumLab;
@end
@implementation ShopMainViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cornerRadius = 4;
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}
-(void)createUI{
    self.imgView  = [UIImageView new];
    [self.contentView addSubview:self.imgView];
    _imgView.sd_layout.leftEqualToView(self.contentView).rightEqualToView(self.contentView).topEqualToView(self.contentView).heightEqualToWidth();
    self.titleLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.titleLab];
    self.titleLab.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.imgView, 10).rightSpaceToView(self.contentView, 10).autoHeightRatio(0);
    
    self.moneyLab = [UILabel labelWithTitle:@"" color:ThemeColor fontSize:14];
    [self.contentView addSubview:self.moneyLab];
    self.moneyLab.sd_layout.leftEqualToView(self.titleLab).topSpaceToView(self.titleLab, 8).heightIs(16);
    [self.moneyLab setSingleLineAutoResizeWithMaxWidth:300];
    self.payNumLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:12];
    [self.contentView addSubview:self.payNumLab];
    self.payNumLab.sd_layout.leftSpaceToView(_moneyLab, 5).centerYEqualToView(_moneyLab).heightIs(14);
    [_payNumLab setSingleLineAutoResizeWithMaxWidth:300];
}
- (void)setRecModel:(ShopMainViewRecommendModel *)recModel{
    _recModel = recModel;
    [self.imgView sd_setImageWithURL:URL(recModel.goods_thumb) placeholderImage:PlaceHolderImg];
    self.titleLab.text = recModel.goods_name;
    self.moneyLab.text = recModel.goods_price;
    NSString * pay = NSLocalizedString(@"已付款", nil);
    self.payNumLab.text = [NSString stringWithFormat:@"%@:%@",pay,[NSString stringWithFormat:@"%ld",recModel.goods_sales_count]];
    
}
- (void)setGoodsModel:(ShopMainViewAllGoodsModel *)goodsModel{
    _goodsModel = goodsModel;
    [self.imgView sd_setImageWithURL:URL(goodsModel.goods_thumb) placeholderImage:PlaceHolderImg];
    self.titleLab.text = goodsModel.goods_name;
    self.moneyLab.text = goodsModel.goods_price;
    NSString * pay = NSLocalizedString(@"已付款", nil);
    self.payNumLab.text = [NSString stringWithFormat:@"%@:%@",pay,[NSString stringWithFormat:@"%ld",goodsModel.goods_sales_count]];
}

@end
