//
//  StoreMainViewVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/9.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreMainViewVC : SuperViewController
@property(nonatomic,assign)NSInteger shopID;
@end

NS_ASSUME_NONNULL_END
