//
//  StoreMainViewVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/9.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "StoreMainViewVC.h"
#import <QMUIKit/QMUIKit.h>
#import "GoodsColPageVC.h"
#import "PYSearchViewController.h"
#import "BXNavicationController.h"
#import "MainViewCtrCell.h"
#import "ShopMainViewCell.h"
@interface StoreMainViewVC ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    UILabel * _storeNameLab, * _fansNumLab;
    QMUIButton * _focusBtn;
    NSInteger _page;
}
@property(nonatomic,retain)ShopInfoModel * shopInfo;
@property(nonatomic,retain)NSMutableArray * dataArr , * recArr/*推荐数组*/;
@property(nonatomic,retain)UICollectionView * myCol;
@end

@implementation StoreMainViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar = YES;
    self.isHideBottomLine = YES;
    _page = 1;
    self.dataArr = [NSMutableArray new];
    self.recArr = [NSMutableArray new];
    
    [self getShopInfo];
}
-(void)getShopInfo{
    NSString * url = [WorkUrl returnURL:Interface_For_GetShopInfo];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.shopID) forKey:@"shop_id"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            self.shopInfo = [ShopInfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            [self createHeadView];
            [self getShopRecommend];
            [self getAllGoods];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getShopRecommend{
    NSString * url = [WorkUrl returnURL:Interface_For_GetShopMainviewRecommend];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.shopID) forKey:@"shop_id"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myCol.mj_header endRefreshing];
        
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [ShopMainViewRecommendModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.recArr addObjectsFromArray:arrm];
            [self.myCol reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getAllGoods{
    NSString* url = [WorkUrl returnURL:Interface_For_GetShopMainviewAllGoods];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(_page) forKey:@"page"];
    [dic setObject:@(self.shopID) forKey:@"shop_id"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myCol.mj_header endRefreshing];
        [self.myCol.mj_footer endRefreshing];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [ShopMainViewAllGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myCol reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)closeBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)createHeadView{
    UIImageView* imgView = [[UIImageView alloc]initWithFrame:Frame(0, 0, IPHONE_WIDTH, (1055/699.0)*IPHONE_WIDTH)];
    imgView.image = Image(@"storephoto");
    [self.view addSubview:imgView];

    UIView * bgView = [UIView new];
    [self.view addSubview:bgView];
    bgView.sd_layout.leftEqualToView(self.view).topEqualToView(self.view).rightEqualToView(self.view);
    
    _storeNameLab = [UILabel labelWithTitle:self.shopInfo.shop_info.shop_name color:[UIColor whiteColor] fontSize:16];
    [bgView addSubview:_storeNameLab];
    _storeNameLab.sd_layout.leftSpaceToView(bgView, 15).topSpaceToView(bgView, kStatusBarHeight + 20).heightIs(20);
    [_storeNameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIButton * closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setImage:Image(@"guanbi") forState:UIControlStateNormal];
    closeBtn.sd_cornerRadiusFromHeightRatio = @(0.5);
    closeBtn.layer.borderWidth = 1;
    [closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [bgView addSubview:closeBtn];
    closeBtn.sd_layout.centerYEqualToView(_storeNameLab).rightSpaceToView(bgView, 15).heightIs(30).widthIs(65);
    
    _focusBtn = [QMUIButton buttonWithType:UIButtonTypeCustom];
    if (self.shopInfo.is_focus==1) {
        _focusBtn.selected = YES;
    }else{
        _focusBtn.selected = NO;
    }
    [_focusBtn setImage:Image(@"guanzhu") forState:UIControlStateNormal];
    [_focusBtn setImage:Image(@"zan1") forState:UIControlStateSelected];
    [_focusBtn setTitle:NSLocalizedString(@"已关注", nil) forState:UIControlStateSelected];
    [_focusBtn setTitle:NSLocalizedString(@"关注", nil) forState:UIControlStateNormal];
    _focusBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _focusBtn.spacingBetweenImageAndTitle = 5;
    [_focusBtn addTarget:self action:@selector(focusBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_focusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _focusBtn.sd_cornerRadiusFromHeightRatio = @(0.5);
    _focusBtn.layer.borderWidth = 1;
    _focusBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [bgView addSubview:_focusBtn];
    if (self.shopInfo.is_focus==1) {
        _focusBtn.sd_layout.rightSpaceToView(closeBtn, 10).centerYEqualToView(closeBtn).heightIs(30).widthIs(100);
    }else{
        _focusBtn.sd_layout.rightSpaceToView(closeBtn, 10).centerYEqualToView(closeBtn).heightIs(30).widthIs(80);
    }
    
    NSString * fans = [NSString stringWithFormat:@"%@%ld",NSLocalizedString(@"粉丝数", nil),self.shopInfo.shop_info.focus_count];
    _fansNumLab = [UILabel labelWithTitle:fans color:[UIColor whiteColor] fontSize:12];
    [bgView addSubview:_fansNumLab];
    _fansNumLab.sd_layout.leftEqualToView(_storeNameLab).topSpaceToView(_storeNameLab, 10).heightIs(15);
    [_fansNumLab setSingleLineAutoResizeWithMaxWidth:300];
    
    QMUIButton * searchBtn = [QMUIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setImage:Image(@"fangdajing_bai") forState:UIControlStateNormal];
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    searchBtn.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    searchBtn.spacingBetweenImageAndTitle = 8;
    [searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setTitle:NSLocalizedString(@"搜索", nil) forState:UIControlStateNormal];
    searchBtn.sd_cornerRadiusFromHeightRatio = @(0.5);
    [bgView addSubview:searchBtn];
    searchBtn.sd_layout.leftEqualToView(_fansNumLab).topSpaceToView(_fansNumLab, 10).heightIs(40).widthIs(100);
    
    [bgView setupAutoHeightWithBottomView:searchBtn bottomMargin:15];
    //创建collectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    self.myCol = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    _myCol.delegate = self;
    _myCol.dataSource = self;
    _myCol.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_myCol registerClass:[ShopMainViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_myCol registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    [_myCol registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    _myCol.alwaysBounceVertical = YES;
    _myCol.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self->_page = 1;
        [self.recArr removeAllObjects];
        [self.dataArr removeAllObjects];
        [self getShopRecommend];
        [self getAllGoods];
    }];
    _myCol.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self->_page ++ ;
        [self getAllGoods];
    }];
    [self.view addSubview:self.myCol];
    self.myCol.sd_layout.leftSpaceToView(self.view, 0).rightSpaceToView(self.view, 0).topSpaceToView(bgView, 0).bottomSpaceToView(self.view, KTabbarSafeBottomMargin);
    
}
-(void)focusBtnClick{
    NSString * url = [WorkUrl returnURL:Interface_For_PostSetShopFocus];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.shopID) forKey:@"shop_id"];
    if (_focusBtn.isSelected == YES) {
        [dic setObject:@"0" forKey:@"is_focus"];
    }else{
        [dic setObject:@"1" forKey:@"is_focus"];
    }
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"desc"] status:Success];
            self->_focusBtn.selected = !self->_focusBtn.selected;
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)searchBtnClick{
    NSArray *hotSeaches = @[];
    PYSearchViewController *searchViewController = [PYSearchViewController searchViewControllerWithHotSearches:hotSeaches searchBarPlaceholder:NSLocalizedString(@"输入关键字搜索", nil) didSearchBlock:^(PYSearchViewController *searchViewController, UISearchBar *searchBar, NSString *searchText, BOOL isClickTag) {
        GoodsColPageVC* vc = [GoodsColPageVC new];
        vc.shopid = self.shopID;
        if (isClickTag) {
            vc.isClickTag = YES;
//            for (NSDictionary * dic in self.hotWordsOriginArr) {
//                if ([dic[@"hash_value"] isEqualToString:searchText]) {
//                    vc.searchStr = dic[@"hash_key"];
//                    break;
//                }
//            }
        }else{
            vc.searchStr = searchText;
        }
        [searchViewController.navigationController pushViewController:vc animated:YES];
    }];
    BXNavicationController *nav = [[BXNavicationController alloc] initWithRootViewController:searchViewController];
    [self presentViewController:nav  animated:NO completion:nil];
}
#pragma mark - CollectonViewDataSource Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return section==0?self.recArr.count:self.dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ShopMainViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (indexPath.section == 0) {
        cell.recModel = self.recArr[indexPath.row];
    }else{
        cell.goodsModel = self.dataArr[indexPath.row];
    }
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
//设置头部
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind  isEqualToString:UICollectionElementKindSectionHeader]) {  //header
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        for (UIView * v in header.subviews) {
            [v removeFromSuperview];
        }
        NSString * name = indexPath.section==0?NSLocalizedString(@"好物推荐", nil):NSLocalizedString(@"全部商品", nil);
        UILabel * lab = [UILabel labelWithTitle:name color:[UIColor blackColor] fontSize:16];
        [header addSubview:lab];
        lab.sd_layout.leftSpaceToView(header, 15).centerYEqualToView(header).heightIs(20);
        [lab setSingleLineAutoResizeWithMaxWidth:300];
        return header;
    }else if([kind isEqualToString:UICollectionElementKindSectionFooter]){  //footer
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        
        return footer;
    }
    return [UICollectionReusableView new];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(IPHONE_WIDTH, 40);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(IPHONE_WIDTH, 0.01);
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*)indexPath {
    CGFloat margin = 10;
    CGFloat w = (IPHONE_WIDTH-30-margin)/2.f;
    if (indexPath.section==0) {
        ShopMainViewRecommendModel * mo = self.recArr[indexPath.row];
        CGFloat titleHeight = [mo.goods_name sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(w, MAXFLOAT)].height;
        CGFloat height = w + 10 + titleHeight + 8 + 16 + 20;
        return CGSizeMake(w, height);
    }else{
        ShopMainViewAllGoodsModel * mo = self.dataArr[indexPath.row];
        CGFloat titleHeight = [mo.goods_name sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(w, MAXFLOAT)].height;
        CGFloat height = w + 10 + titleHeight + 8 + 16 + 20;
        return CGSizeMake(w, height);
    }    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        //商品详情
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return  UIStatusBarStyleLightContent;
}
@end
