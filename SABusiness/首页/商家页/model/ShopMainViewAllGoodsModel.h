//
//Created by ESJsonFormatForMac on 20/02/28.
//

#import <Foundation/Foundation.h>


@interface ShopMainViewAllGoodsModel : NSObject

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *goods_thumb;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *goods_currency;

@property (nonatomic, copy) NSString *goods_introduction;

@property (nonatomic, assign) NSInteger is_shelves;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, copy) NSString *goods_cate;

@property (nonatomic, copy) NSString *goods_tags;

@property (nonatomic, assign) NSInteger shop_id;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, assign) NSInteger goods_integration;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, assign) NSInteger goods_id;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, assign) NSInteger goods_sales_count;

@property (nonatomic, assign) NSInteger sort;

@property (nonatomic, assign) NSInteger is_recommend;

@end
