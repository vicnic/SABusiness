//
//  MainViewItemTypeCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/27.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MainViewItemTypeCell.h"
#define margin 5
@interface MainViewItemTypeCell()
@property(nonatomic,retain)UIImageView * picView;
@property(nonatomic,retain)UILabel * nameLab;
@end
@implementation MainViewItemTypeCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}
-(void)createUI{
    self.nameLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14];
    [self.contentView addSubview:self.nameLab];
    _nameLab.sd_layout.leftEqualToView(self.contentView).rightEqualToView(self.contentView).bottomEqualToView(self.contentView).heightIs(16);
    
    self.picView = [UIImageView new];
    _picView.image = PlaceHolderImg;
    [self.contentView addSubview:self.picView];
    _picView.sd_layout.bottomSpaceToView(self.nameLab, margin).topSpaceToView(self.contentView, margin).widthEqualToHeight().centerXEqualToView(self.contentView);

}
- (void)setModel:(MainItemModel *)model{
    [self.picView sd_setImageWithURL:URL(model.thumb) placeholderImage:PlaceHolderImg];
    self.nameLab.text = model.cate_name;
}
@end
