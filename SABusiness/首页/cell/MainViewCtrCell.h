//
//  MainViewCtrCell.h
//  SABusiness
//
//  Created by Jinniu on 2019/12/25.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewCtrDataModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MainViewCtrCell : UICollectionViewCell
@property(nonatomic,retain)MainViewCtrDataModel * model;
@end

NS_ASSUME_NONNULL_END
