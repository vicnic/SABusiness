//
//  MainViewCtrCell.m
//  SABusiness
//
//  Created by Jinniu on 2019/12/25.
//  Copyright © 2019 Jinniu. All rights reserved.
//

#import "MainViewCtrCell.h"
@interface MainViewCtrCell()
@property(nonatomic,retain)UIImageView * imgView;
@property(nonatomic,retain)UILabel * titleLab, * moneyLab,*payNumLab;
@end
@implementation MainViewCtrCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.cornerRadius = 4;
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}
-(void)createUI{
    self.imgView  = [UIImageView new];
    [self.contentView addSubview:self.imgView];
    _imgView.sd_layout.leftEqualToView(self.contentView).rightEqualToView(self.contentView).topEqualToView(self.contentView).heightEqualToWidth();
    self.titleLab = [UILabel labelWithTitle:@"" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.titleLab];
    self.titleLab.sd_layout.leftSpaceToView(self.contentView, 10).topSpaceToView(self.imgView, 10).rightSpaceToView(self.contentView, 10).autoHeightRatio(0);
    
    self.moneyLab = [UILabel labelWithTitle:@"" color:ThemeColor fontSize:14];
    [self.contentView addSubview:self.moneyLab];
    self.moneyLab.sd_layout.leftEqualToView(self.titleLab).topSpaceToView(self.titleLab, 8).heightIs(16);
    [self.moneyLab setSingleLineAutoResizeWithMaxWidth:300];
    self.payNumLab = [UILabel labelWithTitle:@"" color:[UIColor lightGrayColor] fontSize:12];
    [self.contentView addSubview:self.payNumLab];
    self.payNumLab.sd_layout.leftSpaceToView(_moneyLab, 5).centerYEqualToView(_moneyLab).heightIs(14);
    [_payNumLab setSingleLineAutoResizeWithMaxWidth:300];
}
- (void)setModel:(MainViewCtrDataModel *)model{
    _model = model;
    [self.imgView sd_setImageWithURL:URL(model.goods_thumb) placeholderImage:PlaceHolderImg];
    self.titleLab.text = model.goods_name;
    self.moneyLab.text = model.goods_price;
    NSString * pay = NSLocalizedString(@"已付款", nil);
    self.payNumLab.text = [NSString stringWithFormat:@"%@:%ld",pay,model.goods_sales_count];
//    CGFloat margin = 10;
//    CGFloat w = (IPHONE_WIDTH-30-margin)/2.f;
//    CGFloat titleHeight = [model.title sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(w, MAXFLOAT)].height;
//    CGFloat height = w + 10 + titleHeight + 8 + 16 + 20;
//    model.itemHeight = height;
    
}
@end
