//
//  MainViewItemTypeCell.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/27.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainItemModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MainViewItemTypeCell : UICollectionViewCell
@property(nonatomic,retain)MainItemModel * model;
@end

NS_ASSUME_NONNULL_END
