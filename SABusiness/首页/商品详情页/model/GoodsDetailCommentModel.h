//
//Created by ESJsonFormatForMac on 20/03/24.
//

#import <Foundation/Foundation.h>


@interface GoodsDetailCommentModel : NSObject

@property (nonatomic, copy) NSString *account;

@property (nonatomic, assign) NSInteger star;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, copy) NSString *comment;

@property (nonatomic, assign) NSInteger goods_id;

@end
