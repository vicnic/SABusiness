//
//  GoodsDetailInfoModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/3/11.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailInfoModel : NSObject
@property (nonatomic, assign) NSInteger goods_integration;

@property (nonatomic, strong) NSArray *goods_thumb;

@property (nonatomic, assign) NSInteger shop_id;

@property (nonatomic, copy) NSString *goods_currency;

@property (nonatomic, copy) NSString *goods_introduction;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, assign) NSInteger sort;

@property (nonatomic, copy) NSString *goods_tags;

@property (nonatomic, copy) NSString *goods_cate;

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *shop_address;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, assign) NSInteger goods_sales_count;

@property (nonatomic, retain) NSArray * goods_description;

@property (nonatomic, assign) NSInteger is_recommend;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, assign) NSInteger goods_id;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, assign) NSInteger is_shelves;

@end

NS_ASSUME_NONNULL_END
