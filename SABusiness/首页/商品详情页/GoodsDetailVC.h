//
//  GoodsDetailVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/3/7.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailVC : SuperViewController
@property(nonatomic,assign)NSInteger goodsID;
@end

NS_ASSUME_NONNULL_END
