//
//  GoodsDetailTopView.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/3/7.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "GoodsDetailTopView.h"
#import "NewPagedFlowView.h"
@interface GoodsDetailTopView()<NewPagedFlowViewDelegate,NewPagedFlowViewDataSource>
@property(nonatomic,retain)NewPagedFlowView * pageFlowView;
@property(nonatomic,retain)UILabel * pointLab,* titleLab,* flagLab,*moneyLab;
@end
@implementation GoodsDetailTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}
- (void)setModel:(GoodsDetailInfoModel *)model{
    self.moneyLab.width = [model.goods_price sizeWithFont:[UIFont systemFontOfSize:17] maxSize:CGSizeMake(300, 17)].width;
    self.flagLab.width = [model.goods_currency sizeWithFont:[UIFont systemFontOfSize:13] maxSize:CGSizeMake(300, 13)].width;
    self.flagLab.x = CGRectGetMaxX(_moneyLab.frame);
    self.flagLab.text = model.goods_currency;
    self.moneyLab.text = model.goods_price;
    self.titleLab.text = model.goods_name;
    if (model.goods_integration>0) {
        NSString * temp = [NSString stringWithFormat:@"%@ %ld",NSLocalizedString(@"可获得积分", nil),model.goods_integration];
        self.pointLab.text = temp;
        self.pointLab.width = [temp sizeWithFont:[UIFont systemFontOfSize:12] maxSize:CGSizeMake(IPHONE_WIDTH, 12)].width;
        self.pointLab.height =15;
    }
    self.pageFlowView.urlImageDataSource = model.goods_thumb.mutableCopy;
    [self.pageFlowView reloadData];//设置完数据刷新数据
}
-(void)createUI{
    self.pageFlowView = [[NewPagedFlowView alloc] initWithFrame:Frame(0, 0, IPHONE_WIDTH, 250)];
    [self addSubview:self.pageFlowView];
    self.pageFlowView.delegate = self;
    self.pageFlowView.dataSource = self;
    self.pageFlowView.minimumPageAlpha = 0.3;//非当前页的透明比例
    self.pageFlowView.hiddenPageControll = YES;
    self.pageFlowView.orginPageCount = 1;//原始页数
    self.pageFlowView.autoTime = 3.0;//设置定时器秒数
//    if (arr.count>0) {
//        self.pageFlowView.urlImageDataSource = arr;//传入网络数据
//    }    
    self.pageFlowView.cornerRadius = 5;//设置圆角
    [self.pageFlowView reloadData];
    
    self.moneyLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:17];
    _moneyLab.frame = Frame(15, CGRectGetMaxY(_pageFlowView.frame), 0, 17);
    [self addSubview:self.moneyLab];
    self.flagLab = [UILabel labelWithTitle:@"" color:[UIColor redColor] fontSize:13];
    _flagLab.frame = Frame(CGRectGetMaxX(_moneyLab.frame), CGRectGetMaxY(_moneyLab.frame)-13, 0, 13);
    [self addSubview:self.flagLab];
    
    UIButton * shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setTitle:NSLocalizedString(@"分享", nil) forState:UIControlStateNormal];
    shareBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [shareBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(sharBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:shareBtn];
    shareBtn.sd_layout.centerYEqualToView(_moneyLab).rightEqualToView(self);
    [shareBtn setupAutoSizeWithHorizontalPadding:5 buttonHeight:30];
    
    self.pointLab = [UILabel labelWithTitle:@"" color:[UIColor colorWithHue:0.05 saturation:0.91 brightness:1 alpha:1] fontSize:12];
    _pointLab.frame = Frame(CGRectGetMinX(_moneyLab.frame), CGRectGetMaxY(_moneyLab.frame)+ 10,  0, 0);
    _pointLab.cornerRadius = 3;
    [self addSubview:self.pointLab];
    
    self.titleLab = [UILabel labelWithTitle:@"婴幼儿穿面窗为宝宝防撞彩电科迪昂志床上永平可水洗婴儿床位" color:[UIColor blackColor] fontSize:16 alignment:NSTextAlignmentLeft];
    [self addSubview:self.titleLab];
    _titleLab.sd_layout.leftEqualToView(_moneyLab).rightSpaceToView(self, 15).topSpaceToView(_pointLab, 10).autoHeightRatio(0);
    [self setupAutoHeightWithBottomView:_titleLab bottomMargin:15];
    
}
- (void)didSelectCell:(PGIndexBannerSubiew *)subView withSubViewIndex:(NSInteger)subIndex{
    
}
-(void)sharBtnClick{
    
}
@end
