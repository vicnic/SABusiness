//
//  GoodsDetailTopView.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/3/7.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailTopView : UIView
@property(nonatomic,retain)GoodsDetailInfoModel * model;
@end

NS_ASSUME_NONNULL_END
