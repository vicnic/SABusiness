//
//  GoodsDetailVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/3/7.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import "GoodsDetailVC.h"
#import <QMUIKit/QMUIKit.h>
#import "LoginVC.h"
#import "ShoppingCarModel.h"
#import "BXNavicationController.h"
#import "ShopInfoModel.h"
#import "GoodsDetailTopView.h"
#import "StoreMainViewVC.h"
#import "GoodsDetailCommentModel.h"
@interface GoodsDetailVC (){
    UIScrollView * _bgScro;
    UILabel * _sendLab,* _monthSend, * _commentNameLab,*_commentContenLab, * _shopNameLab;
    UIImageView * _commentImgView;
    UIView * _botView,*_commentView;
    GoodsDetailTopView  * _topView;
    QMUIButton * _focusBtn;
}
@property(nonatomic,retain)GoodsDetailInfoModel * infoModel;
@property(nonatomic,retain)ShopInfoModel * shopInfo;
@property(nonatomic,retain)GoodsDetailCommentModel * commentModel;
@end

@implementation GoodsDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self createBotView];
    [self createUI];
    [self getData];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetGoodsDetailInfo];
    NSDictionary * dic = @{@"goods_id":@(self.goodsID)};
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            self.infoModel = [GoodsDetailInfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            self.myTitle = self.infoModel.goods_name;
            [self configData];
            [self getShopInfo];
            [self getCommentData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getCommentData{
    NSString * url = [WorkUrl returnURL:Interface_For_GETGoodsDetailComment];
    NSMutableDictionary * dic = [NSMutableDictionary new];
    [dic setObject:@(self.infoModel.goods_id) forKey:@"goods_id"];
    [dic setObject:@(1) forKey:@"page"];
    [dic setObject:@(1) forKey:@"limit"];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [GoodsDetailCommentModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            if (arrm.count > 0) {
                self.commentModel = arrm[0];
                self->_commentNameLab.text = self.commentModel.account;
                self->_commentContenLab.text = self.commentModel.comment;
                
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
    }];
}
-(void)getShopInfo{
    NSString * token = [DLUserDefaultModel userDefaultsModel].token;
    if (token.length==0) {
        LoginVC * vc = [LoginVC new];
        vc.successBlock = ^{
            [self getShopInfo];
        };
        BXNavicationController * nav = [[BXNavicationController alloc]initWithRootViewController:vc];
        [self presentViewController:nav animated:YES completion:nil];
        return;
    }
    NSString * url = [WorkUrl returnURL:Interface_For_GetShopInfo];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.infoModel.shop_id) forKey:@"shop_id"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            self.shopInfo = [ShopInfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            if (self.shopInfo.is_focus==1) {
                self->_focusBtn.selected = YES;
            }else{
                self->_focusBtn.selected = NO;
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)configData{
    _shopNameLab.text = self.infoModel.shop_name;
    _topView.model = self.infoModel;
    _sendLab.text = self.infoModel.shop_address;
    _monthSend.text = [NSString stringWithFormat:@"%@ %ld",NSLocalizedString(@"销量", nil),self.infoModel.goods_sales_count];
    UIImageView* lasImg = nil;
    for (int i =0; i<1; i++) {
        UIImageView * imgView = [UIImageView new];
        [_bgScro addSubview:imgView];
        imgView.sd_layout.leftEqualToView(_bgScro).rightEqualToView(_bgScro).topSpaceToView(lasImg==nil?_commentView:lasImg, lasImg==nil?10:0);
        lasImg = imgView;
        [[SDWebImageManager sharedManager]loadImageWithURL:URL(@"http://t8.baidu.com/it/u=3571592872,3353494284&fm=79&app=86&f=JPEG?w=1200&h=1290") options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
            CGFloat imgHeight = (image.size.height * IPHONE_WIDTH)/image.size.width;
            imgView.sd_layout.heightIs(imgHeight);
            imgView.image = image;
        }];
    }
    if (lasImg==nil) {
        [_bgScro setupAutoHeightWithBottomView:_commentView bottomMargin:10];
    }else{
        [_bgScro setupAutoHeightWithBottomView:lasImg bottomMargin:10];
    }
    
}
-(void)createBotView{
    _botView = [[UIView alloc]initWithFrame:Frame(0, IPHONE_HEIGHT-KTabbarSafeBottomMargin-50, IPHONE_WIDTH, 50)];
    _botView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_botView];
    NSArray * nameArr = @[NSLocalizedString(@"店铺", nil),NSLocalizedString(@"客服", nil),NSLocalizedString(@"收藏", nil)];
    NSArray * imgArr = @[@"店铺",@"客服",@"星形"];
    UIButton* lastBtn = nil;
    for (int i =0; i<3; i++) {
        QMUIButton * btn = [QMUIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 10 + i;
        [btn setTitle:nameArr[i] forState:UIControlStateNormal];
        btn.selected = NO;
        [btn setImagePosition:QMUIButtonImagePositionTop];
        [btn setImage:Image(imgArr[i]) forState:UIControlStateNormal];
        if (i==2) {
            [btn setImage:Image(@"star") forState:UIControlStateSelected];
            _focusBtn = btn;
        }
        [btn addTarget:self action:@selector(botBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.spacingBetweenImageAndTitle = 5;
        btn.frame = Frame(10+50*i, 0, 45, 50);
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_botView addSubview:btn];
        lastBtn = btn;
    }
    CGFloat btnW = (IPHONE_WIDTH - CGRectGetMaxX(lastBtn.frame) - 30)/2.f;
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:NSLocalizedString(@"加入购物车", nil) forState:UIControlStateNormal];
    leftBtn.frame = Frame(CGRectGetMaxX(lastBtn.frame)+15, 5, btnW, 40);
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [leftBtn addTarget:self action:@selector(addToCartClick) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.backgroundColor = [UIColor colorWithHue:0.13 saturation:1 brightness:1 alpha:1];
    [_botView addSubview:leftBtn];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, btnW, 40) byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(20, 20)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = leftBtn.bounds;
    maskLayer.path = maskPath.CGPath;
    leftBtn.layer.mask = maskLayer;
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:NSLocalizedString(@"立即购买", nil) forState:UIControlStateNormal];
    rightBtn.frame = Frame(CGRectGetMaxX(leftBtn.frame), 5, btnW, 40);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    rightBtn.backgroundColor = [UIColor colorWithHue:0.02 saturation:1 brightness:1 alpha:1];
    [_botView addSubview:rightBtn];
    [rightBtn addTarget:self action:@selector(buyNowClick) forControlEvents:UIControlEventTouchUpInside];
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, btnW, 40) byRoundingCorners:UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii:CGSizeMake(20, 20)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = rightBtn.bounds;
    maskLayer1.path = maskPath1.CGPath;
    rightBtn.layer.mask = maskLayer1;
    
}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (scrollView.contentOffset.y <= kTopHeight) {
//        self.navView.y = - scrollView.contentOffset.y;
//        self.navView.alpha = (kTopHeight - scrollView.contentOffset.y)/kTopHeight;
//        _bgScro.y = kTopHeight - scrollView.contentOffset.y;
//    }
//
//}
-(void)createUI{
    _bgScro = [UIScrollView new];
//    _bgScro.delegate = self;
    [self.view addSubview:_bgScro];
    _bgScro.sd_layout.leftEqualToView(self.view).topSpaceToView(self.view, kTopHeight).rightEqualToView(self.view).bottomSpaceToView(_botView, 0);
    _topView = [GoodsDetailTopView new];
    [_bgScro addSubview:_topView];
    _topView.sd_layout.leftEqualToView(_bgScro).rightEqualToView(_bgScro).topEqualToView(_bgScro);
    
    UIView * sendView = [UIView new];
    sendView.backgroundColor = [UIColor whiteColor];
    [_bgScro addSubview:sendView];
    sendView.sd_layout.leftEqualToView(_bgScro).topSpaceToView(_topView, 10).rightEqualToView(_bgScro);
    
    _shopNameLab = [UILabel labelWithTitle:@"新天际家具生活用品店" color:[UIColor colorWithHue:0.05 saturation:0.91 brightness:1 alpha:1] fontSize:16];
    [sendView addSubview:_shopNameLab];
    _shopNameLab.sd_layout.leftSpaceToView(sendView, 15).topSpaceToView(sendView, 15).heightIs(22);
    [_shopNameLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIButton * gotoStoreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [gotoStoreBtn addTarget:self action:@selector(gotoStoreBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [gotoStoreBtn setTitle:NSLocalizedString(@"进店逛逛", nil) forState:UIControlStateNormal];
    gotoStoreBtn.backgroundColor = [UIColor colorWithHue:0.05 saturation:0.91 brightness:1 alpha:1];
    gotoStoreBtn.cornerRadius = 17.5;
    gotoStoreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [sendView addSubview:gotoStoreBtn];
    gotoStoreBtn.sd_layout.rightSpaceToView(sendView, 10).centerYEqualToView(_shopNameLab);
    [gotoStoreBtn setupAutoSizeWithHorizontalPadding:5 buttonHeight:35];
    
    UILabel * tempLab = [UILabel labelWithTitle:NSLocalizedString(@"发货地", nil) color:[UIColor lightGrayColor] fontSize:14];
    [sendView addSubview:tempLab];
    tempLab.sd_layout.leftSpaceToView(sendView, 15).topSpaceToView(_shopNameLab, 10).heightIs(20);
    [tempLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIImageView * locImg = [UIImageView new];
    locImg.image = Image(@"定位");
    [sendView addSubview:locImg];
    locImg.sd_layout.leftSpaceToView(tempLab, 10).centerYEqualToView(tempLab).heightIs(20).widthEqualToHeight();
    
    _sendLab = [UILabel labelWithTitle:@"上海 | 快递 0.00" color:[UIColor blackColor] fontSize:14];
    [sendView addSubview:_sendLab];
    _sendLab.sd_layout.leftSpaceToView(locImg, 10).centerYEqualToView(locImg).heightIs(20);
    [_sendLab setSingleLineAutoResizeWithMaxWidth:300];
    _monthSend = [UILabel labelWithTitle:@"月销 3000" color:[UIColor lightGrayColor] fontSize:14];
    [sendView addSubview:_monthSend];
    _monthSend.sd_layout.leftEqualToView(tempLab).topSpaceToView(tempLab, 10).heightIs(16);
    [_monthSend setSingleLineAutoResizeWithMaxWidth:300];
    [sendView setupAutoHeightWithBottomView:_monthSend bottomMargin:15];
    
    _commentView = [UIView new];
    _commentView.backgroundColor = [UIColor whiteColor];
    [_bgScro addSubview:_commentView];
    _commentView.sd_layout.leftEqualToView(_bgScro).topSpaceToView(sendView, 10).rightEqualToView(_bgScro);
    UILabel * commentTmpLab = [UILabel labelWithTitle:NSLocalizedString(@"宝贝评价", nil) color:[UIColor blackColor] fontSize:16];
    [_commentView addSubview:commentTmpLab];
    commentTmpLab.sd_layout.leftSpaceToView(_commentView, 15).topSpaceToView(_commentView, 15).heightIs(20);
    [commentTmpLab setSingleLineAutoResizeWithMaxWidth:300];
    
    UIButton * seeAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [seeAllBtn setTitle:NSLocalizedString(@"查看全部 >", nil) forState:UIControlStateNormal];
    [seeAllBtn setTitleColor:[UIColor colorWithHue:0.05 saturation:0.91 brightness:1 alpha:1] forState:UIControlStateNormal];
    [seeAllBtn addTarget:self action:@selector(seeAllBtnClick) forControlEvents:UIControlEventTouchUpInside];
    seeAllBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_commentView addSubview:seeAllBtn];
    seeAllBtn.sd_layout.rightEqualToView(_commentView).centerYEqualToView(commentTmpLab);
    [seeAllBtn setupAutoSizeWithHorizontalPadding:10 buttonHeight:35];
    
    _commentImgView = [UIImageView new];
    _commentImgView.cornerRadius =17.5;
    [_commentView addSubview:_commentImgView];
    _commentImgView.sd_layout.leftEqualToView(commentTmpLab).topSpaceToView(commentTmpLab, 15).heightIs(35).widthEqualToHeight();
    _commentNameLab = [UILabel labelWithTitle:@"大白菜" color:[UIColor lightGrayColor] fontSize:14];
    [_commentView addSubview:_commentNameLab];
    _commentNameLab.sd_layout.leftSpaceToView(_commentImgView, 10).centerYEqualToView(_commentImgView).heightIs(18);
    [_commentNameLab setSingleLineAutoResizeWithMaxWidth:300];
    _commentContenLab = [UILabel labelWithTitle:@"选的米色北极星一款全套，忒别飘香，就是只有朝里的是彩棉，背后不是，中间的画图杨不是彩棉，要是全是那就更好了啊，但是还是会给店家好评" color:[UIColor blackColor] fontSize:14 alignment:NSTextAlignmentLeft];
    [_commentView addSubview:_commentContenLab];
    _commentContenLab.sd_layout.leftEqualToView(_commentImgView).rightSpaceToView(_commentView, 15).topSpaceToView(_commentImgView, 10).autoHeightRatio(0);
    
    [_commentView setupAutoHeightWithBottomView:_commentContenLab bottomMargin:15];
    [_bgScro setupAutoHeightWithBottomView:_commentView bottomMargin:10];
}
-(void)seeAllBtnClick{
    
}
-(void)addToCartClick{
    [self addShopCart];
}
-(void)buyNowClick{
    
}
-(void)gotoStoreBtnClick{
    StoreMainViewVC * vc = [StoreMainViewVC new];
    vc.shopID = self.infoModel.shop_id;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)botBtnClick:(QMUIButton*)sender{
    if (sender.tag == 10) {
        [self gotoStoreBtnClick];
    }else if (sender.tag ==11){
        
    }else{
        [self focusBtnClick:sender];
    }
}
-(void)addShopCart{
    //先获取购物车列表看是否有商品，有就在那基础上加一，没有就老样子新增
    NSString * url = [WorkUrl returnURL:Interface_For_GetShopCarList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [ShoppingCarModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            BOOL  isFind = NO;
            NSInteger count = 0;
            for (ShoppingCarModel * mo  in arrm) {
                for (ShoppingCardList * model in mo.goods_list) {
                    if (model.goods_id == self.infoModel.goods_id) {
                        isFind = YES;
                        count = model.goods_count;
                        break;
                    }
                }
            }
            if (isFind) {
                count ++ ;
            }else{
                count = 1;
            }
            [self postAddCartRequest:[NSString stringWithFormat:@"%ld",count]];
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
    
    
}
-(void)postAddCartRequest:(NSString*)count{
    NSString * url = [WorkUrl returnURL:Interface_For_SetShoppingCartNum];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.infoModel.goods_id) forKey:@"goods_id"];
    [dic setObject:@(self.infoModel.shop_id) forKey:@"shop_id"];
    [dic setObject:count forKey:@"goods_count"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Success];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)focusBtnClick:(QMUIButton*)sender{
    NSString * url = [WorkUrl returnURL:Interface_For_PostSetShopFocus];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:@(self.infoModel.shop_id) forKey:@"shop_id"];
    if (_focusBtn.isSelected) {
        [dic setObject:@"0" forKey:@"is_focus"];
    }else{
        [dic setObject:@"1" forKey:@"is_focus"];
    }
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"desc"] status:Success];
            sender.selected = !sender.selected;
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
@end
