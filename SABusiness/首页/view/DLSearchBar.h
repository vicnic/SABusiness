//
//  DLSearchBar.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^BeginClickBlock)(void);
typedef void(^SearchTFBlock)(NSString * keyWord);
@interface DLSearchBar : UIView
@property(nonatomic,copy)SearchTFBlock searchBlock;
@property(nonatomic,copy)BeginClickBlock beginBlock;
@end

NS_ASSUME_NONNULL_END
