//
//  GoodsColPageVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsColPageVC : SuperViewController
@property(nonatomic,copy)NSString * cateStr;
@property(nonatomic,copy)NSString * searchStr;
@property(nonatomic,assign)BOOL isClickTag;
@property(nonatomic,assign)NSInteger shopid;//如果是商铺搜索则毕传
@end

NS_ASSUME_NONNULL_END
