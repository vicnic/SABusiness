//
//  GoodsColPageVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "GoodsColPageVC.h"
#import "YBPopupMenu.h"
#import "GoodsColPageCell.h"
@interface GoodsColPageVC ()<YBPopupMenuDelegate,UITableViewDelegate,UITableViewDataSource>{
    NSInteger _page;
}
@property(nonatomic,retain)NSMutableArray * dataArr;
@property(nonatomic,retain)UITableView * myTab;
@property(nonatomic,copy)NSString * orderStr;
@end

@implementation GoodsColPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"商品", nil);
    self.rightBtnImage = Image(@"shaixuan");
    _page =1 ;
    self.dataArr = [NSMutableArray new];
    [self.view addSubview:self.myTab];
    if (self.cateStr) {
        //表示是分类点进来的
        [self getData];
    }else{
        if (self.isClickTag) {
            [self searchByTag];
        }else{
            [self getSearchData];
        }
    }
    
}
-(void)searchByTag{
    NSString * url = [WorkUrl returnURL:Interface_For_SearchByClickTag];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:self.searchStr forKey:@"goods_tag"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [SVProgressHUD dismiss];
            [self.myTab.mj_header endRefreshing];
            [self.myTab.mj_footer endRefreshing];
            if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
                NSArray * arrm = [GoodsColPageModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
                [self.dataArr addObjectsFromArray:arrm];
                [self.myTab reloadData];
                [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
            }else{
                [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getSearchData{
    NSString * url = @"";
    if (self.shopid) {
        url = [WorkUrl returnURL:Interface_For_PostShopSearchGoods];
    }else{
        url = [WorkUrl returnURL:Interface_For_PostSearchAllGoods];
    }
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:self.searchStr forKey:@"keywords"];
    [dic setObject:@(_page) forKey:@"page"];
    if (self.orderStr) {
        [dic setObject:self.orderStr forKey:@"order"];
    }
    if(self.shopid){
        [dic setObject:@(self.shopid) forKey:@"shop_id"];
    }
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [SVProgressHUD dismiss];
            [self.myTab.mj_header endRefreshing];
            [self.myTab.mj_footer endRefreshing];
            if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
                NSArray * arrm = [GoodsColPageModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
                [self.dataArr addObjectsFromArray:arrm];
                [self.myTab reloadData];
                [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
            }else{
                [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getData{
    NSString * url = [WorkUrl returnURL:Interface_For_GetCateListData];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [dic setObject:self.cateStr forKey:@"cate"];
    [dic setObject:@(_page) forKey:@"page"];
    if (self.orderStr) {
        [dic setObject:self.orderStr forKey:@"order"];
    }
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.myTab.mj_header endRefreshing];
        [self.myTab.mj_footer endRefreshing];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arrm = [GoodsColPageModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:arrm];
            [self.myTab reloadData];
            [ReuseFile NeedResetNoViewWithTable:self.myTab andArr:self.dataArr];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
#pragma mark tableView代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GoodsColPageCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(UITableView*)myTab{
    if (_myTab == nil) {
        _myTab = [[UITableView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-KTabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _myTab.delegate = self;
        _myTab.dataSource = self;
        [_myTab registerNib:[UINib nibWithNibName:@"GoodsColPageCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        _myTab.rowHeight = 120;
        _myTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self.dataArr removeAllObjects];
            if (self.cateStr) {
                [self getData];
            }else{
                if (self.isClickTag) {
                    [self searchByTag];
                }else{
                    [self getSearchData];
                }
            }
        }];
        _myTab.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self->_page ++ ;
            if (self.cateStr) {
                [self getData];
            }else{
                if (self.isClickTag) {
                    [self searchByTag];
                }else{
                    [self getSearchData];
                }
            }
        }];
    }
    return _myTab;
}
- (void)rightBtnClick{
    NSArray * arr = @[NSLocalizedString(@"按价格升序", nil),NSLocalizedString(@"按价格降序", nil),NSLocalizedString(@"按销量升序", nil),NSLocalizedString(@"按销量降序", nil),];
    CGFloat maxWidth = 0;
    for (NSString * str in arr) {
        CGFloat w = [str sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(300, 20)].width;
        if (w>maxWidth) {
            maxWidth = w;
        }
    }
    
    [YBPopupMenu showAtPoint:CGPointMake(IPHONE_WIDTH-22.5, kTopHeight) titles:arr icons:nil menuWidth:maxWidth+35 otherSettings:^(YBPopupMenu *popupMenu) {
        popupMenu.fontSize = 14;
        popupMenu.delegate= self;
    }];
}
- (void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index{
    if (index==0) {
        self.orderStr = @"price_asc";
    }else if (index==1){
        self.orderStr = @"price_desc";
    }else if (index==2){
        self.orderStr = @"sale_asc";
    }else{
        self.orderStr = @"sale_desc";
    }
    [self.myTab.mj_header beginRefreshing];
}

@end
