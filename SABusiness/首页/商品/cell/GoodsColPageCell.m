//
//  GoodsColPageCell.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "GoodsColPageCell.h"
@interface GoodsColPageCell()
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *payNumLab;
@property (weak, nonatomic) IBOutlet UILabel *storeNameLab;
@property (weak, nonatomic) IBOutlet UILabel *descLab;

@end

@implementation GoodsColPageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}
- (void)setModel:(GoodsColPageModel *)model{
    [self.picView sd_setImageWithURL:URL(model.goods_thumb) placeholderImage:PlaceHolderImg];
    self.titleLab.text = model.goods_name;
    self.priceLab.text = model.goods_price;
    self.payNumLab.text = [NSString stringWithFormat:@"%ld %@",model.goods_sales_count,NSLocalizedString(@"人付款", nil)];
    self.storeNameLab.text = model.shop_name;
    self.descLab.text = model.goods_introduction;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
