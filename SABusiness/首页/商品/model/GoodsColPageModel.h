//
//  GoodsColPageModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/29.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsColPageModel : NSObject

@property (nonatomic, copy) NSString *goods_name;

@property (nonatomic, copy) NSString *goods_thumb;

@property (nonatomic, assign) NSInteger is_recommend;

@property (nonatomic, copy) NSString *goods_currency;

@property (nonatomic, copy) NSString *goods_introduction;

@property (nonatomic, assign) NSInteger is_shelves;

@property (nonatomic, assign) NSInteger created_at;

@property (nonatomic, copy) NSString *shop_name;

@property (nonatomic, assign) NSInteger updated_at;

@property (nonatomic, copy) NSString *manager_id;

@property (nonatomic, assign) NSInteger goods_id;

@property (nonatomic, copy) NSString *goods_price;

@property (nonatomic, assign) NSInteger goods_sales_count;

@property (nonatomic, copy) NSString *sort;

@property (nonatomic, copy) NSString *goods_cate;

@property (nonatomic, assign) NSInteger shop_id;
@end

NS_ASSUME_NONNULL_END
