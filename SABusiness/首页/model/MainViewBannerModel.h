//
//  MainViewBannerModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2020/2/1.
//  Copyright © 2020年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainViewBannerModel : NSObject
@property(nonatomic,copy)NSString * thumb,*banner_title;
@property(nonatomic,assign)NSInteger shop_id, goods_id;
@end

NS_ASSUME_NONNULL_END
