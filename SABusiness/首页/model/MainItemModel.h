//
//  MainItemModel.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/27.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainItemModel : NSObject
@property(nonatomic,copy)NSString * cate_name, * thumb, * key;
@end

NS_ASSUME_NONNULL_END
