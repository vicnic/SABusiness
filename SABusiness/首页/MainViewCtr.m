//
//  MainViewCtr.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "MainViewCtr.h"
#import "DLSearchBar.h"
#import "NewPagedFlowView.h"
#import "MainViewCtrCell.h"
#import "PYSearch.h"
#import "BXNavicationController.h"
#import "MainViewItemTypeCell.h"
#import "GoodsColPageVC.h"
#import "MainViewBannerModel.h"
#import "StoreMainViewVC.h"
#import "ZLImageViewDisplayView.h"
#import "GoodsDetailVC.h"
#import <RongIMKit/RongIMKit.h>
@interface MainViewCtr ()<UICollectionViewDelegate,UICollectionViewDataSource,NewPagedFlowViewDelegate,NewPagedFlowViewDataSource>
@property(nonatomic,retain)NewPagedFlowView * pageFlowView;
@property(nonatomic,retain)UICollectionView * myCol;
@property(nonatomic,retain)NSMutableDictionary * bannerDic;
@property(nonatomic,retain)NSMutableArray * dataArr,* itemArr,* bannerArr,* recommendArr,* hotWordsArr,*hotWordsOriginArr;
@property(nonatomic,retain)ZLImageViewDisplayView *recomendPage;
@end

@implementation MainViewCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr = [NSMutableArray new];
    self.bannerArr = [NSMutableArray new];
    self.recommendArr = [NSMutableArray new];
    self.itemArr = [NSMutableArray new];
    self.hotWordsOriginArr = [NSMutableArray new];
    self.hotWordsArr = [NSMutableArray new];
    self.bannerDic = [NSMutableDictionary new];
    [self getHotWords];
    [self getBannerList];
}
-(void)getBannerList{
    NSString * url = [WorkUrl returnURL:Interface_For_GetMainViewBannerList];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * dataArr = [MainViewBannerModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.bannerArr addObjectsFromArray:dataArr];
            [self getRecommend];
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getRecommend{
    NSString * url = [WorkUrl returnURL:Interface_For_GetMainViewRecommentBannerList];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * dataArr = [MainViewBannerModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.recommendArr addObjectsFromArray:dataArr];
            [self.view addSubview:self.myCol];
            [self getCateList];
            [self getMainViewGoods];
        }else{
            [SVProgressHUD dismiss];
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getCateList{
    NSString * url = [WorkUrl returnURL:Interface_For_GetMainViewCateList];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * dataArr = [MainItemModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.itemArr addObjectsFromArray:dataArr];
            [self.myCol reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getMainViewGoods{
    NSString * url = [WorkUrl returnURL:Interface_For_GetMainViewGoodsList];
    NSMutableDictionary * dic = [ReuseFile getParamDic];
    [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * dataArr = [MainViewCtrDataModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
            [self.dataArr addObjectsFromArray:dataArr];
            [self.myCol reloadData];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)getHotWords{
    NSString * url = [WorkUrl returnURL:Interface_For_GetSearchHotWords];
    [PGNetworkHelper GET:url parameters:nil cache:NO responseCache:nil success:^(id responseObject) {
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            NSArray * arr = responseObject[@"data"];
            [self.hotWordsOriginArr addObjectsFromArray:arr];
            for (NSDictionary * dic in arr) {
                NSString * name = dic[@"hash_value"];
                [self.hotWordsArr addObject:name];
            }
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
        [self setUpSearchBar];
    } failure:^(NSError *error) {
    }];
}
-(void)setUpSearchBar{
    NSArray * obj = [[NSBundle mainBundle]loadNibNamed:@"DLSearchBar" owner:nil options:nil];
    DLSearchBar * v = obj.firstObject;
    v.beginBlock = ^{
        NSArray *hotSeaches = self.hotWordsArr.copy;//self.hotWordsArr.count>0?self.hotWordsArr.copy:@[@"新生儿包被", @"家电", @"茶具", @"碗筷", @"平底锅", @"小轿车", @"平板电脑", @"望远镜", @"电热毯"];
        PYSearchViewController *searchViewController = [PYSearchViewController searchViewControllerWithHotSearches:hotSeaches searchBarPlaceholder:NSLocalizedString(@"输入关键字搜索", nil) didSearchBlock:^(PYSearchViewController *searchViewController, UISearchBar *searchBar, NSString *searchText, BOOL isClickTag) {
            GoodsColPageVC* vc = [GoodsColPageVC new];
            if (isClickTag) {
                vc.isClickTag = YES;
                for (NSDictionary * dic in self.hotWordsOriginArr) {
                    if ([dic[@"hash_value"] isEqualToString:searchText]) {
                        vc.searchStr = dic[@"hash_key"];
                        break;
                    }
                }
            }else{
                vc.searchStr = searchText;
            }
            
            [searchViewController.navigationController pushViewController:vc animated:YES];
        }];
        BXNavicationController *nav = [[BXNavicationController alloc] initWithRootViewController:searchViewController];
        [self presentViewController:nav  animated:NO completion:nil];

    };
    v.frame = Frame(kNavBarHeight+10, kStatusBarHeight+5, IPHONE_WIDTH-kNavBarHeight*2-10, kNavBarHeight-10);
    [self.navView addSubview:v];
}
#pragma mark - CollectonViewDataSource Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return section == 0 ? self.itemArr.count :section==1?0:self.dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        MainViewItemTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainViewItemTypeCell" forIndexPath:indexPath];
        if (self.itemArr.count>0) {
            cell.model = self.itemArr[indexPath.row];
        }
        return cell;
    }else{
        MainViewCtrCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TYCustomCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section==1) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsMake(10, 10, 10, 10);
    
}
//设置头部
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind  isEqualToString:UICollectionElementKindSectionHeader]) {  //header
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        for (UIView *view in header.subviews) {
            [view removeFromSuperview];
        }
        if (indexPath.section == 0) {
            id obj = self.bannerDic[@"key0"];
            if (obj == nil) {
                NSMutableArray * arr = [NSMutableArray new];
                if (self.bannerArr.count>0) {
                    for (MainViewBannerModel * mo in self.bannerArr) {
                        [arr addObject:mo.thumb];
                    }
                }
                self.pageFlowView = [[NewPagedFlowView alloc] init];
                [header addSubview:self.pageFlowView];
                self.pageFlowView.sd_layout.topSpaceToView(header, 10).leftEqualToView(header).widthIs(IPHONE_WIDTH).heightIs(180);
                self.pageFlowView.delegate = self;
                self.pageFlowView.dataSource = self;
                self.pageFlowView.minimumPageAlpha = 0.3;//非当前页的透明比例
                self.pageFlowView.hiddenPageControll = YES;
                self.pageFlowView.orginPageCount = self.bannerArr.count;//原始页数
                self.pageFlowView.autoTime = 3.0;//设置定时器秒数
                if (arr.count>0) {
                    self.pageFlowView.urlImageDataSource = arr;//传入网络数据
                }
                
                self.pageFlowView.cornerRadius = 5;//设置圆角
                [self.pageFlowView reloadData];//设置完数据刷新数据
                [self.bannerDic setObject:self.pageFlowView forKey:@"key0"];
            }else{
                self.pageFlowView = (NewPagedFlowView *)obj;
                [header addSubview:self.pageFlowView];
                self.pageFlowView.sd_layout.topSpaceToView(header, 10).leftEqualToView(header).widthIs(IPHONE_WIDTH).heightIs(180);
                [self.pageFlowView reloadData];
            }
        }else if(indexPath.section == 1){
            UILabel * lab = [UILabel labelWithTitle:NSLocalizedString(@"好物推荐", nil) color:[UIColor blackColor] fontSize:16];
            [header addSubview:lab];
            lab.sd_layout.leftSpaceToView(header, 15).centerYEqualToView(header).heightIs(20);
            [lab setSingleLineAutoResizeWithMaxWidth:300];
        }
        
        return header;
    }else if([kind isEqualToString:UICollectionElementKindSectionFooter]){  //footer
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        for (UIView *view in footer.subviews) {
            [view removeFromSuperview];
        }
        if (indexPath.section == 1){
            id obj = self.bannerDic[@"key1"];
            if (obj == nil){
                NSMutableArray * arr = [NSMutableArray new];
                if (self.recommendArr.count>0) {
                    for (MainViewBannerModel * mo in self.recommendArr) {
                        [arr addObject:mo.thumb];
                    }
                }
                self.recomendPage = [ZLImageViewDisplayView zlImageViewDisplayViewWithFrame:Frame(0, 10, IPHONE_WIDTH, 180)];
                if (arr.count>0) {
                    self.recomendPage.imageViewArray = arr;//传入网络数据
                }
                self.recomendPage.scrollInterval = 5;
                self.recomendPage.animationInterVale = 0.6;
                [footer addSubview:self.recomendPage];
                MJWeakSelf
                [self.recomendPage addTapEventForImageWithBlock:^(NSInteger imageIndex) {
                    //轮播图点击事件
                    MainViewBannerModel * mo = weakSelf.recommendArr[imageIndex-1];
                    if (mo.shop_id == 0) {
                        GoodsDetailVC * vc = [GoodsDetailVC new];
                        vc.goodsID = mo.goods_id;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else{
                        StoreMainViewVC * vc = [StoreMainViewVC new];
                        vc.shopID = mo.shop_id;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }
                }];
                [self.bannerDic setObject:self.recomendPage forKey:@"key1"];
            }else{
                self.recomendPage = (ZLImageViewDisplayView *)obj;
                [footer addSubview:self.recomendPage];
                self.recomendPage.frame = Frame(0, 10, IPHONE_WIDTH, 180);
            }
        }
        return footer;
    }
    return [UICollectionReusableView new];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if(section==1){
        return CGSizeMake(IPHONE_WIDTH, 40);
    }else if(section==0){
        return CGSizeMake(IPHONE_WIDTH, 200);
    }else{
        return CGSizeMake(IPHONE_WIDTH, 0.01);
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return section==1?CGSizeMake(IPHONE_WIDTH, 200):CGSizeMake(IPHONE_WIDTH, 0.01);
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.section == 0) {
        CGFloat margin = 10;
        CGFloat w = (IPHONE_WIDTH-30-3*margin)/4.f;
        return CGSizeMake(w, w);
    }else{
        CGFloat margin = 10;
        CGFloat w = (IPHONE_WIDTH-30-margin)/2.f;
        MainViewCtrDataModel * mo = self.dataArr[indexPath.row];
        CGFloat titleHeight = [mo.goods_name sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(w, MAXFLOAT)].height;
        CGFloat height = w + 10 + titleHeight + 8 + 16 + 20;
        return CGSizeMake(w, height);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        MainItemModel * mo = self.itemArr[indexPath.row];
        GoodsColPageVC* vc = [GoodsColPageVC new];
        vc.cateStr = mo.key;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MainViewCtrDataModel * mo = self.dataArr[indexPath.row];
        GoodsDetailVC * vc = [GoodsDetailVC new];
        vc.goodsID = mo.goods_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(UICollectionView*)myCol{
    if (_myCol == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _myCol = [[UICollectionView alloc]initWithFrame:Frame(0, kTopHeight, IPHONE_WIDTH, IPHONE_HEIGHT-kTopHeight-kTabbarHeight) collectionViewLayout:layout];
        _myCol.delegate = self;
        _myCol.dataSource = self;
        _myCol.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_myCol registerClass:[MainViewCtrCell class] forCellWithReuseIdentifier:@"TYCustomCell"];
        [_myCol registerClass:[MainViewItemTypeCell class] forCellWithReuseIdentifier:@"MainViewItemTypeCell"];
        [_myCol registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [_myCol registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        _myCol.alwaysBounceVertical = YES;
        _myCol.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{

        }];
    }
    return _myCol;
}
- (void)didSelectCell:(PGIndexBannerSubiew *)subView withSubViewIndex:(NSInteger)subIndex{
    MainViewBannerModel * mo = self.bannerArr[subIndex];
    if (mo.shop_id == 0) {
        GoodsDetailVC * vc = [GoodsDetailVC new];
        vc.goodsID = mo.goods_id;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        StoreMainViewVC * vc = [StoreMainViewVC new];
        vc.shopID = mo.shop_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
@end
