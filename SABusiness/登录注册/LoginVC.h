//
//  LoginVC.h
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "SuperViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^LoginSuccessBlock)(void);
@interface LoginVC : SuperViewController
@property(nonatomic,copy)LoginSuccessBlock successBlock;
@end

NS_ASSUME_NONNULL_END
