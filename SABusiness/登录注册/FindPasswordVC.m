//
//  FindPasswordVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/28.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "FindPasswordVC.h"
#import "JKCountDownButton.h"
@interface FindPasswordVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet JKCountDownButton *verifyBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;
@property (weak, nonatomic) IBOutlet UITextField *pswTF2;

@end

@implementation FindPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"找回密码", nil);
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.topHeight.constant = kTopHeight +10;
    self.botHeight.constant = KTabbarSafeBottomMargin + 80;
    
    [self.verifyBtn countDownButtonHandler:^(JKCountDownButton *countDownButton, NSInteger tag) {
        if (self.nameTF.text.length == 0) {
            [Mytools warnText:NSLocalizedString(@"用户名不能为空", nil) status:Error];
            return ;
        }
        
        NSString * url = [WorkUrl returnURL:Interface_For_ForgetPasswordGetVerify];
        NSDictionary * dic = @{@"account":self.nameTF.text};
        [SVProgressHUD show];
        [PGNetworkHelper GET:url parameters:dic cache:NO responseCache:nil success:^(id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
                [Mytools warnText:responseObject[@"desc"] status:Success];
                countDownButton.enabled = NO;
                [countDownButton startCountDownWithSecond:30];
                [countDownButton countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                    countDownButton.backgroundColor = [UIColor lightGrayColor];
                    NSString *title = [NSString stringWithFormat:@"%@ %lus",NSLocalizedString(@"剩余", nil),(unsigned long)second];
                    return title;
                }];
                [countDownButton countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
                    countDownButton.enabled = YES;
                    countDownButton.backgroundColor = ThemeColor;
                    return NSLocalizedString(@"验证码", nil);
                }];
            }else{
                [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            }
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
        }];
        
    }];
}
- (IBAction)submitBtnClick:(id)sender {
    if (self.nameTF.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"用户名不能为空", nil) status:Error];
        return;
    }
    if (self.pswTF2.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"新密码不能为空", nil) status:Error];
        return;
    }
    if (self.codeTF.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"验证码不能为空", nil) status:Error];
        return;
    }
    if (self.emailTF.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"邮箱不能为空", nil) status:Error];
        return;
    }
    if (self.phoneTF.text.length==0) {
        [Mytools warnText:NSLocalizedString(@"手机号不能为空", nil) status:Error];
        return;
    }
    NSString * url = [WorkUrl returnURL:Interface_For_SubmitResetPassword];
    NSMutableDictionary * p = [ReuseFile getParamDic];
    [p setObject:self.nameTF.text forKey:@"account"];
    [p setObject:self.pswTF2.text forKey:@"new_password"];
    [p setObject:self.codeTF.text forKey:@"verify"];
    [PGNetworkHelper POST:url parameters:p  cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"desc"] status:Success];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

@end
