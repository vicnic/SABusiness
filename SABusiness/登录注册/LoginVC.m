//
//  LoginVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/15.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "LoginVC.h"
#import "MainTabbarCtr.h"
#import "RegisterVC.h"
#import "FindPasswordVC.h"
#import <RongIMKit/RongIMKit.h>
@interface LoginVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *pswTF;
@property (weak, nonatomic) IBOutlet UIButton *rememberBtn;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHideNavBar  = YES;
    self.isHideBottomLine = YES;
    self.topHeight.constant = kTopHeight + 40;
    if ([[DLUserDefaultModel userDefaultsModel].isRememberPsw isEqualToString:@"1"]) {
        self.rememberBtn.selected = YES;
    }
}

- (IBAction)rememberBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.isSelected) {
        [DLUserDefaultModel userDefaultsModel].isRememberPsw = @"1";
    }else{
        [DLUserDefaultModel userDefaultsModel].isRememberPsw = @"0";
    }
}
- (IBAction)forgetBtnClick:(id)sender {
    FindPasswordVC* vc = [FindPasswordVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)loginBtnClick:(id)sender {
    NSString * url =[WorkUrl returnURL:Interface_For_Login];
    NSMutableDictionary * p = [NSMutableDictionary new];
    [p setObject:self.nameTF.text forKey:@"account"];
    [p setObject:self.pswTF.text forKey:@"password"];
    [SVProgressHUD show];
    [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [DLUserDefaultModel userDefaultsModel].username = self.nameTF.text;
            [DLUserDefaultModel userDefaultsModel].password = self.pswTF.text;
            [DLUserDefaultModel userDefaultsModel].token = responseObject[@"data"][@"token"];
            [ReuseFile getUserInfo:^(NSDictionary * _Nonnull dic) {
                NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
                NSNotification * message = [[NSNotification alloc]initWithName:@"modifyuserinfo" object:self userInfo:nil];
                [center postNotification:message];
                VicSingleObject * single = [VicSingleObject getInstance];
                [SVProgressHUD dismiss];
                [self dismissViewControllerAnimated:YES completion:nil];
                if (single.userInfoModel.rong_cloud_token.length==0) {
                    [ReuseFile getRongYunToken:^(NSDictionary * _Nonnull dic) {
                        //返回一定成功
                        NSString * token = [DLUserDefaultModel userDefaultsModel].rongyunToken;
                        [self writeRYTokenToSDK:token];
                    }];
                }else{
                    [self writeRYTokenToSDK:single.userInfoModel.rong_cloud_token];
                }
                if (self.successBlock) {
                    self.successBlock();
                }
            }];            
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)writeRYTokenToSDK:(NSString*)token{
    [[RCIM sharedRCIM]connectWithToken:token success:^(NSString *userId) {
        NSLog(@"登陆成功的userid是:%@",userId);
    } error:^(RCConnectErrorCode status) {
        NSLog(@"登陆的错误码为:%ld", (long)status);
    } tokenIncorrect:^{
        NSLog(@"token错误");
    }];
}
- (IBAction)registerBtnClick:(id)sender {
    RegisterVC * vc = [RegisterVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
