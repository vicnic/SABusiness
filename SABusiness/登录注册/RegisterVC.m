//
//  RegisterVC.m
//  SABusiness
//
//  Created by 谢黎鹏 on 2019/12/22.
//  Copyright © 2019年 Jinniu. All rights reserved.
//

#import "RegisterVC.h"
#import "JKCountDownButton.h"
@interface RegisterVC ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *botHeight;
@property (weak, nonatomic) IBOutlet JKCountDownButton *verifyBtn;

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTitle = NSLocalizedString(@"注册", nil);
    self.topHeight.constant = kTopHeight;
    self.botHeight.constant = KTabbarSafeBottomMargin + 80;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.verifyBtn countDownButtonHandler:^(JKCountDownButton *countDownButton, NSInteger tag) {
        if(self.emailTF.text.length==0){
            [Mytools warnText:NSLocalizedString(@"邮箱不能为空", nil) status:Error];
            return;
        }
        NSString * url = [WorkUrl returnURL:Interface_For_RegisterGetSMS];
        NSMutableDictionary * p = [NSMutableDictionary new];
        [p setObject:self.emailTF.text forKey:@"email"];
        [SVProgressHUD show];
        [PGNetworkHelper GET:url parameters:p cache:NO responseCache:nil success:^(id responseObject) {
            [SVProgressHUD dismiss];
            if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
                [Mytools warnText:responseObject[@"desc"] status:Success];
                countDownButton.enabled = NO;
                [countDownButton startCountDownWithSecond:30];
                [countDownButton countDownChanging:^NSString *(JKCountDownButton *countDownButton,NSUInteger second) {
                    countDownButton.backgroundColor = [UIColor lightGrayColor];
                    NSString *title = [NSString stringWithFormat:@"%@ %lus",NSLocalizedString(@"剩余", nil),(unsigned long)second];
                    return title;
                }];
            }else{
                [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
            }
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
        }];
        
        [countDownButton countDownFinished:^NSString *(JKCountDownButton *countDownButton, NSUInteger second) {
            countDownButton.enabled = YES;
            countDownButton.backgroundColor = ThemeColor;
            return NSLocalizedString(@"验证码", nil);
        }];
    }];
}

- (IBAction)submitBtnClick:(id)sender {
    if(self.phoneTF.text.length==0){
        [Mytools warnText:NSLocalizedString(@"手机号不能为空", nil) status:Error];
        return;
    }
    if(self.emailTF.text.length==0){
        [Mytools warnText:NSLocalizedString(@"邮箱不能为空", nil) status:Error];
        return;
    }
    if(self.usernameTF.text.length==0){
        [Mytools warnText:NSLocalizedString(@"用户名不能为空", nil) status:Error];
        return;
    }
    if(self.passwordTF.text.length==0){
        [Mytools warnText:NSLocalizedString(@"密码不能为空", nil) status:Error];
        return;
    }
    if(self.verifyCodeTF.text.length==0){
        [Mytools warnText:NSLocalizedString(@"验证码不能为空", nil) status:Error];
        return;
    }
    NSString * url = [WorkUrl returnURL:Interface_For_regist];
    NSMutableDictionary * p = [ReuseFile getParamDic];
    [p setObject:self.phoneTF.text forKey:@"mobile"];
    [p setObject:self.emailTF.text forKey:@"email"];
    [p setObject:self.usernameTF.text forKey:@"account"];
    [p setObject:self.passwordTF.text forKey:@"password"];
    [p setObject:self.verifyCodeTF.text forKey:@"verify"];
    [SVProgressHUD show];
    [PGNetworkHelper POST:url parameters:p  cache:NO responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"msg"]isEqualToString:SB_SUCCESS]) {
            [Mytools warnText:responseObject[@"desc"] status:Success];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [Mytools warnText:[ReuseFile getErrorString:responseObject] status:Error];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

@end
